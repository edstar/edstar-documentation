# Exemples de commandes

\rm puce.stl ceramique.stl radiateur.stl sconvection.stl sechangeur.stl; ./mkelec 0 0.001 0.002 0.005 -0.05 -0.01 0.01 0.05 -0.05 -0.01 0.01 0.05

\rm puce.stl ceramique.stl radiateur.stl sconvection.stl sechangeur.stl; ./mkelec 0 0.001 0.002 0.005 -0.011 -0.01 0.01 0.011 -0.011 -0.01 0.01 0.011

stardis -V3 -M config-elec.txt  -d > geom.vtk

stardis -V3 -M config-elec.txt -p 0.0005,0,0,inf -T 0 -n 1000 -e

stardis -V3 -M config-elec.txt -P 0.001,0,0,inf -T 0 -n 1000 -e

stardis -V3 -M config-elec.txt -P 0,0,0,inf -T 0 -n 1000 -e

stardis -V3 -M config-elec.txt -p 0.0005,0,0,inf -T 0 -n 10 -e -D all,path_

# Nouvelle version

\rm puce.stl radiateur.stl ceramique.stl sconvection.stl sechangeur.stl; ./mkelec.diffuseur 0.001 0.001 0.003 0.02 0.1 1 0.9 12 0.2 0.03

stardis -V3 -M config-elec.diffuseur.txt  -d > geom.vtk

stardis -V3 -M config-elec.diffuseur.txt -p 0.0005,0,0,inf -T 0 -n 10 -e -D all,path_

stardis -V3 -M config-elec.diffuseur.txt -p 0.0005,0,0,inf -T 0 -n 1000 -e
