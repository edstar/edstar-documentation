mkbox -0.5 0.5 -0.5 0.5 -0.5 0.5 > cube.stl

stardis -V3 -M config-cube.txt  -d > geom.vtk

stardis -V3 -M config-cube.txt -p 0,0,0,30000 -T 0 -n 1000 -e

stardis -V3 -M config-cube.txt -P -0.5,0,0,30000 -T 0 -n 1000 -e

stardis -V3 -M config-cube.txt -p 0,0,0,30000 -T 0 -n 10 -e -D all,path_
