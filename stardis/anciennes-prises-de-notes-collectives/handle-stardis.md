# Voir la géométrie

Pour dumper la géométrie au format vtk. Cela permet de vérifier que les géométries que l'on fourni sont bien faites :

    stardis -M material.txt -M bc.txt -d > geom.vtk

# Sortir des chemins

Pour dumper des chemins. C'est en ajout à un calcul. `-D all,path_` signifie que l'on sort tous les types de chemins et qu'on les sauvegarde dans des fichiers commençant par `path_`.

    stardis -V 2 -M material.txt -M bc.txt -p 0.5,0.5,-0.15,28800 -n 10 -D all,path_

# Les mans

    man stardis
    man stardis-input
    man stardis-output

# Les outils git

    git log
    git show
    git show <nom fichier>
    git merge abort

# Les compilations

Quand on travaille sous `edstar-stardis-solver` il faut penser à faire `make` et `make install`. Mais il faut penser à configurer le lieu de l'installation (qui par défaut est /usr/src dans le cmake) en lançant

    ccmake ./

# Pour les bidouilles d'ammandine

## Imposer la température radiative à 293

    stardis -V 2 -M material.txt -M bc.txt -a 293 -p 0.5,0.5,-0.15,28800 -n 10

## Utiliser un fichier de température radiative (nuit_T_rayonnement.txt)

    stardis -V 2 -M material.txt -M bc.txt -a 1010101 -p 0.5,0.5,-0.15,28800 -n 10
