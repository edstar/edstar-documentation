# Récupérer cette documentation

    git clone gitalb_edstar:edstar/edstar-documentation.git

# Introduction

Il s'agit d'installer `stardis-solver` et `stardis` et d'apprendre à programmer au coeur de ces deux projets. En première intention, l'idée n'est pas de faire avancer les projets au sens du développement. Ceux d'entre nous qui contribueront directement au développement de `stardis-solver` et `stardis` créeront des branches dans les dépots git de ces deux projets. Ici, nous allons travailler sur des dépots git séparés, pour des travaux d'exploration plus amont. Typiquement, on peut songer à un travail de thèse au cours duquel on cherche à faire avancer un des concepts de thermique statistique à l'origine de ces projets et pour celà on a besoin de faire des tests de mise en oeuvre qui nécessitent l'infrastucture existante (gestion de la donnée géométrique, couplage immédiat avec le reste de la thermique, reproduction des benchmarks, etc). Il est possible qu'à terme les idées explorées dans cette thèse soient intégrées aux projets eux-même, mais ce sera dans un second temps, après une réflexion plus approfondie sur la cohérence interne des codes. L'étudiant en thèse, lui, peut ne pas se pas se préoccuper de la cohérence. Il n'a pas besoin de tenir compte des enjeux associés à la dynamique de développement, mais pour autant, comme il s'attaque au coeur de la question scientifique, il peut avoir besoin des développements les plus récents. En résumé, il est au contact du développement mais à un stade *interne* où il ne veut pas s'embarasser des contraintes de rigueur d'un projet informatique, dont la préocupation principale est la réception par des utilisateurs *externes* (fiabilité, rétro-compatibilité, documentation, etc).

On se donne donc ici les moyens de faire vivre une activité d'exploration interne, partiellement détachée de la gestion des projets, mais encore suffisamment connectée à celle-ci pour qu'il soit facile de bénéficier des fonctionalités les plus récentes et que le transfert soit rapide lorsque de nouvelles idées sont jugées assez mures pour passer en phase de développement.

# Les dépots `git`

En accord avec ce découpage, il y a deux ensembles de dépots git :

- ceux dédiés au développement (et que l'on n'hésite pas à diffuser vers l'extérieur) : `https://gitlab.com/meso-star/star-engine.git`, `https://gitlab.com/meso-star/stardis-solver.git`, `https://gitlab.com/meso-star/stardis.git` et `https://gitlab.com/meso-star/stardis-documentation.git`

- ceux dédiés à l'exploration interne : `git@gitlab_edstar:edstar/stardis-solver.git`, `git@gitlab_edstar:edstar/stardis.git` et `git@gitlab_edstar:edstar/edstar-documentation.git`

On accède aux premiers via le protocole `https` car dans la pratique que nous décrivons ici, nous ne nous présentons pas comme développeurs : nous ne créerons pas de branches sur ces dépots et nous contenterons de suivre leurs mises à jour.

On accède aux seconds via le protocole `ssh` car nous allons créer des branches sur ces dépots pour contribuer à la vie collective qu'ils reflettent. Le motif `git@gitlab_edstar` sous-entend une gestion des connexions `ssh` via le fichier de configuration `~/.ssh/config`. Dans ce fichier, vous devez ajouter les lignes suivantes :

    Host gitlab_edstar
      IdentityFile ~/.ssh/id_rsa_gitlab_edstar
      Hostname gitlab.com
      User git
      IdentitiesOnly yes

où `id_rsa_gitlab_edstar` est le nom de la clé `ssh` que vous utiliserez pour vos connexions au serveur `gitlab` (nous avons choisi d'utiliser `gitlab` dans la phase actuelle mais ce serveur pourra être déplacé). Vous aurez besoin de donner votre clé publique `id_rsa_gitlab_edstar.pub` à un utilisateur déjà autorisé, de façon à ce qu'il puisse rajouter cette clé publique sur le serveur `gitlab` de `edstar`. Une fois cette clé rajoutée, vous pourrez accéder au serveur par `ssh` avec l'intégralité des droits d'administration.

# La gestion des dépendances : le gestionnaire de paquets `star-engine`

Nous allons vouloir travailler dans `stardis-solver` (la bibliothèque de calcul thermique) et dans `stardis` (l'applicatif), par contre nous ne prévoyons pas ici de faire des modifications dans les bibliothèques dont se servent `stardis-solver` et `stardis` (leurs dépendances). Il nous suffit d'installer ces bibliothèques de façon à pouvoir les employer.

## Installation de `star-engine`

`star-engine` est un gestionnaire de paquets qui permet ce type d'installations. Nous commençons donc par installer `star-engine`. On choisit un répertoire pour toutes nos installations, par exemple `/home/lafrier/Thermique`, on s'y rend, on va chercher `star-engine` sur son dépot `gitlab`, on bascule sur la branche dédiée à `stardis` et on compile les dépendances : 

    lafrier$  cd /home/lafrier/Thermique
    lafrier$  git clone https://gitlab.com/meso-star/star-engine.git
    lafrier$  cd star-engine/
    lafrier$  git checkout stardis
    lafrier$  mkdir build
    lafrier$  cd build/
    lafrier$  cmake ../cmake -DDEPLOY_STARDIS_CLI=1
    lafrier$  make
   
## Utilisation de `star-engine`

Dans tout nouveau shell, avant d'utiliser quoi que ce soit qui se sert du `star-engine` que nous venons d'installer, il faut sourcer le fichier de configuration :

    lafrier$  source /home/lafrier/Thermique/star-engine/local/etc/stardis.profile

## Mise à jour de `star-engine`

Il faut penser à mettre à jour `star-engine` régulièrement si on souhaite pouvoir merger les évolutions de `stardis-solver` et `stardis` avec nos propres codes (aller récupérer les derniers développements si besoin). Pour celà, avant toute chose, il nous faudra aller chercher la nouvelle version de `star-engine` sur le serveur git et la recompiler :

    lafrier$  cd /home/lafrier/Thermique/star-engine
    lafrier$  git checkout stardis
    lafrier$  git pull
    lafrier$  cd build
    lafrier$  make clean
    lafrier$  make

RQ: Lorsque l'on fait une telle mise à jour, on perd tout ce que nos versions personnelles de `stardis-solver` et `stardis` on rajouté comme dépendances propres. En effet, `star-engine` sur sa branche `stardis_develop` est le gestionnaire de paquets de dépendances pour la version `develop` des projets `stardis-solver` et `stardis`. Toutes les dépendances propres à nos versions de travail ont été gérées par nous, indépendament. Mais en général on choisit d'installer nos dépendances propres à au même endroit que celles prévues pour les versions officielles de `stardis-solver` et `stardis`, c'est à dire sous `/home/lafrier/Thermique/star-engine/local`. Après une mise à jour du `star-engine`, il faut donc refaire ces installation de dépendances propres. Cela se résumera le plus souvent à l'exécution des commande de déploiement (voir les paragraphes d'installation de `stardis-solver` et `stardis`) :

    lafrier$  source /home/lafrier/Thermique/star-engine/local/etc/stardis.profile
    lafrier$  cd /home/lafrier/Thermique/stardis-solver/build
    lafrier$  make install
    lafrier$  cd /home/lafrier/Thermique/stardis/build
    lafrier$  make install

RQ: Quand on fait une mise à jour de `star-engine` il faut aussi penser à mettre à jour `stardis-documentation` (voir ci-dessous).

# La documentation et les exemples : `stardis-documentation`

Le projet `stardis` vient avec ses pages de manuel. Notamment, il est utile de regarder les trois man suivants :

    lafrier$  man stardis
    lafrier$  man stardis-input
    lafrier$  man stardis-output

Mais au projet `stardis` est aussi associé un projet de documentation "par la pratique". Ce projet contient un ensemble de configurations thermiques sur lesquelles on peut tester les différentes fonctionnalités de `stardis`.

## Installation de `stardis-documentation`

    lafrier$  cd /home/lafrier/Thermique
    lafrier$  git clone gitlab:meso-star/stardis-documentation.git

## Mise à jour de `stardis-documentation`

Ce projet est à mettre à jour à chaque fois que l'on met à jour le `star-engine` car ils évoluent de façon cohérente.

    lafrier$  cd /home/lafrier/Thermique/stardis-documentation
    lafrier$  git pull

# La bibliothèque de calcul thermique : `stardis-solver`

## Installation de `stardis-solver`

    lafrier$  cd /home/lafrier/Thermique
    lafrier$  git clone https://gitlab.com/meso-star/stardis-solver.git
    lafrier$  cd stardis-solver
    lafrier$  git checkout develop
    lafrier$  git remote rename origin meso-star
    lafrier$  git remote add origin git@gitlab_edstar:edstar/stardis-solver.git
    lafrier$  git fetch origin
    lafrier$  git branch lafrier
    lafrier$  git checkout lafrier
    lafrier$  git push origin lafrier
    lafrier$  mkdir build
    lafrier$  cd build
    lafrier$  cmake ../cmake/ -DCMAKE_PREFIX_PATH=/home/lafrier/Thermique/star-engine/local/ -DNO_TEST=ON
    lafrier$  ccmake ./
    ccmake:
      CMAKE_INSTALL_PREFIX /home/lafrier/Thermique/star-engine/local

L'éditeur ouvert par ccmake permet de changer l'adresse en appuyant sur [Entrée] une première fois pour modifier puis une seconde pour confirmer. Ensuite il faut configurer en appuyant sur [c] puis quitter avec [q].

Possibilité de se mettre en mode DEBUG ou RELEASE. Deux solutions:
-cmake ../cmake/ -DCMAKE_BUILD_TYPE=Release
-ccmake ./
ccmake: 
   CMAKE_BUILD_TYPE RELEASE (ou DEBUG)

## Modification des sources

Les sources sont sous `/home/lafrier/Thermique/stardis-solver/src`.

## Compilation

    lafrier$  cd /home/lafrier/Thermique/stardis-solver/build
    lafrier$  make

## Déploiement dans `star-engine`

    lafrier$  cd /home/lafrier/Thermique/stardis-solver/build
    lafrier$  make install

## Merge avec la branche `meso-star/develop`

L'idée est de bénéficier, dans nos codes (typiquement celui qui est sur la branche `lafrier`), des avancées faites par les développeurs de `stardis-solver` sur la branche `develop` du projet `stardis-solver`, qui est hébergée sur le dépot `meso-star`.

    lafrier$  cd /home/lafrier/Thermique/stardis-solver
    lafrier$  git checkout lafrier
    lafrier$  git fetch meso-star
    lafrier$  git merge meso-star/develop




    lafrier$  

# L'applicatif : `stardis`

## Installation de `stardis`

    lafrier$  cd /home/lafrier/Thermique
    lafrier$  git clone https://gitlab.com/meso-star/stardis.git
    lafrier$  cd stardis
    lafrier$  git checkout develop
    lafrier$  git remote rename origin meso-star
    lafrier$  git remote add origin git@gitlab_edstar:edstar/stardis.git
    lafrier$  git fetch origin
    lafrier$  git branch lafrier
    lafrier$  git checkout lafrier
    lafrier$  git push origin lafrier
    lafrier$  mkdir build
    lafrier$  cd build
    lafrier$  cmake ../cmake/ -DCMAKE_PREFIX_PATH=/home/lafrier/Thermique/star-engine/local/
    lafrier$  ccmake ./
    ccmake:
      CMAKE_INSTALL_PREFIX /home/lafrier/Thermique/star-engine/local

Possibilité de se mettre en mode DEBUG ou RELEASE. Deux solutions:
-cmake ../cmake/ -DCMAKE_BUILD_TYPE=Release
-ccmake ./
ccmake: 
   CMAKE_BUILD_TYPE RELEASE (ou DEBUG)

## Modification des sources

Les sources sont sous `/home/lafrier/Thermique/stardis/src`.

## Compilation

    lafrier$  cd /home/lafrier/Thermique/stardis/build
    lafrier$  make

## Déploiement dans `star-engine`

    lafrier$  cd /home/lafrier/Thermique/stardis/build
    lafrier$  make install

## Merge (d'après séance de travail du 30-09-2020, à confirmer...)

- cd star-engine-stardis-develop
- git fetch origin
- git merge origin/stardis
- tig (si pas installé: sudo apt install tig)
- cd build
- make

### Sur stardis-solver
- cd stardis-solver
- git remote -v
- git fetch meso-star (interroger le dépôt meso-star)
- git status
- git add ./ (ajout de tous les fichiers)
- git commit -m "Nom de la modif" (commit de la modif)
- git status
- git merge meso-star/develop (fusion avec ma branche en local)
- tig 

Le merge est fait localement, il faut ensuite pousser:

- git push origin lafrier (ou autre branche perso)
- cd build
- make
- make install
 








