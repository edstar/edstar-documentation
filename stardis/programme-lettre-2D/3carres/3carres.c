/* Copyright (C) |Meso|Star> 2018 (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */
 
#include <sdis.h>
#include <rsys/rsys.h>
#include <rsys/math.h>
#include <stdio.h>

/* T < 0 means the temperature is unknown */
#define UNKNOWN_TEMPERATURE -1
/* Define conductivities in W/m.K */
#define LAMBDA1 1.0
#define LAMBDA2 0.4
/* Define the T1 and T2 temperatures, in Kelvins */
#define T1 473.15
#define T2 293.15
/* Define the H coefficients */
#define H1 10.0
#define H2 40.0
/* Define emissivities */
#define EPSILON1 0.8
#define EPSILON2 0.3
/* Realization count */
#define N 10000
/* Tref for linearization */
#define TREF ((T1 + T2) * 0.5)
/* SIGMA in W/m^2.K^4 */
#define SIGMA 5.67e-8

/*
 *        //////////////////////////////////
 *        +----------+----------+----------+(3,1)
 *        |##########|          |..........|
 *        |##########|          |..........|          Y
 *  h1,T1 |##########|          |..........| h2,T2    |
 *        |##########|          |..........|          o--X
 *        |##########|          |..........|
 * (-3,-1)+----------+----------+----------+
 *        //////////////////////////////////
 */

/*******************************************************************************
 * 3 Squares geometry
 ******************************************************************************/

/* The following array lists the 2D coordinates of the vertices.
 *
 *           0------1------2------3 (+3,+1)
 *           |      |      |      |             Y
 *           |      |      |      |             |
 *           |      |      |      |             o--X
 *   (-3,-1) 7------6------5------4
 */
static const double
squares_vertices[8/*#vertices*/][2/*#coords per vertex*/] = {
 {-3.0,+1.0}, /* Vertex 0 */
 {-1.0,+1.0},
 {+1.0,+1.0},
 {+3.0,+1.0},
 {+3.0,-1.0},
 {+1.0,-1.0},
 {-1.0,-1.0},
 {-3.0,-1.0}  /* Vertex 7 */
};

/* The following array lists the indices toward the 2D vertices of each segment.
 *
 *    +--0---+--8---+--4---+
 *    |      |      |      |
 *    3      1      7      5
 *    |      |      |      |
 *    +--2---+--9---+--6---+
 */
static const size_t
squares_segments[10/*#segments*/][2/*#ids per segment*/] = {
  {0, 1}, /* Segment 0 */
  {1, 6},
  {6, 7},
  {7, 0},
  {2, 3},
  {3, 4},
  {4, 5},
  {5, 2},
  {2, 1},
  {6, 5}  /* Segment 9 */
};

static void
squares_get_position(const size_t ivert, double pos[2], void* context)
{
  pos[0] = squares_vertices[ivert][0];
  pos[1] = squares_vertices[ivert][1];
}

static void
squares_get_indices(const size_t iseg, size_t ids[2], void* context)
{
  ids[0] = squares_segments[iseg][0];
  ids[1] = squares_segments[iseg][1];
}

static void
squares_get_interface
  (const size_t iseg, struct sdis_interface** interf, void* context)
{
  struct sdis_interface** interfaces = context;
  *interf = interfaces[iseg];
}

/*******************************************************************************
 * Solid data
 ******************************************************************************/
struct solid {
  double lambda; /* Conductivity */
  double rho; /* Volumic mass */
  double cp; /* Calorific capacity */
  double delta; /* Numerical parameter */
  double temperature; /* < 0 means unknown temperature */
};

static double
solid_get_calorific_capacity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->cp;
}

static double
solid_get_thermal_conductivity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->lambda;
}

static double
solid_get_volumic_mass
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->rho;
}

static double
solid_get_delta
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->delta;
}

static double
solid_get_temperature
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->temperature;
}

/*******************************************************************************
 * Fluid data
 ******************************************************************************/
struct fluid {
  double cp; /* Calorific capacity */
  double rho; /* Volumic mass */
  double temperature; /* < 0 means unknown temperature */
};

static double
fluid_get_calorific_capacity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->cp;
}

static double
fluid_get_volumic_mass
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->rho;
}

static double
fluid_get_temperature
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->temperature;
}

/*******************************************************************************
 * Interface data
 ******************************************************************************/
struct intface { /* Cannot use the name 'interface' or Visual C++ will fail */
  double convection_coef;
  double emissivity;
};

static double
interface_get_convection_coef
  (const struct sdis_interface_fragment* frag, struct sdis_data* data)
{
  const struct intface* interface_props = sdis_data_cget(data);
  return interface_props->convection_coef;
}

static double
interface_get_emissivity
  (const struct sdis_interface_fragment* frag, struct sdis_data* data)
{
  const struct intface* interface_props = sdis_data_cget(data);
  return interface_props->emissivity;
}

/*******************************************************************************
 * 1D analytic computation functions
 *******************************************************************************
 *            Ta          Tb                    Tc             Td
 *       H1,T1|  LAMBDA1  |EPSILON1     EPSILON2|   LAMBDA2    |H2,T2
 *             <----e1---> <---------e---------> <-----e2----->
 *        -e/2-e1 ----- -e/2 --------0-------- e/2 ---------- e/2+e2 ----> X
 ******************************************************************************/

static int
linearized_flux
  (double e1, double e2, double* phi_out)
{
  double K = 4 * SIGMA * TREF * TREF * TREF / (1/EPSILON1 + 1/EPSILON2 - 1);
  *phi_out = K * (T1 - T2) / (1 + K * (e1/LAMBDA1 + e2/LAMBDA2 + 1/H1 + 1/H2));
  return 1;
}

static double
phi_equation
  (double e1, double e2, double phi)
{
  double K0 = SIGMA / (1/EPSILON1 + 1/EPSILON2 - 1);
  double K1 = e1/LAMBDA1 + 1/H1;
  double K2 = e2/LAMBDA2 + 1/H2;
  return K0 * (pow(T1 - phi * K1, 4) - pow(T2 + phi * K2, 4)) - phi;
}

static int
non_linearized_flux
  (double e1, double e2, double* phi_out)
{
  double phi; /* Radiative flux in central fluid */

  double K0 = SIGMA / (1/EPSILON1 + 1/EPSILON2 - 1);
  double K1 = e1/LAMBDA1 + 1/H1;
  double K2 = e2/LAMBDA2 + 1/H2;
  int cpt = 0;

  int CPT_MAX = 100;
  double ERRX_MAX = (e1 + e2) * 1e-6;
  double ERRF_MAX;

  double phiL, x;
  double phi1, phi2, phi_mid;
  double f1, f2, f_mid;
  double errx, errf;

  /* Find a first [phi1 phi2] interval that includes a root */
  if(!linearized_flux(e1, e2, &phiL))
    return 0;
  x = 0.1;
  do {
    phi1 =  phiL * (1 - x);
    phi2 =  phiL * (1 + x);
    f1 = phi_equation(e1, e2, phi1);
    f2 = phi_equation(e1, e2, phi2);
    x *= 2;
    if(++cpt > CPT_MAX) return 0;
  } while (f1 * f2 > 0);

  ERRF_MAX = phiL * 1e-5;
  errx = phi2 - phi1;
  errf = fabs(f1 + f2) * 0.5;
  cpt = 0;

  /* Dychotomic search */
  while(errx > ERRX_MAX && errf > ERRF_MAX) {
    phi_mid = (phi1 + phi2) * 0.5;
    f_mid = phi_equation(e1, e2, phi_mid);
    if(f1 * f_mid < 0) {
      phi2 = phi_mid;
      f2 = f_mid;
    } else {
      phi1 = phi_mid;
      f1 = f_mid;
    }
    errx = phi2 - phi1;
    errf = fabs(f1 + f2) * 0.5;
    if(++cpt > CPT_MAX) return 0;
  }
  phi = (phi1 + phi2) * 0.5;
  *phi_out = phi;
  return 1;
}

static int
analytic_temperature
  (double x, double e1, double e2, double e, int linearize, double* t)
{
  double phi; /* Radiative flux in central fluid */
  double Ta; /* T at the left boundary */
  double Tb; /* T at the central-left boundary */
  double Tc; /* T at the central-right boundary */
  double Td; /* T at the right boundary */
  double u;
  int ok;

  /* If out of solid squares */
  if(x > -e*0.5 && x < +e*0.5) return 0; /* Cannot compute T in central square */
  if(x < -e1-e*0.5) { *t = T1; return 1; } /* Left fluid */
  if(x > e2+e*0.5) { *t = T2; return 1; } /* Right fluid */

  /* If in solid squares */
  if(linearize)
    ok = linearized_flux(e1, e2, &phi);
  else
    ok = non_linearized_flux(e1, e2, &phi);
  if(!ok) return 0;

  Ta = T1-phi/H1;
  Td = T2+phi/H2;
  Tb = Ta-phi*e1/LAMBDA1;
  Tc = Td+phi*e2/LAMBDA2;

  /* If at a boundary */
  if(x == -e1-e*0.5) { *t = Ta; return 1; }
  if(x == -e*0.5) { *t = Tb; return 1; }
  if(x == +e*0.5) { *t = Tc; return 1; }
  if(x == +e2+e*0.5) { *t = Td; return 1; }

  /* If in left solid square */
  if(-e1-e*0.5 < x && x < -e*0.5) {
    u = (x + e1+0.5*e) / e1;
    ASSERT(u > 0 && u < 1);
    *t = (1 - u) * Ta + u * Tb;
    return 1;
  }
  /* If in right solid square */
  ASSERT(e*0.5 < x && x < e2+e*0.5);
  u = (x - e*0.5) / e2;
  ASSERT(u > 0 && u < 1);
  *t = (1 - u) * Tc + u * Td;
  return 1;
}

static int
linearized_temperature
  (double x, double e1, double e2, double e, double* t)
{
  return analytic_temperature(x, e1, e2, e, 1, t);
}

static int
non_linearized_temperature
  (double x, double e1, double e2, double e, double* t)
{
  return analytic_temperature(x, e1, e2, e, 0, t);
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sdis_device* dev = NULL;
  struct sdis_solid_shader solid_shader;
  struct sdis_data* data;
  struct solid* solid_props;
  struct sdis_medium* left_square_solid;
  struct sdis_medium* right_square_solid;
  struct sdis_medium* adiabatic_solid;
  struct sdis_fluid_shader fluid_shader;
  struct sdis_medium* central_fluid;
  struct sdis_medium* left_fluid;
  struct sdis_medium* right_fluid;
  struct fluid* fluid_props;
  struct sdis_interface_shader interface_shader;
  struct intface* interface_props;
  struct sdis_interface* left_adiabatic_interface;
  struct sdis_interface* central_adiabatic_interface;
  struct sdis_interface* right_adiabatic_interface;
  struct sdis_interface* left_limit_interface;
  struct sdis_interface* left_central_cavity_interface;
  struct sdis_interface* right_central_cavity_interface;
  struct sdis_interface* right_limit_interface;
  struct sdis_interface* interfaces[10/*#segments*/];
  struct sdis_scene* scn;
  const double probe_pos[2] = {-2, 0};
  struct sdis_estimator* estimator;
  struct sdis_mc temperature;
  size_t nfailures;
  double linearized = UNKNOWN_TEMPERATURE;
  double non_linearized = UNKNOWN_TEMPERATURE;

  /* Create device */
  SDIS(device_create(NULL, NULL, SDIS_NTHREADS_DEFAULT, 0, &dev));

  /* Setup the solid shader */
  solid_shader.calorific_capacity = solid_get_calorific_capacity;
  solid_shader.thermal_conductivity = solid_get_thermal_conductivity;
  solid_shader.volumic_mass = solid_get_volumic_mass;
  solid_shader.delta_solid = solid_get_delta;
  solid_shader.volumic_power = NULL;
  solid_shader.temperature = solid_get_temperature;

  /* Create and setup physical properties for the left square */
  SDIS(data_create(dev, sizeof(struct solid), ALIGNOF(struct solid), NULL, &data));
  solid_props = sdis_data_get(data);
  solid_props->lambda = LAMBDA1;
  solid_props->rho = 1000.0;
  solid_props->cp = 1.0;
  solid_props->delta = 0.1;
  solid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the solid medium for the left square */
  SDIS(solid_create(dev, &solid_shader, data, &left_square_solid));
  SDIS(data_ref_put(data));

  /* Create and setup physical properties for the right square */
  SDIS(data_create(dev, sizeof(struct solid), ALIGNOF(struct solid), NULL, &data));
  solid_props = sdis_data_get(data);
  solid_props->lambda = LAMBDA2;
  solid_props->rho = 1123.0;
  solid_props->cp = 1.2;
  solid_props->delta = 0.1;
  solid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the solid medium for the right square */
  SDIS(solid_create(dev, &solid_shader, data, &right_square_solid));
  SDIS(data_ref_put(data));

  /* Create and setup physical properties for the additional solid used
   * in adiabatic interfaces */
  SDIS(data_create(dev, sizeof(struct solid), ALIGNOF(struct solid), NULL, &data));
  solid_props = sdis_data_get(data);
  solid_props->lambda = 0; /* Adiabatic */
  solid_props->rho = 1111;
  solid_props->cp = 1;
  solid_props->delta = 0.1;
  solid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the solid medium for adiabatic interfaces */
  SDIS(solid_create(dev, &solid_shader, data, &adiabatic_solid));
  SDIS(data_ref_put(data));

  /* Setup the fluid shader */
  fluid_shader.calorific_capacity = fluid_get_calorific_capacity;
  fluid_shader.volumic_mass = fluid_get_volumic_mass;
  fluid_shader.temperature = fluid_get_temperature;

  /* Create and setup the physical properties for the central fluid */
  SDIS(data_create(dev, sizeof(struct fluid), ALIGNOF(struct fluid), NULL, &data));
  fluid_props = sdis_data_get(data);
  fluid_props->rho = 1.1;
  fluid_props->cp = 1.0;
  fluid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the central fluid medium */
  SDIS(fluid_create(dev, &fluid_shader, data, &central_fluid));
  SDIS(data_ref_put(data));

  /* Create and setup the physical properties for the left fluid */
  SDIS(data_create(dev, sizeof(struct fluid), ALIGNOF(struct fluid), NULL, &data));
  fluid_props = sdis_data_get(data);
  fluid_props->rho = 1.0;
  fluid_props->cp = 1.0;
  fluid_props->temperature = T1;

  /* Create the left fluid medium */
  SDIS(fluid_create(dev, &fluid_shader, data, &left_fluid));
  SDIS(data_ref_put(data));

  /* Create and setup the physical properties for the right fluid */
  SDIS(data_create(dev, sizeof(struct fluid), ALIGNOF(struct fluid), NULL, &data));
  fluid_props = sdis_data_get(data);
  fluid_props->rho = 0.9;
  fluid_props->cp = 1.0;
  fluid_props->temperature = T2;

  /* Create the right fluid medium */
  SDIS(fluid_create(dev, &fluid_shader, data, &right_fluid));
  SDIS(data_ref_put(data));

  /* Setup the interface shader */
  interface_shader = SDIS_INTERFACE_SHADER_NULL;
  /* Overwrite default where appropriate */
  interface_shader.convection_coef = interface_get_convection_coef;
  interface_shader.back.emissivity = interface_get_emissivity;

  /* Create and setup the left adiabatic interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = 0;
  interface_props->emissivity = 0;
  SDIS(interface_create(dev, left_square_solid, adiabatic_solid,
     &interface_shader, data, &left_adiabatic_interface));
  SDIS(data_ref_put(data));

  /* Create and setup the central adiabatic interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = 0;
  interface_props->emissivity = 0;
  SDIS(interface_create(dev, adiabatic_solid, central_fluid,
    &interface_shader, data, &central_adiabatic_interface));
  SDIS(data_ref_put(data));

  /* Create and setup the right adiabatic interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = 0;
  interface_props->emissivity = 0;
  SDIS(interface_create(dev, right_square_solid, adiabatic_solid,
    &interface_shader, data, &right_adiabatic_interface));
  SDIS(data_ref_put(data));

  /* Setup the left-limit interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = H1;
  interface_props->emissivity = 0;
  SDIS(interface_create(dev, left_square_solid, left_fluid,
    &interface_shader, data, &left_limit_interface));
  SDIS(data_ref_put(data));

  /* Setup the left-central interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = 0;
  interface_props->emissivity = EPSILON1;
  SDIS(interface_create(dev, left_square_solid, central_fluid,
    &interface_shader, data, &left_central_cavity_interface));
  SDIS(data_ref_put(data));

  /* Setup the right-central interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = 0;
  interface_props->emissivity = EPSILON2;
  SDIS(interface_create(dev, right_square_solid, central_fluid,
    &interface_shader, data, &right_central_cavity_interface));
  SDIS(data_ref_put(data));

  /* Setup the right-limit interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->convection_coef = H2;
  interface_props->emissivity = 0;
  SDIS(interface_create(dev, right_square_solid, right_fluid,
    &interface_shader, data, &right_limit_interface));
  SDIS(data_ref_put(data));

  /* Setup by-segment interfaces */
  interfaces[0] = interfaces[2] = left_adiabatic_interface;
  interfaces[4] = interfaces[6] = right_adiabatic_interface;
  interfaces[8] = interfaces[9] = central_adiabatic_interface;
  interfaces[3] = left_limit_interface;
  interfaces[1] = left_central_cavity_interface;
  interfaces[7] = right_central_cavity_interface;
  interfaces[5] = right_limit_interface;

  SDIS(scene_2d_create(dev,
    10/*#segment*/, squares_get_indices, squares_get_interface,
    8/*#vertices*/, squares_get_position, interfaces, &scn));

  /* Launch the probe simulation */
  SDIS(solve_probe(scn, N, probe_pos, INF, 1.0, 0.0, TREF, &estimator));

  /* Fetch MC estimation and #failures */
  SDIS(estimator_get_temperature(estimator, &temperature));
  SDIS(estimator_get_failure_count(estimator, &nfailures));

  /* Analytically compute the temperature */
  if(! non_linearized_temperature(probe_pos[0], 2, 2, 2, &non_linearized))
    printf("Error when computing analytic reference.\n");
  if(! linearized_temperature(probe_pos[0], 2, 2, 2, &linearized))
    printf("Error when computing linearized analytic reference.\n");

  /* Print results */
  printf("Temperature at [%g, %g] = %g : %g ~ %g +/- %g\n",
    probe_pos[0], probe_pos[1],
    non_linearized, linearized,
    temperature.E, /* Expected value */
    temperature.SE); /* Standard error */
  printf("#failures: %lu/%lu\n",
    (unsigned long)nfailures,
    (unsigned long)N);
    
  /* Release allocated data */
  SDIS(device_ref_put(dev));
  SDIS(scene_ref_put(scn));
  SDIS(medium_ref_put(left_square_solid));
  SDIS(medium_ref_put(right_square_solid));
  SDIS(medium_ref_put(adiabatic_solid));
  SDIS(medium_ref_put(central_fluid));
  SDIS(medium_ref_put(left_fluid));
  SDIS(medium_ref_put(right_fluid));
  SDIS(interface_ref_put(left_adiabatic_interface));
  SDIS(interface_ref_put(central_adiabatic_interface));
  SDIS(interface_ref_put(right_adiabatic_interface));
  SDIS(interface_ref_put(left_limit_interface));
  SDIS(interface_ref_put(left_central_cavity_interface));
  SDIS(interface_ref_put(right_central_cavity_interface));
  SDIS(interface_ref_put(right_limit_interface));
  SDIS(estimator_ref_put(estimator));

  return 0;
}
