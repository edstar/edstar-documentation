/* Copyright (C) |Meso|Star> 2018 (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */
 
#include <sdis.h>
#include <rsys/rsys.h>
#include <rsys/math.h>
#include <stdio.h>

/* T < 0 means the temperature is unknown */
#define UNKNOWN_TEMPERATURE -1
/* Define the T0 and T1 temperatures, in Kelvins */
#define T0 300.0
#define T1 310.0
/* Realization count */
#define N 10000
/* Tref for linearization */
#define TREF ((T0 + T1) * 0.5)

/*
 *           (-1,-1,-1)
 *       +-------+
 *      /'      /|  T0
 *     +-------+ |       Y
 *     | +.....|.+       |
 * T1  |,      |/        o--X
 *     +-------+        /
 * (1,1,1)             Z
 */

/*******************************************************************************
 * Box geometry
 ******************************************************************************/
 
/* The following array lists the 3D coordinates of the vertices.
 *           (-1,-1,-1)
 *       2-------3
 *      /'      /|
 *     6-------7 |       Y
 *     | 0.....|.1       |
 *     |,      |/        o--X
 *     4-------5        /
 * (1,1,1)             Z
 */
static const double box_vertices[8/*#vertices*/][3/*#coords per vertex*/] = {
 {-1.0,-1.0,-1.0}, /* Vertex 0 */
 { 1.0,-1.0,-1.0},
 {-1.0, 1.0,-1.0},
 { 1.0, 1.0,-1.0},
 {-1.0,-1.0, 1.0},
 { 1.0,-1.0, 1.0},
 {-1.0, 1.0, 1.0},
 { 1.0, 1.0, 1.0}  /* Vertex 7 */
};

/* The following array lists the indices toward the 3D vertices of each triangle.
 *        ,2---,3           ,2----3
 *      ,' | ,'/|         ,'/| \  |
 *    6----7' / |       6' / |  \ |        Y
 *    |',  | / ,1       | / ,0---,1        |
 *    |  ',|/,'         |/,' | ,'          o--X
 *    4----5'           4----5'           /
 *  Front, right      Back, left and     Z
 * and Top faces       bottom faces */
static const size_t box_triangles[12/*#triangles*/][3/*#ids per triangle*/] = {
  {0, 4, 2}, {2, 4, 6}, /* -X face, triangles 0 and 1 */
  {0, 1, 5}, {5, 4, 0}, /* -Y face */
  {0, 2, 1}, {1, 2, 3}, /* -Z face */
  {3, 7, 5}, {5, 1, 3}, /* +X face */
  {2, 6, 7}, {7, 3, 2}, /* +Y face */
  {4, 5, 6}, {6, 5, 7}  /* +Z face, triangles 10 and 11 */
};

static void
box_get_position(const size_t ivert, double pos[3], void* context)
{
  pos[0] = box_vertices[ivert][0];
  pos[1] = box_vertices[ivert][1];
  pos[2] = box_vertices[ivert][2];
}

static void
box_get_indices(const size_t itri, size_t ids[3], void* context)
{
  ids[0] = box_triangles[itri][0];
  ids[1] = box_triangles[itri][1];
  ids[2] = box_triangles[itri][2];
}

static void
box_get_interface(const size_t itri, struct sdis_interface** interf, void* context)
{
  struct sdis_interface** interfaces = context;
  *interf = interfaces[itri];
}

/*******************************************************************************
 * Solid data
 ******************************************************************************/
struct solid {
  double lambda; /* Conductivity */
  double rho; /* Volumic mass */
  double cp; /* Calorific capacity */
  double delta; /* Numerical parameter */
  double temperature; /* < 0 means unknown temperature */
};

static double
solid_get_calorific_capacity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->cp;
}

static double
solid_get_thermal_conductivity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->lambda;
}

static double
solid_get_volumic_mass
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->rho;
}

static double
solid_get_delta
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->delta;
}

static double
solid_get_temperature
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct solid* solid_props = sdis_data_cget(data);
  return solid_props->temperature;
}

/*******************************************************************************
 * Fluid data
 ******************************************************************************/
struct fluid {
  double cp; /* Calorific capacity */
  double rho; /* Volumic mass */
  double temperature; /* < 0 means unknown temperature */
};

static double
fluid_get_calorific_capacity
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->cp;
}

static double
fluid_get_volumic_mass
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->rho;
}

static double
fluid_get_temperature
  (const struct sdis_rwalk_vertex* vtx, struct sdis_data* data)
{
  const struct fluid* fluid_props = sdis_data_cget(data);
  return fluid_props->temperature;
}

/*******************************************************************************
 * Interface data
 ******************************************************************************/
struct intface { /* Cannot use the name 'interface' or Visual C++ will fail */
  double temperature; /* < 0 means unknown temperature */
};

static double
interface_get_temperature
  (const struct sdis_interface_fragment* frag, struct sdis_data* data)
{
  const struct intface* interface_props = sdis_data_cget(data);
  return interface_props->temperature;
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sdis_device* dev = NULL;
  struct sdis_solid_shader solid_shader;
  struct sdis_data* data;
  struct solid* solid_props;
  struct sdis_medium* solid;
  struct sdis_fluid_shader fluid_shader;
  struct sdis_medium* fluid;
  struct fluid* fluid_props;
  struct sdis_interface_shader interface_shader;
  struct intface* interface_props;
  struct sdis_interface* interface_adiabatic;
  struct sdis_interface* interface_T0;
  struct sdis_interface* interface_T1;
  struct sdis_interface* interfaces[12/*#triangles*/];
  struct sdis_scene* scn;
  const double probe_pos[3] = {0, 0, 0};
  struct sdis_estimator* estimator;
  struct sdis_mc temperature;
  size_t nfailures;
  double analytic, u;

  /* Create device */
  SDIS(device_create(NULL, NULL, SDIS_NTHREADS_DEFAULT, 0, &dev));

  /* Setup the solid shader */
  solid_shader.calorific_capacity = solid_get_calorific_capacity;
  solid_shader.thermal_conductivity = solid_get_thermal_conductivity;
  solid_shader.volumic_mass = solid_get_volumic_mass;
  solid_shader.delta_solid = solid_get_delta;
  solid_shader.volumic_power = NULL;
  solid_shader.temperature = solid_get_temperature;

  /* Create and setup the solid physical properties */
  SDIS(data_create(dev, sizeof(struct solid), ALIGNOF(struct solid), NULL, &data));
  solid_props = sdis_data_get(data);
  solid_props->cp = 1.0;
  solid_props->lambda = 0.1;
  solid_props->rho = 1.0;
  solid_props->delta = 0.1;
  solid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the solid medium */
  SDIS(solid_create(dev, &solid_shader, data, &solid));
  SDIS(data_ref_put(data));

  /* Setup the fluid shader */
  fluid_shader.calorific_capacity = fluid_get_calorific_capacity;
  fluid_shader.volumic_mass = fluid_get_volumic_mass;
  fluid_shader.temperature = fluid_get_temperature;

  /* Create and setup the fluid physical properties */
  SDIS(data_create(dev, sizeof(struct fluid), ALIGNOF(struct fluid), NULL, &data));
  fluid_props = sdis_data_get(data);
  fluid_props->cp = 1.0;
  fluid_props->rho = 0.1;
  fluid_props->temperature = UNKNOWN_TEMPERATURE;

  /* Create the fluid medium */
  SDIS(fluid_create(dev, &fluid_shader, data, &fluid));
  SDIS(data_ref_put(data));

  /* Setup the interface shader */
  interface_shader = SDIS_INTERFACE_SHADER_NULL;
  interface_shader.front.temperature = interface_get_temperature;

  /* Create and setup the adiabatic interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->temperature = UNKNOWN_TEMPERATURE;
  SDIS(interface_create(dev, solid, fluid, &interface_shader, data, &interface_adiabatic));
  SDIS(data_ref_put(data));

  /* Create and setup the T0 interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->temperature = T0;
  SDIS(interface_create(dev, solid, fluid, &interface_shader, data, &interface_T0));
  SDIS(data_ref_put(data));

  /* Create and setup the T1 interface */
  SDIS(data_create(dev, sizeof(struct intface), ALIGNOF(struct intface), NULL, &data));
  interface_props = sdis_data_get(data);
  interface_props->temperature = T1;
  SDIS(interface_create(dev, solid, fluid, &interface_shader, data, &interface_T1));
  SDIS(data_ref_put(data));

  /* Setup by-triangle interfaces */
  interfaces[0] = interfaces[1] = interface_adiabatic; /* -X */
  interfaces[2] = interfaces[3] = interface_adiabatic; /* -Y */
  interfaces[4] = interfaces[5] = interface_T0;        /* -Z */
  interfaces[6] = interfaces[7] = interface_adiabatic; /* +X */
  interfaces[8] = interfaces[9] = interface_adiabatic; /* +Y */
  interfaces[10]= interfaces[11]= interface_T1;        /* +Z */

  SDIS(scene_create(dev,
    12/*#triangles*/, box_get_indices, box_get_interface,
    8/*#vertices*/, box_get_position, interfaces, &scn));
    
  /* Launch the probe simulation */
  SDIS(solve_probe(scn, N, probe_pos, INF, 1.0, 0.0, TREF, &estimator));
  SDIS(estimator_get_temperature(estimator, &temperature));
  SDIS(estimator_get_failure_count(estimator, &nfailures));

  /* Analytically compute the temperature */
  u = (probe_pos[2] + 1.0) / 2.0;
  analytic = T0 * (1-u) + u * T1;

  /* Print the results */
  printf("Temperature at [%g, %g, %g] = %g ~ %g +/- %g\n",
    probe_pos[0], probe_pos[1], probe_pos[2],
    analytic,
    temperature.E, /* Expected value */
    temperature.SE); /* Standard error */
  printf("#failures: %lu/%lu\n", 
    (unsigned long)nfailures,
    (unsigned long)N);
    
  /* Release allocated data */
  SDIS(device_ref_put(dev));
  SDIS(scene_ref_put(scn));
  SDIS(medium_ref_put(solid));
  SDIS(medium_ref_put(fluid));
  SDIS(interface_ref_put(interface_adiabatic));
  SDIS(interface_ref_put(interface_T0));
  SDIS(interface_ref_put(interface_T1));
  SDIS(estimator_ref_put(estimator));

  return 0;
}
