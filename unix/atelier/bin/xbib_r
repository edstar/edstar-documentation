#!/bin/bash
#####################################################################################
#
#  Ecrit par Stephane Blanco Octobre 2017
#  stephane.blanco (at) laplace.univ-tlse.fr
#  
#
######################################################################################

########################################################################################
# xbib_r(1)                   COMMANDES D'EXTRACTION                       xbib_r(1)
#
# NOM
#       xbib_r - extraction multicritere d'une setion biblio pour un fichier .bib
#
#       - similaire en tout point a xbib a l'exception de la sortie qui est resume.
#
# SYNOPSIS
#       xbib_r key1 key2 ... keyn nom-du-fichier.bib
#
# DESCRIPTION
#       Quand elle est invoquee la commande xbib attend  :
#
#       - n mots cles pour la recherche : key1 ... keyn (n au moins egal a 1)
#
#       - le nom d'un fichier .bib sur lequel faire la recherche (nom-du-fichier.bib)
#
# EXEMPLE
#       pour trouver les sections biblio contenant 1996 et toto :
#
#          $ xbib_r 1996 toto mabiblio.bib
#
# AUTEUR
#       Stephane Blanco (stephane.blanco (at) laplace.univ-tlse.fr)
#
# VOIR AUSSI
#       xbib_author(1), xbib_year(1), xbib_title(1), xbib_journal(1)
############################
############################

nb_param=0
for param in "$@" #====== recupere dans fichinit le nom du fichier d'entree
do
fichinit=$param 
((++nb_param))
done
fich=$fichinit
((--nb_param))
#echo $nb_param
compt_param=1
######################################################################
for param in "$@"  #====== Debut de la boucle sur les mots recherches
do
if [ "$param" != "$fichinit" ] #====== exclu le nom du fichier des parametres
then

########################################################################
#====== recherche les numeros de lignes des occurences du mot dans $fich
########################################################################
nb_occur=$((grep -in $param $fich | grep -o "^[0-9]\+")| wc -l)
if (( nb_occur == 0)) 
then
exit 0
fi
(grep -in $param $fich | grep -o "^[0-9]\+") > /tmp/tmp1
declare -a mesoccurences
let i=1
while IFS=$'\n' read -r line_data; do
    mesoccurences[i]="${line_data}" # Populate array.
#    echo 'mes occur ' ${mesoccurences[i]}
    ((++i))
done < /tmp/tmp1
rm /tmp/tmp1

#######################################################################
#====== recherche les numeros de lignes des occurences de @ dans $fich
#######################################################################
nb_arrob=$((grep -in '^@' $fich | grep -o "^[0-9]\+")| wc -l)
(grep -in '^@' $fich | grep -o "^[0-9]\+") > /tmp/tmp1
declare -a mesarrobase
let i=1
while IFS=$'\n' read -r line_data; do
    mesarrobase[i]="${line_data}" # Populate array.
#    echo 'mes arrob' ${mesarrobase[i]}
    ((++i))
done < /tmp/tmp1
valfinal=$(cat $fich | wc -l)
let "mesarrobase[i]=valfinal+1"
((++nb_arrob))
rm /tmp/tmp1

#######################################################################
#====== tri des occurences en n'en gardant qu'une par section biblio
#######################################################################

let "fin_arrob=nb_arrob+1"
let "fin_occur=nb_occur+1"
iocc=1
iarr=1
let 'jarr=iarr+1'
while (( mesoccurences[$iocc] < mesarrobase[$iarr] )) ; do
#echo 'coucou'
((++iocc))
done

let 'inf=mesarrobase[$iarr]'
let 'sup=mesarrobase[$jarr]'
compt=0
compt_occur=0

while (( iocc != fin_occur)) ; 
 do
# echo $inf ${mesoccurences[$iocc]} $sup
  if (( mesoccurences[$iocc] >=  $inf )) &&  (( mesoccurences[$iocc] <  $sup )) ;
  then
	if (( compt == 0)) 
	then
#	echo 'coucou' $compt
	echo ${mesoccurences[$iocc]} >> /tmp/tmp1		
        ((++compt_occur))
        fi
	let 'compt=compt+1'
  else
    ((++iarr))   
    let 'inf=mesarrobase[$iarr]'
    let 'jarr=iarr+1'
    let 'sup=mesarrobase[$jarr]'
    ((--iocc))
    compt=0
  fi
((++iocc))
done
nb_occur=$compt_occur
declare -a mesoccurences
let i=1
while IFS=$'\n' read -r line_data; do
    mesoccurences[i]="${line_data}" 
#    echo ${mesoccurences[i]}
    ((++i))
done < /tmp/tmp1
rm /tmp/tmp1

#######################################################################
#====== resulat du tri intermediaire > dans le fichier (tampon) sortie 
#######################################################################
compt=1
let "fin=nb_arrob+1"
i=1
while [ "$i" -ne "$fin" ]; do
    cible=${mesarrobase[$i]}
    courant=${mesoccurences[$compt]}
   if (( compt <= nb_occur )) 
   then
    if (( courant >= cible  ))
    then 
    	low=${mesarrobase[$i]}    
    else 
         high=${mesarrobase[$i]}   
         let "haute=high-1"
	 sed -n ''"$low","$haute"'p' $fich > sortie_tmp_tmp
         cat sortie_tmp_tmp >> sortie_tmp
       if (( compt_param == nb_param )) 
       then
         pr1=$(grep -i '^@' sortie_tmp_tmp)
	 pr2=$(grep -i 'author' sortie_tmp_tmp | cut -d, -f1 | cut -d{ -f2)
         pr3=$(grep -i 'year' sortie_tmp_tmp | cut -d{ -f2 | cut -d} -f1)
         pr4=$(grep -i 'title' sortie_tmp_tmp | cut -d{ -f2 | cut -d' ' -f1,2,3)
         pr5=$(grep -i 'journal' sortie_tmp_tmp | cut -d{ -f2 | cut -d' ' -f1,2,3,4) 
         echo $pr1 [$pr2 et al.] [$pr3] [$pr4 ..] [$pr5 ..] >> sortie_reduite
       fi
	 ((compt++))
         if (( compt <= nb_occur )) 
	 then
        	courant=${mesoccurences[$compt]}
        	low=${mesarrobase[$i]}
         fi
    fi
   fi
((++i))
done
sed '' sortie_tmp > sortie
#echo @fin >> sortie
fich="sortie"
rm sortie_tmp
rm sortie_tmp_tmp
fi
((++compt_param))
done # fin de la boucle sur les mots recherches

echo nombre occurences :  $nb_occur
#tail -n $nb_occur sortie_reduite
cat sortie_reduite

rm sortie_reduite
rm sortie

exit 0

