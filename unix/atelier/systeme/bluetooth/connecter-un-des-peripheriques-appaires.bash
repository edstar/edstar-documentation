#!/bin/bash
# On lit et on affiche la liste des periphériques appairés
source "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"
numero="0"
for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
do
  numero="$( echo "$numero + 1" | bc )"
  echo "$numero : $nom" 
done
# On demande le périphérique bluetooth a connecter
echo "Les périphériques bluetooth ci-dessus sont disponibles. Lequel souhaitez-vous connecter ? [entrez le numéro du début de ligne]"
read reponse
reponseValable="0"
numero="0"
for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
do
  numero="$( echo "$numero + 1" | bc )"
  if [ "$reponse" = "$numero" ]
  then
    reponseValable="1"
    nom_choisi=$nom
  fi
done
if [ "$reponseValable" = "1" ]
then
  bluetoothctl connect "${adresse_mac_peripherique_bluetooth[$nom_choisi]}"
fi
