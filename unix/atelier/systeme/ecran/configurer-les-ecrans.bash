#!/bin/bash
# On importe la liste des écrans.
source /tmp/ecran.bash
# On liste les écrans actifs.
nombreEcransActifs="0"
for numero in "${!ecran[@]}"
do
  test="$(grep -c \* "/tmp/ecran$numero.bash")"
  if [ "$test" != "0" ]
  then
    echo "$numero" : ${ecran["$numero"]} "(actif)"
    nombreEcransActifs="$( echo "$nombreEcransActifs + 1" | bc )"
  else
    echo "$numero" : ${ecran["$numero"]} "(non actif)"
  fi
done
if [ "$nombreEcransActifs" = "0" ]
then
  echo "Aucun écran n'est marqué actif. C'est peut-être du au fait que l'écran actuel utilise une résolution non conseillée (rapport de longueur modifié). Nous ne passons ici en revue que les résolutions conseillées."
fi
# On demande l'écran principal.
echo "Les écrans listés ci-dessus sont disponibles. Lequel souhaitez-vous définir comme écran principal ? [entrez le numéro du début de ligne]"
read reponse
reponseValable="0"
for i in "${!ecran[@]}"
do
  if [ "$reponse" = "$i" ]
  then
    reponseValable="1"
  fi
done
if [ "$reponseValable" = "1" ]
then
  # On lit le nom de l'écran choisi et on charge son tableau de résolutions et de fréquences.
  numeroEcranPrincipal="$reponse"
  nomEcranPrincipal="${ecran["$numeroEcranPrincipal"]}"
  nomDuTableauAssociatif="ecran$numeroEcranPrincipal"
  source "/tmp/$nomDuTableauAssociatif.bash"
  # On déclare un tableau associatif temporaire pour les résolutions de l'écran choisi.
  declare -A lesResolutions
  eval "listeDesResolutions=\${!$nomDuTableauAssociatif[@]}"
  numero="0"
  for resolution in $listeDesResolutions
  do
    # On incrémente le numéro de résolution.
    numero="$( echo "$numero + 1" | bc )"
    # On associe le numero de résolution à la résolution.
    lesResolutions["$numero"]="$resolution"
    # On affiche la résolution précédée de son numéro.
    echo "$numero : $resolution"
  done
  # On demande à l'utilisateur de choisir une résolution.
  echo "Les résolutions listées ci-dessus sont disponibles. Laquelle souhaitez-vous retenir ? [entrez le numéro du début de ligne]"
  read reponse
  reponseValable="0"
  for numero in "${!lesResolutions[@]}"
  do
    if [ "$reponse" = "$numero" ]
    then
      reponseValable="1"
    fi
  done
  if [ "$reponseValable" = "1" ]
  then
    # On lit la résolution associée au numéro donné par l'utilisateur.
    resolutionEcranPrincipal="${lesResolutions["$reponse"]}"
    # On enlève le `x` de façon à pouvoir séparer les résolutions horizontales et verticales. On enlève aussi les `i` (pour `interlacé`) et les `p`.
    tmp="$(echo "$resolutionEcranPrincipal" | sed -e $'s/x/\ /g' | sed -e $'s/i/\ /g'| sed -e $'s/p/\ /g')"
    resxEcranPrincipal="$(echo "$tmp" | awk '{print $1}')"
    resyEcranPrincipal="$(echo "$tmp" | awk '{print $2}')"
    # On active l'écran choisi, avec la résolution choisie, en le déclarant comme écran principal.
    eval "xrandr --output $nomEcranPrincipal --mode $resolutionEcranPrincipal --pos 0x0 --scale 1x1 --primary"
  fi
  # On enlève l'écran principal du tableau des écrans.
  unset ecran["$numeroEcranPrincipal"]
  # On détermine le nombre d'écrans restants et on demande à l'utilisateur si il souhaite configurer un autre écran.
  nombreEcransRestants="${#ecran[@]}"
  configuration="1"
  while [ "$nombreEcransRestants" != "0" ] && [ "$configuration" = "1" ]
  do
    # On affiche les écrans restants.
    for numero in "${!ecran[@]}"
    do
      test="$(grep -c \* "/tmp/ecran$numero.bash")"
      if [ "$test" != "0" ]
      then
        echo "$numero" : ${ecran["$numero"]} "(actif)"
      else
        echo "$numero" : ${ecran["$numero"]} "(non actif)"
      fi
    done
    echo "Les écrans listés ci-dessus sont disponibles. Souhaitez-vous en configurer un ? [si oui entrez le numéro du début de ligne, entrez '0' sinon]"
    read reponse
    reponseValable="0"
    for i in "${!ecran[@]}"
    do
      if [ "$reponse" = "$i" ]
      then
        reponseValable="1"
      fi
    done
    if [ "$reponseValable" = "1" ]
    then
      # On lit le nom de l'écran choisi et on charge son tableau de résolutions et de fréquences.
      numeroEcran="$reponse"
      nomEcran="${ecran["$numeroEcran"]}"
      nomDuTableauAssociatif="ecran$numeroEcran"
      source "/tmp/$nomDuTableauAssociatif.bash"
      # On déclare un tableau associatif temporaire pour les résolutions de l'écran choisi.
      declare -A lesResolutions
      eval "listeDesResolutions=\${!$nomDuTableauAssociatif[@]}"
      numero="0"
      for resolution in $listeDesResolutions
      do
        # On incrémente le numéro de résolution.
        numero="$( echo "$numero + 1" | bc )"
        # On associe le numero de résolution à la résolution.
        lesResolutions["$numero"]="$resolution"
        # On affiche la résolution précédée de son numéro.
        echo "$numero : $resolution"
      done
      # On demande à l'utilisateur de choisir une résolution.
      echo "Les résolutions listées ci-dessus sont disponibles. Laquelle souhaitez-vous retenir ? [entrez le numéro du début de ligne]"
      read reponse
      reponseValable="0"
      for numero in "${!lesResolutions[@]}"
      do
        if [ "$reponse" = "$numero" ]
        then
          reponseValable="1"
        fi
      done
      if [ "$reponseValable" = "1" ]
      then
        # On lit la résolution associée au numéro donné par l'utilisateur.
        resolution="${lesResolutions["$reponse"]}"
        # On enlève le `x` de façon à pouvoir séparer les résolutions horizontales et verticales. On enlève aussi les `i` (pour `interlacé`) et les `p`.
        tmp="$(echo "$resolution" | sed -e $'s/x/\ /g' | sed -e $'s/i/\ /g'| sed -e $'s/p/\ /g')"
        resx="$(echo "$tmp" | awk '{print $1}')"
        resy="$(echo "$tmp" | awk '{print $2}')"
        # On demande la position souhaitée.
        echo "Par rapport à l'écran principal, cet écran secondaire peut être placé à l'une des positions suivantes :"
        echo "1 : à la même position (il joue alors le rôle d'un écran de contrôle et on ajuste la résolution pour qu'elle coïncide quitte à perdre en qualité d'affichage) ;"
        echo "2 : à gauche ;"
        echo "3 : à droite ;"
        echo "4 : au dessus ;"
        echo "5 : en dessous."
        echo "Quelle position choisissez-vous ? [entrez le numéro du début de ligne]"
        read reponse
        if [ "$reponse" = "1" ]
        then
          # On calcule les facteurs à utiliser pour l'ajustement à la résolution de l'écran principal.  
          scalex="$( echo "$resxEcranPrincipal/$resx" | bc -l )"
          scaley="$( echo "$resyEcranPrincipal/$resy" | bc -l )"
          # On active l'écran choisi, avec la résolution choisie, en le configurant comme un écran de controle (même image, ajustée à la résolution). 
          eval "xrandr --output $nomEcran --mode $resolution --pos 0x0 --scale "$scalex"x"$scaley""
        fi
        if [ "$reponse" = "2" ]
        then
          # On active l'écran choisi, avec la résolution choisie, en le positionnant à gauche de l'écran principal. 
          posx="-$resx"
          eval "xrandr --output $nomEcran --mode $resolution --pos "$posx"x0 --scale 1x1"
        fi
        if [ "$reponse" = "3" ]
        then
          # On active l'écran choisi, avec la résolution choisie, en le positionnant à droite de l'écran principal. 
          posx="$resxEcranPrincipal"
          eval "xrandr --output $nomEcran --mode $resolution --pos "$posx"x0 --scale 1x1"
        fi
        if [ "$reponse" = "4" ]
        then
          # On active l'écran choisi, avec la résolution choisie, en le positionnant au dessus de l'écran principal. 
          posy="-$resy"
          eval "xrandr --output $nomEcran --mode $resolution --pos 0x"$posy" --scale 1x1"
        fi
        if [ "$reponse" = "5" ]
        then
          # On active l'écran choisi, avec la résolution choisie, en le positionnant en dessous de l'écran principal. 
          posy="$resyEcranPrincipal"
          eval "xrandr --output $nomEcran --mode $resolution --pos 0x"$posy" --scale 1x1"
        fi
      fi
      # On enlève l'écran du tableau des écrans.
      unset ecran["$numeroEcran"]
      # On détermine le nombre d'écrans restants.
      nombreEcransRestants="${#ecran[@]}"
    else
      # La réponse n'est pas valable : on arrète la configuration.
      configuration="0"
    fi
  done
fi
