#!/bin/bash
nomPartition="$1"

# On démonte la partition (au cas où elle n'aurait pas été démontée par l'utilisateur avant d'enlever le périphérique).
/bin/umount "/dev/$nomPartition"

# On efface le fichier d'information correspondant à la partition.
rm "/tmp/disqueExterne-$nomPartition.bash"

# Debug
#/bin/echo "enlever partition : $@" >> /tmp/zzz-99-test.txt
