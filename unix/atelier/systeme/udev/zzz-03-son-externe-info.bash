#!/bin/bash
nom=$1
nomManufacturer=$2
nomProduct=$3
declare -A carteSon
# Si le tableau associatif existe déjà, on le charge.
if [ -e "/tmp/carteSonExterne.bash" ]
then
  source /tmp/carteSonExterne.bash
fi
if [ "$nomManufacturer" != "" ] && [ "$nomProduct" != "" ]
then
  # Si le nom du périphérique commence par `card` on l'ajoute au tableau associatif.
  test="$(echo "${nom:0:4}")"
  if [ "$test" = "card" ]
  then
    carteSon["$nom"]="$nomManufacturer $nomProduct"
  fi
fi
# On exporte le tableau associatif et on accorde les droits de lecture, d'ecriture et d'exécution à tous les utilisateurs.
declare -p carteSon > /tmp/carteSonExterne.bash
chmod 666 /tmp/carteSonExterne.bash

# Debug
#echo "son-info $1 $2 $3" >> /tmp/toto.txt
