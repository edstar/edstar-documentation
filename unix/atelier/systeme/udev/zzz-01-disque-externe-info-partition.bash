#!/bin/bash
nomPartition="$1"
nomPeripherique="$2"
numeroPartition="$3"
nomVendeur="$4"
nomModele="$5"
nomNom="$6"
nomSystemeDeFichier="$7"

# On écrit les informations dans un tableau associatif.
declare -A disque

disque["peripherique"]="$nomPeripherique"
disque["partition"]="$numeroPartition"
disque["vendeur"]="$nomVendeur"
disque["modele"]="$nomModele"
disque["nom"]="$nomNom"
disque["systemeDeFichier"]="$nomSystemeDeFichier"
disque["lien"]="$nomPartition"

# Au cas où la partition serait déjà montée, on cherche le nom de son point de montage.
nomPointDeMontage="$(findmnt -n -o TARGET /dev/"$nomPartition")"
if [ "$nomPointDeMontage" != "" ]
then
  /bin/umount "nomPointDeMontage"
  nomPointDeMontage="$(findmnt -n -o TARGET /dev/"$nomPartition")"
  if [ "$nomPointDeMontage" != "" ]
  then
    disque["pointDeMontage"]="$nomPointDeMontage"
  else
    disque["pointDeMontage"]="vide"
  fi
else
  disque["pointDeMontage"]="vide"
fi

# On écrit le tableau associatif dans le fichier d'information et on donne les droits de lecture et d'écriture à tous les utilisateurs.
declare -p disque > "/tmp/disqueExterne-$nomPartition.bash"
chmod 666 "/tmp/disqueExterne-$nomPartition.bash"

# Si le point de montage n'existe pas, on le crée.
if [ ! -e "/mnt/$nomPartition" ]
then
  mkdir "/mnt/$nomPartition"
fi

# Debug
#/bin/echo "info partition : $@" >> /tmp/zzz-99-test.txt
