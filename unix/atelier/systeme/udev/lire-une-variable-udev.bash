#!/bin/bash
# On regarde si il y a un paramètre d'entrée.
if [ $# -eq 0 ]
then
# Si il n'y a pas de paramètre d'entrée, on renvoie le mot-clé `vide`.
  variableUdev="vide"
else
# Si il y a une variable d'entrée, on enlève les blancs et les tabulations et on teste si le résultat est vide.
  variableUdev="$(echo "$1" | sed 's/[[:blank:]]//g')"
  if [ "$variableUdev" = "" ]
  then
  # Si le résultat est vide (il n'y avait aucun autre caractère que des blancs et des tabulations), on renvoie le mot-clé `vide`.
    variableUdev="vide"
  fi
fi
# Sortie
/bin/echo "$variableUdev"

# Debug
#/bin/echo "lire-une-variable-udev : $@ $variableUdev" >> /tmp/zzz-99-test.txt
