#!/bin/bash
# On lit les informations sur le matériel vu par alsa.
#aplay -l | awk -F "," '/carte/ {print $1}' | sed 's/://' | sed 's/carte //' | awk '{for(i=3;i<=NF;i++) printf $i" "; print ""}' | sed 's/\[//' | sed 's/\]//' > /tmp/materielCarteSon.txt
aplay -l | awk -F"]" '/carte/ {print $1}' | awk -F"[" '{print $2}' > /tmp/materielCarteSon.txt
OLDIFS=$IFS
IFS=$'\n'
materielCarteSon=( $(cat /tmp/materielCarteSon.txt) )
IFS=$OLDIFS
# On lit les noms utilisés par alsa
aplay -l | awk '/carte/ {print $3}' > /tmp/nomCarteSon.txt
OLDIFS=$IFS
IFS=$'\n'
nomCarteSon=( $(cat /tmp/nomCarteSon.txt) )
IFS=$OLDIFS
# On enlève les redonndances et on ecrit le résultat dans un vecteur associatif.
declare -A carteSon
nombreDeLignes="${#nomCarteSon[@]}"
for (( i=0; i < "$nombreDeLignes"; i++ ))
do
  carteSon["${nomCarteSon[$i]}"]="${materielCarteSon[$i]}"
done
# On enlève les cartes qui ne sont pas reconnues.
source /usr/share/alsa/carteSonInterne.bash
source "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/carteSonUsbReconnue.bash
for nom in "${!carteSon[@]}"
do
  # Pour les cartes son interne on teste le nom vu par alsa car le noyau commencera par ces cartes et conservera toujours les mêmes noms que ceux attribués lors de l'installation du système.
  nomInterneReconnu=0
  if [[ -v "carteSonInterne[$nom]" ]]
  then
    nomInterneReconnu=1
  fi
  # Pour les cartes son externes on teste le materiel car le nom vu par alsa peut changer selon les cartes branchées et l'ordre de leur branchement. C'est pour cette raison que le nom utilisé dans le fichier carteSonUsbReconnue.bash est un nom choisi par nous lors du travail sur la carte, notamment lors de la préparation des fichiers asoundrc associés, un nom qui peut être différent de celui vu par alsa. 
  materielExterneReconnu=0
  for nomUsb in "${!carteSonUsbReconnue[@]}"
  do
    if [ "${carteSon[$nom]}" = "${carteSonUsbReconnue[$nomUsb]}" ]
    then
      materielExterneReconnu=1
    fi
  done
  # Si les deux tests sont négatifs on enlève la carte.
  if [ "$nomInterneReconnu" = "0" ] && [ "$materielExterneReconnu" = "0" ]
  then
    unset carteSon["$nom"]
  fi
done
# On lit la liste des periphériques bluetooth appairés, on ne retient que les cartes son, on regarde si elles sont connectées, et on les ajoute au vecteur associatif.
source "$HOME/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash"
for nom in "${!adresse_mac_peripherique_bluetooth[@]}"
do
  icon=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Icon/ {print $2}' )
  connected=$( bluetoothctl info "${adresse_mac_peripherique_bluetooth[$nom]}" | awk '/Connected/ {print $2}' )
  # Si le periphérique est un périphérique audio et si il est connecté, alors on le rajoute à la liste des cartes son disponibles
  if [ "$icon" = "audio-card" ] && [ "$connected" = "yes" ]
  then
    carteSon["$nom"]="bluetooth"
  fi
  if [ "$icon" = "audio-headset" ] && [ "$connected" = "yes" ]
  then
    carteSon["$nom"]="bluetooth"
  fi
done
# On écrit le vecteur associatif dans un fichier
declare -p carteSon > /tmp/carteSon.bash
# On affiche les cartes
echo "Les cartes sons :"
for nom in "${!carteSon[@]}"
do
  echo Nom : "$nom" - Matériel : "${carteSon[$nom]}"
done
