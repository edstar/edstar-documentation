#/bin/bash
# On teste si le fichier ~/.asoundrc est bien un lien symbolique (sinon on ne l'écrase pas).
if [ ! -L ~/.asoundrc ]
then
  echo "Le fichier ~/.asoundrc n'est pas un lien symbolique. On conserve ce fichier et on ne lance donc aucune configuration. Si vous souhaitez lancer la configuration, il faut d'abord déplacer ce fichier."
else
  # On importe la liste des cartes son.
  source /tmp/carteSon.bash
  nombre="${#carteSon[@]}"
  if [ "$nombre" = "0" ]
  then
    echo "Aucune carte son n'est disponible."
  else
    # On liste les cartes son.
    numero="0"
    for nom in "${!carteSon[@]}"
    do
      numero="$( echo "$numero + 1" | bc )"
      echo "$numero" : "$nom" "${carteSon["$nom"]}"
    done
    echo "Les cartes son listées ci-dessus sont disponibles. Souhaitez-vous utiliser une de ces cartes~? [entrez le numéro du début de ligne, sinon entrez 0]" 
    read reponse
    reponseValable="0"
    numero="0"
    for nom in "${!carteSon[@]}"
    do
      numero="$( echo "$numero + 1" | bc )"
      if [ "$reponse" = "$numero" ]
      then
        reponseValable="1"
        nomCarte="$nom"
      fi
    done
    if [ "$reponseValable" = "1" ]
    then
      # On regarde si la carte son choisie est une carte son interne (en testant le nom).
      source /usr/share/alsa/carteSonInterne.bash
      if [[ -v "carteSonInterne[$nomCarte]" ]]
      then
        rm ~/.asoundrc
        ln -s /usr/share/alsa/asoundrc-PCH ~/.asoundrc 
      fi
      # On regarde si la carte son choisie est une carte son usb (en testant le matériel).
      source "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/carteSonUsbReconnue.bash
      materielExterneReconnu=0
      for nomUsb in "${!carteSonUsbReconnue[@]}"
      do
        if [ "${carteSon[$nomCarte]}" = "${carteSonUsbReconnue[$nomUsb]}" ]
        then
          materielExterneReconnu=1
          nomUsbReconnu="$nomUsb"
        fi
      done
      if [ "$materielExterneReconnu" = "1" ]
      then
        cat "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/asoundrc-"$nomUsbReconnu" | sed "s/$nomUsbReconnu/$nomCarte/" > ~/.asoundrc-"$nomCarte"
        rm ~/.asoundrc
        ln -s ~/.asoundrc-"$nomCarte" ~/.asoundrc 
        echo usb
      fi
      # On regarde si la carte son choisie est une carte son bluetooth.
      if [ "${carteSon["$nomCarte"]}" = "bluetooth" ]
      then
        source ~/.config/bluetooth/adresse_mac_peripherique_bluetooth.bash
        cat "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/asoundrc-bluetooth | sed "s/adresse_mac_peripherique_bluetooth/${adresse_mac_peripherique_bluetooth["$nomCarte"]}/" > ~/.asoundrc-bluetooth-"${adresse_mac_peripherique_bluetooth["$nomCarte"]}"
        rm ~/.asoundrc
        ln -s ~/.asoundrc-bluetooth-"${adresse_mac_peripherique_bluetooth["$nomCarte"]}" ~/.asoundrc
        echo bluetooth
      fi
    fi
  fi
fi
