#/bin/bash
# On teste si le fichier ~/.asoundrc est bien un lien symbolique (sinon on ne l'écrase pas).
if [ ! -L ~/.asoundrc ]
then
  echo "Le fichier ~/.asoundrc n'est pas un lien symbolique. On conserve ce fichier et on ne lance donc aucune configuration. Si vous souhaitez lancer la configuration, il faut d'abord déplacer ce fichier."
else
  rm ~/.asoundrc
  ln -s "$EDSTARDOCUMENTATION"/unix/atelier/systeme/audio/asoundrc-H5 ~/.asoundrc 
fi
