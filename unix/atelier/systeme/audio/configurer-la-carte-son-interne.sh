#/bin/bash
# On teste si le fichier ~/.asoundrc est bien un lien symbolique (sinon on ne l'écrase pas).
if [ ! -L ~/.asoundrc ]
then
  echo "Le fichier ~/.asoundrc n'est pas un lien symbolique. On conserve ce fichier et on ne lance donc aucune configuration. Si vous souhaitez lancer la configuration, il faut d'abord déplacer ce fichier."
else
  # On teste si le fichier /usr/share/alsa/asoundrc-interne a été créé lors de l'intsallation de la machine.
  if [ ! -e /usr/share/alsa/asoundrc-interne ]
  then
    echo "Le fichier /usr/share/alsa/asoundrc-interne n'existe pas. On ne peut donc pas configurer la carte son interne. Ce fichier aurait du être créé lors de l'installation de la machine. Voir votre administrateur système."
  else
    rm ~/.asoundrc
    ln -s /usr/share/alsa/asoundrc-interne ~/.asoundrc
  fi
fi
