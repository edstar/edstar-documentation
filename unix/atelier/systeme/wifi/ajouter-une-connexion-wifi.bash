#!/bin/bash
# On liste les connexions déjà déclarées
wpa_cli list_networks > /tmp/connexions-wifi-declarees.txt
# On compte les connexions wifi déjà déclarées
nombreDeclare="$(cat /tmp/connexions-wifi-declarees.txt | wc -l)"
nombreDeclare="$( echo "$nombreDeclare - 2" | bc )"
# On lit les noms des connexions déjà déclarées
declare -A nomDeclare
indiceDeclare="0"
for i in $(seq 1 $nombreDeclare)
do
  indiceDeclare="$( echo "$indiceDeclare + 1" | bc )"
  # On enlève les deux lignes d'entète
  numeroLigne="$( echo "$i + 2" | bc )"
  # On lit la ligne
  ligne="$(sed -n "$numeroLigne p" /tmp/connexions-wifi-declarees.txt)"
  # On lit le nom qui est le second mot de la ligne
  nomDeclare["$indiceDeclare"]="$(echo $ligne | awk '{print $2}')"
done
# On scanne les émetteurs disponibles
wpa_cli scan > /dev/null
# On liste les émetteurs disponibles
wpa_cli scan_result > /tmp/connexions-wifi-disponibles.txt
# On compte les émetteurs disponibles
nombreDisponible="$(cat /tmp/connexions-wifi-disponibles.txt | wc -l)"
nombreDisponible="$( echo "$nombreDisponible - 2" | bc )"
# On lit les noms des émetteurs disponibles et on conserve ceux qui ne sont pas encore déclarés
declare -A nom
nombre="0"
for i in $(seq 1 $nombreDisponible)
do
  # On enlève les deux lignes d'entète
  numeroLigne="$( echo "$i + 2" | bc )"
  # On lit la ligne
  ligne="$(sed -n "$numeroLigne p" /tmp/connexions-wifi-disponibles.txt)"
  # On lit le nom qui est le cinquième mot de la ligne
  nomDisponible="$(echo $ligne | awk '{print $5}')"
  # On regarde si ce nom est déjà déclaré
  dejaDeclare="0"
  for j in ${nomDeclare[@]}
  do
    if [ "$nomDisponible" = "$j" ]
    then
      dejaDeclare="1"
    fi
  done
  # Si il n'est pas encore déclaré, on l'archive dans la liste des noms des connexions supplémentaires possibles
  if [ "$dejaDeclare" = "0" ]
  then
    nombre="$( echo "$nombre + 1" | bc )"
    nom["$nombre"]="$nomDisponible"
  fi
done
# On affiche les nouvelles connexions possibles.
if [ "$nombre" = "0" ]
then
  echo "Il n'y a aucune nouvelle connexion wifi possible."
else
  echo "Nouvelles connexions wifi disponibles (en dehors de celles déjà déclarées) :"
  for i in "${!nom[@]}"
  do
    echo "$i : ${nom["$i"]}"
  done
  # On demande le choix de connexion
  echo "Quelle nouvelle connexion voulez-vous ajouter dans la liste ci-dessus ? [entrez le numéro du début de ligne]"
  read reponse
  # On vérifie que le choix est valable.
  reponseValable="0"
  for i in "${!nom[@]}"
  do
    if [ "$reponse" = "$i" ]
    then
      reponseValable="1"
    fi
  done
  if [ "$reponseValable" = "1" ]
  then
    # On ajoute un numéro de connexion
    sudo wpa_cli add_network > /tmp/numero-de-connexion-wifi-ajoute.txt
    # On récupère le nouveau numéro de connexion
    ligne="$(sed -n "2 p" /tmp/numero-de-connexion-wifi-ajoute.txt)"
    numeroConnexion="$(echo $ligne | awk '{print $1}')"
    # On crée la connexion
    sudo wpa_cli set_network "$numeroConnexion" ssid "\"${nom["$reponse"]}\""
    # On rentre le mot de passe
    echo "Saisir le mot de passe :"
    read -s reponse
    sudo wpa_cli set_network "$numeroConnexion" psk "\"$reponse\""
    # On valide la connexion
    sudo wpa_cli enable_network "$numeroConnexion"
    # On sauvegarde la configuration
    sudo wpa_cli save_config
    # On relance `wpa_supplicant`
    sudo rc-service wpa_supplicant restart
  fi
fi
