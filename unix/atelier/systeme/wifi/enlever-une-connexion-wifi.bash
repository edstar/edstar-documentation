#!/bin/bash
# On liste les connexions déjà déclarées
wpa_cli list_networks > /tmp/connexions-wifi-declarees.txt
# On compte les connexions wifi déjà déclarées
nombreDeclare="$(cat /tmp/connexions-wifi-declarees.txt | wc -l)"
nombreDeclare="$( echo "$nombreDeclare - 2" | bc )"
# On lit les noms des connexions déjà déclarées
declare -A nomDeclare
for i in $(seq 1 $nombreDeclare)
do
  # On enlève les deux lignes d'entète
  numeroLigne="$( echo "$i + 2" | bc )"
  # On lit la ligne
  ligne="$(sed -n "$numeroLigne p" /tmp/connexions-wifi-declarees.txt)"
  # On lit le numero de la connexion, qui est le premier mot de la ligne.
  numeroDeclare="$(echo $ligne | awk '{print $1}')"
  # On lit le nom de la connexion, qui est le second mot de la ligne.
  nomDeclare["$numeroDeclare"]="$(echo $ligne | awk '{print $2}')"
done
if [ "nombreDeclare" = "0" ]
then
  echo "Il n'y a aucun connexion wifi déclarée."
else
  # On affiche les noms des connexions déjà déclarées.
  echo "Connexions wifi déclarées :"
  for i in "${!nomDeclare[@]}"
  do
    echo "$i : ${nomDeclare["$i"]}"
  done
  # On demande le numero de la connexion à enlever.
  echo "Quelle connexion voulez-vous enlever dans la liste ci-dessus ? [entrez le numéro du début de ligne]"
  read reponse
  # On vérifie que le choix est valable.
  reponseValable="0"
  for i in "${!nomDeclare[@]}"
  do
    if [ "$reponse" = "$i" ]
    then
      reponseValable="1"
    fi
  done
  if [ "$reponseValable" = "1" ]
  then
    echo "reponse valable $reponse"
    # On retire la connexion
    sudo wpa_cli remove_network "$reponse"
    # On sauvegarde la configuration
    sudo wpa_cli save_config
    # On relance `wpa_supplicant`
    sudo rc-service wpa_supplicant restart
  fi
fi
