#!/bin/bash
# On liste les fichiers d'information existant dans le répertoire `/tmp` et on les met dans la variable `fichiersInfo`.
fichiersInfo=($(ls /tmp/disqueExterne-*.bash 2>/dev/null))
if [ "${#fichiersInfo[@]}" = "0" ]
then
# Il n'y a aucun fichier d'information donc aucun disque externe n'est branché
  echo "Il n'y a aucun disque externe branché sur l'ordinateur"
else
# Il y a des fichiers d'information : on les passe en revue et on repère tous ceux qui indique une partition non encore montée.
  declare -A disque
  declare -A fichiersInfoMontables
  nombre="0"
  for i in "${fichiersInfo[@]}"
  do
  # Boucle sur les fichiers d'information
    source "$i"
    # On teste si il s'agit d'une partition et si elle n'est pas encore montée
    if [ "${disque[partition]}" != "vide" ] && [ "${disque[pointDeMontage]}" = "vide" ]
    then
      # On incrémente le compteur
      nombre="$( echo "$nombre + 1" | bc )"
      # On archive le nom du fichier d'information dans un tableau associatif [numero -> nom du fichier]
      numero="$nombre"
      fichiersInfoMontables["$numero"]="$i"
      # On affiche les informations sur la partition
      echo "$numero : /dev/${disque[lien]} est disponible au montage. [ nom : ${disque[nom]} , modèle : ${disque[modele]} , vendeur : ${disque[vendeur]} , système de fichier : ${disque[systemeDeFichier]} ]"
    fi
  done
  if [ "$nombre" = 0 ]
  then
    echo "Aucune partition n'est disponible : soit les disques branchés n'ont pas de partition, soit les partitions sont déjà montées. Vous pouvez éventuellement voir les partitions déjà montées (et éventuellement les démonter) avec la commande demonter-un-disque-externe.bash."
  else
    echo "Quelle partition voulez-vous monter dans la liste ci-dessus ? [entrez le numéro du début de ligne]"
    read reponse
    reponseValable=0
    for i in "${!fichiersInfoMontables[@]}"
    do
      if [ "$reponse" = "$i" ]
      then
        reponseValable=1
      fi
    done
    if [ "$reponseValable" = "1" ]
    then
      # On lit le fichier d'information de la partition choisie
      source "${fichiersInfoMontables[$reponse]}"
      # On vérifie que le système de fichier fait partie des systèmes que l'on sait monter
      if [ "${disque[systemeDeFichier]}" = "ext4" ] || [ "${disque[systemeDeFichier]}" = "vfat" ]
      then
      # On monte les partitions avec `sudo` (il faut que `mount` fasse partie des commandes autorisées pour cet utilisateur), en passant éventuellement des options de montage pour la gestion des droits d'accès. 
        if [ "${disque[systemeDeFichier]}" = "vfat" ]
        then
        # Pour `vfat` on passe l'utilisateur et le groupe.
          sudo /bin/mount -o uid="$(id -u $USER)" -o gid="$(id -g $USER)" "/dev/${disque[lien]}" "/mnt/${disque[lien]}"
         fi
        if [ "${disque[systemeDeFichier]}" = "ext4" ]
        then
        # Pour `ext4` on ne passe rien car c'est géré pas le système de fichier lui-même.
          sudo /bin/mount "/dev/${disque[lien]}" "/mnt/${disque[lien]}"
        fi
        # On écrit le point de montage dans le fichier d'information
        disque[pointDeMontage]="/mnt/${disque[lien]}"
        declare -p disque > "${fichiersInfoMontables[$reponse]}"
      else
        echo "Le système de fichier est ${disque[systemeDeFichier]} : il n'est pas pris en charge et la partition ne peut donc pas être montée."
      fi
    fi
  fi
fi
