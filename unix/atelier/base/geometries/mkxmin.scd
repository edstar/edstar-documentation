mkxmin(1e)

# NAME
mkxmin - Utilisation de mkxmin

# DESCRIPTION
*mkxmin* est une commande permettant de créer un fichier au format *stl* correspondant à une surface parallélépipédique située (dans un repère cartésien Oxyz) dans le plan _x=xmin_, entre les cooredonnées _ymin_ et _ymax_ en y et entre les coordonnées _zmin_ et _zmax_ en z. Les normales sont tournées vers les x décroissants.

# UTILISATION
Pour _xmin=4_, _ymin=2_, _ymax=6_, _zmin=5_, _zmax=7_ :
```
lafrier$  mkxmin 4 2 6 5 7
```

# VOIR AUSSI
*mkxmin-inv*(1e), *mkxmax*(1e), *mkxmax-inv*(1e), *mkymin*(1e), *mkymin-inv*(1e), *mkymax*(1e), *mkymax-inv*(1e), *mkzmin*(1e), *mkzmin-inv*(1e), *mkzmax*(1e), *mkzmax-inv*(1e)
