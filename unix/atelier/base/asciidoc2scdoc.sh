#!/bin/sh

# Ce script convertit les sources d'un man écrit en asciidoc en source scdoc(5)
# via une succession de commandes sed dont l'objet de chacune d'entre elles est
# décrit ci-après :
#
#   - Supprime la ligne qui souligne le titre du man ;
#   - Converti les sauts de lignes explicites composé d'un espace et du
#     caractère '+' par deux caractères '+' ;
#   - Supprime les espaces en fin de lignes ;
#   - Remplace toutes les occurrences du caractère "accent grave" (`) par le
#     caractère "souligné" (_) ;
#   - Marque l'ouverture d'un bloc de texte verbatim par trois caractères
#     "accent grave" (```) à la place de "[...]" souligné de 4 tirets
#     courts ;
#   - Marque la fermeture d'un bloc de texte verbatim par trois caractères
#     "accent grave " (```) à la place de 4 tirets courts ;
#   - Précède d'un croisillon (#) les titres de section déclarés par un
#     souligné de tirets courts et supprime ledit souligné ;
#   - Précède de deux croisillons (##) les titres de sous-section déclarés par
#     un souligné de tildes (~) et supprime ledit souligné ;
#   - Précède de trois croisillons (###) les titres de sous-section déclarés
#     par un souligné d'accents circonflexes (^) et supprime ledit souligné.
#     Cette notation n'est pas une notation valide pour scdoc qui ne supporte
#     que 2 niveaux de sections. Cette règle ne sert ici qu'à générer une
#     erreur pour que l'auteur puisse modifier son texte en conséquence de
#     l'erreur détectée ;
#   - Pour le texte hors bloc verbatim, remplace par une tabulation les espaces
#     en début de ligne qui précèdent un tiret court ;
#   - Pour le texte hors bloc verbatim, remplace par une tabulation chaque
#     début de ligne commençant par 2 espaces ou plus ;
#   - Détecte les lignes qui marquent un nouvel élément dans une liste
#     mise en retrait, càd une ligne commençant par un tiret court précédé
#     d'une tabulation. Si la ligne suivante est elle aussi mise en retrait
#     d'une ou plusieurs tabulations mais ne débute pas par un tiret court,
#     elle est alors considérée comme étant la suite du texte de l'élément
#     courant, et ce faisant est mise en retrait d'une tabulation suivie de
#     deux espaces ;
#   - Détecte les lignes qui marquent la continuité de texte d'un élément dans
#     une liste mise en retrait, càd une ligne qui débute par une tabulation et
#     deux espaces. Si la ligne suivante est elle aussi mise en retrait d'une
#     ou plusieurs tabulations mais ne débute pas par un tiret court, elle est
#     alors considérée comme faisant partie du même élément de la liste, et ce
#     faisant, est elle aussi mise en retrait d'une tabulation suivie de deux
#     espaces.

# shellcheck disable=SC2016
  sed '/^[=]\{1,\}$/d' \
| sed 's/[[:space:]]\{1\}+$/++/' \
| sed 's/[[:space:]]\{1,\}$//' \
| sed 's/`/_/g' \
| sed '/^\[[^]]\{1,\}\]$/{N;s/^\[[^]]\{1,\}\]\n----$/```/}' \
| sed '/^```$/,/^----$/s/^----$/```/' \
| sed '/^[[:alnum:][:space:]&.,/*_()'\''-]\{1,\}$/{N;s/^\([[:alnum:][:space:]&.,/*_()'\''-]\{1,\}\)\n[-]\{1,\}$/# \1/}' \
| sed '/^[[:alnum:][:space:]&.,/*_()'\''-]\{1,\}$/{N;s/^\([[:alnum:][:space:]&.,/*_()'\''-]\{1,\}\)\n[~]\{1,\}$/## \1/}' \
| sed '/^[[:alnum:][:space:]&.,/*_()'\''-]\{1,\}$/{N;s/^\([[:alnum:][:space:]&.,/*_()'\''-]\{1,\}\)\n[\^]\{1,\}$/### \1/}' \
| sed '/^```$/,/^```$/!s/^[ ]\{1,\}-/\t-/' \
| sed '/^```$/,/^```$/!s/^\([ ]\{2,\}\)/\t/g' \
| sed '/\t-/{N;s/\n\t\{1,\}\([^-]\)/\n\t  \1/}' \
| sed '/\t  /{N;s/\n\t\{1,\}\([^-]\)/\n\t  \1/}'
