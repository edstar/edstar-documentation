asciidoc2scdoc(1e)

# NAME

asciidoc2scdoc - converti au format scdoc les sources d'un man écrit en asciidoc

# SYNOPSIS

asciidoc2scdoc.sh < _entrée.txt_ > _sortie.scd_

# DESCRIPTION

L'utilitaire asciidoc2scd est un script shell au standard POSIX qui lit sur
l'entrée standard les sources d'une page de manuel formatée suivant la synthaxe
*asciidoc*(7e), et écrit sur la sortie standard leur conversion au format
*scdoc*(5).

# VOIR AUSSI

*asciidoc*(7e), *scdoc*(5)

