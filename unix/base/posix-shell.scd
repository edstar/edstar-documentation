posix_shell(5e)

# NAME
posix_shell - Utilisation du shell posix

# DESCRIPTION
... Décrire l'état d'esprit de posix ...

# BOUCLES
```
#!/bin/sh
i=1
while [ "$i" -ne 10 ]
do
    echo "$i"
    i=$((i + 1))
done
```

# VOIR AUSSI
*unix*(7e)
