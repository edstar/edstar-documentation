noweb(1e)

# NAME
noweb - Utilisation de noweb

# DESCRIPTION
*noweb* est un langage de programmation lettrée. Lorsque le contenu d'un fichier *ascii* a été écrit dans ce langage, il peut être interprété avec les commandes *noweave* et *notangle*. La commande *noweave* produit un document de type article, rapport, livre, etc, sous un format _latex_, _html_, etc, qui contient le programme lettrée, c'est à dire la version pensée pour être lue par un programmeur. La commande *notangle* produit un ou plusieurs programmes sous leurs formes usuelles, c'est à dire pouvant être compilées/interprétées/executées par un calculateur.

# L'ENVIRONNEMENT

## gentoo
Sous *gentoo*(7e), chaque utilisateur doit ajuster ses variables d'environnement de la façon suivante :
```
lafrier$  echo "export PATH+=:/usr/local/noweb" >> ~/.bashrc
lafrier$  echo "export MANPATH+=:/usr/local/noweb/man" >> ~/.bashrc
lafrier$  echo "export TEXINPUTS+=:/usr/local/tex/inputs" >> ~/.bashrc
```

# UTILISATION

## L'exemple _hello.nw_
Voici un exemple de fichier _ascii_ écrit dans le langage _noweb_ :
```
\section{Hello world}

Today I awoke and decided to write
some code, so I started to write Hello World in \textsf C.

<<hello.c>>=
/*
  <<license>>
*/
#include <stdio.h>

int main(int argc, char *argv[]) {
  printf("Hello World!\n");
  return 0;
}
@
\noindent \ldots then I did the same in PHP.

<<hello.php>>=
<?php
  /*
  <<license>>
  */
  echo "Hello world!\n";
?>
@
\section{License}
Later the same day some lawyer reminded me about licenses.
So, here it is:

<<license>>=
This work is placed in the public domain.
```

La commande suivante interprète le contenu de ce fichier pour produir un document _hello.tex_ au format *latex*(7e).
```
lafrier$  noweave -index -latex hello.nw > hello.tex
```

Les deux commandes suivantes interprètent le contenu du fichier pour produire un prohramme en langage _c_ pour la première et un programme en langage _php_ pour la seconde.
```
lafrier$  notangle -Rhello.c hello.nw > hello.c
lafrier$  notangle -Rhello.php hello.nw > hello.php
```

# VOIR AUSSI
*noweb*(1), *noweb-gentoo*(7e), *latex*(7e)
