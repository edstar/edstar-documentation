drawpile(1e)

# NAME
drawpile - Utilisation de drawpile

# DESCRIPTION
*drawpile* est un logiciel permettant de dessiner de façon collaborative. Nous l'utilisons comme tableau lors des séances de travail collectif sur plusieurs sites.

# LE PROJET
Le projet Drawpile est hébergé à _https://github.com/drawpile/Drawpile_ (le wiki est très instructif). Voir aussi la page _https://drawpile.net/help/_. Drawpile est actuellement disponible sur windows, macOS et GNU/Linux (_https://drawpile.net/download/_). Les développeurs travaillent à Android, mais ce n'est encore pas disponible.

# UTILISATION

## Un utilisateur créé un tableau
Cliquer sur l'onglet session puis host et tout en bas entrer _19.432.591.761_ (par exemple, demander la bonne adresse au gestionnaire de votre serveur) dans la case remote. Ensuite le serveur demandera un username : taper celui de votre choix.

## Les autres rejoignent ce tableau

Cliquer sur l'onglet sesssion puis join et tout en bas entrer l'host _19.432.591.761_

## Remarques

Ca vaut le coup de prendre 5 min à la première utilisation pour régler son pinceau.

Il est possible de copier-coller des images sur le tableau.

Dans les outils, l'outil "inspector" associé à un symbole bleu avec un point d'intérogation au centre, permet en cliquant sur une zone du tableau de savoir qui a gribouillé.

## La taille du canvas
Si on veut avoir un canvas qui ait approximativement le rapport d'aspect d'une feuille _A4_, alors on peut choisir la taille suivante en pixels :

```
1600x2200
```

On peut ensuite essayer d'avoir la rigueur de pointer la fin de page (une petite marque) avant d'augmenter la dimension verticale, si besoin, et l'augmenter de 22OO à chaque foix (ATTENTION ! Enlever "Keep apsect ratio"). Par exemple

```
1600x4400
```

puis

```
1600x6600
```

etc. Après avoir exporté l'image au format _png_, on pourra alors la convertir en pdf avec la commande suivante :

```
rm /tmp/cropped_*.png
convert -crop 1600x2200 image.png /tmp/cropped_%d.png
for f in /tmp/cropped_*.png ; do convert "$f" "${f%.png}.jpeg" ; done
convert /tmp/cropped_*.jpeg image.pdf
```

soit (en remplaçant _image_ par le nom du fichier)

```
rm /tmp/cropped_*.png ; convert -crop 1600x2200 image.png /tmp/cropped_%d.png ; for f in /tmp/cropped_*.png ; do convert "$f" "${f%.png}.jpeg" ; done ; convert /tmp/cropped_*.jpeg image.pdf
```

# VOIR AUSSI
*drawpile-gentoo*(7e) *drawpile-ubuntu*(7e)
