mupdf(1e)

# NAME
mupdf - Utilisation de mupdf

# DESCRIPTION
*mupdf*(1e) permet de visualiser des fichier au format _pdf_.

# UTILISATION
Pour ouvrir _toto.pdf_ :

	mupdf toto.pdf

Pour recharger le fichier :

	r

Pour aller à la page 123

	123g

Pour aller à la première page

	g

Pour aller à la dernière page

	G

Pour tourner la page à gauche et à droite

	\[ ou \]
