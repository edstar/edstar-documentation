irssi(1e)

# NAME
irssi - Utilisation de irssi

# DESCRIPTION
*irssi* est un client *irc* (Internet Relay Chat, en français : « discussion relayée par Internet »). Il s'agit de permettre une communication textuelle via le réseau internet.

# UTILISATION
Pendant les journées du consortium stardis, chaque participant utilisait la commande suivante pour accéder aux discussions :
```
lafrier$  bash ./meso-utils.sh run irc
```

# VOIR AUSSI
*irssi-gentoo*(7e)

