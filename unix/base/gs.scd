gs(1e)

# NAME
gs - Ghostscript (interpretation, visualisation, modification des fichiers PDF
et PostScript)

# UTILISATION
Pour extraire les pages d'un fichier PDF (un fichier par page) :
```
lafrier $ gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=output_page_%03d.pdf -dBATCH input_file.pdf
```
