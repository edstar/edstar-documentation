gstreamer(1e)

# NAME
gstreamer - framework multimedia (bibliothèque logicielle de gestion globale du son et de l'image).

# DESCRIPTION
*gstreamer* est la bibliothèque dont nous nous servons pour gérer les flux audio et video. Il contient notamment des outils d'encodage et des outils de traitement numérique de signal (DSP). La bibliothèque est conçue à base de pipelines et chaque pipeline peut être utilisé/testé directement comme une ligne *bash* grace à la commande _gst-launch-1.0_. Elle est bien sûr compatible avec *jack*.

# A ECRIRE
Par défaut, si un pcm de capture n'a pas de nombre de canaux précisé (et si 2 est dans les choix possibles), alors gst-launch-1.0 utilise 2 canaux.

# EXEMPLES D'UTILISATION

## Envoyer un la-440Hz vers les enceintes

Quand *jack* est lancé, le playback de référence, typiquement les enceintes, est connu par *gstreamer* sous le nom _jackaudiosink_. Le nom _audiotestsrc_ fait référence à une source qui est un la-440Hz. La commande suivante envoie ce la-440Hz vers les enceintes.

```
lafrier$  gst-launch-1.0 -v audiotestsrc ! audioconvert ! jackaudiosink
```

## Envoyer le flux du micro vers les enceintes

Quand *jack* est lancé, le micro de référence est connu par *gstreamer* sous le nom _jackaudiosrc_. La commande suivante envoie la prise de son effectuée par le micro vers les enceintes. Attention ! c'est un moyen de produire des échos et un effet larsen, donc tenez vous prets à baisser le son.

```
lafrier$  gst-launch-1.0 -v jackaudiosrc ! audioconvert ! jackaudiosink
```

## Envoyer le flux du micro vers les enceintes en filtrant les échos

Avec la commande suivante, on active le DSP _webrtcdsp_ pour enlever les échos. Cela se fait en enlevant un flux qui correspond à ce qui est envoyé aux enceintes : voir l'idée de *loopback*, materiel ou soft. Ici le loopback est soft. Il est nommé _webrtcechoprobe_ et est pris juste avant l'envoi aux enceintes.

```
lafrier$  gst-launch-1.0 -v jackaudiosrc ! audioconvert ! webrtcdsp ! webrtcechoprobe ! audioconvert ! "audio/x-raw,channels=2" ! jackaudiosink
```

Remarque : _webrtcdsp_ accepte des options que l'on donne à l'interieur du pipeline. Dans l'exemple suivant, on annule le filtrage de l'écho.

```
lafrier$  gst-launch-1.0 -v jackaudiosrc ! audioconvert ! webrtcdsp "echo-cancel=false" ! webrtcechoprobe ! audioconvert ! "audio/x-raw,channels=2" ! jackaudiosink
```

Remarque : Dans la documentation de _webrtcdsp_, il est dit que la commande usuelle pour annuler un écho est celle qui concerne une situation de vidéoconférence. Par exemple, si on utilise *mumble*, si on nomme _far-end-sink_ le son de notre micro (qui rentre dans _mumble_ pour aller vers les enceintes de la salle distance) et si on nomme _far-end-src_ le son pris par le micro de la salle distante, qui passe par _mumble_ pour aller vers nos enceintes, alors la commande contient deux pipelines :

```
lafrier$  gst-launch-1.0 far-end-src ! audio/x-raw,rate=48000 ! webrtcechoprobe ! jackaudiosink \ jackaudiosrc ! audio/x-raw,rate=48000 ! webrtcdsp ! far-end-sink
```

Le problème devient alors de trouver les noms de _far-end-sink_ et _far-end-src_. C'est ce qu'on teste avec *firefox* ci-dessous.

## Envoyer le flux sortant de *firefox* vers les enceintes

Si on a lancé *firefox* et que l'on se sert de _firefox_ (compilé avec *jack*) pour écouter un flux audio, alors en ouvrant le graphe des connexions de _jack_ on voit que le nom de la sortie audio de _firefox_ est _AudioStream_0_out_. On peut alors passer ce nom à _jackaudiosrc_ de façon à ce que _jackaudiosrc_ ne poite plus vers le flux du micro de base, mais vers le fux sortant de _firefox_.

```
lafrier$  gst-launch-1.0 -v jackaudiosrc port-pattern=\"AudioStream_0_out\" ! audioconvert ! "audio/x-raw,channels=2" ! jackaudiosink
```

Remarque : C'est une expérience intéressante d'écouter ce que le filtre d'écho fait sur une musique de clavecin par exemple ...

```
lafrier$  gst-launch-1.0 jackaudiosrc port-pattern=\"AudioStream_0_out\" ! audioconvert ! webrtcechoprobe ! audioconvert ! "audio/x-raw,channels=2" ! jackaudiosink
```

## Enlever les echo avec le loopback

Voir *alsa*(7e) pour le loopback _aloop_. On va l'utiliser avec _webrtcdsp_.

Sous mumble on a choisi les entrées et sorties sysdefault:Loopback. RQ: en faisant ça, on prend implicitement Loopback:0. On retrouve donc ce signal sur Loopback:1 (entrée et sortie).

```
lafrier$  gst-launch-1.0 \
  alsasrc device=hw:Loopback,1 \
  ! audioconvert \
  ! webrtcechoprobe \
  ! audioconvert \
  ! alsasink device=hw:3 \
  alsasrc device=hw:3 \
  ! audioconvert \
  ! webrtcdsp echo-cancel=true \
  ! audioconvert \
  ! alsasink device=hw:Loopback,1
```

## Utiliser les loopback pour les applications qui ne voient pas les pcm d'alsa
Certaines applications ne peuvent se connecter qu'au hardware. On ne peut donc pas composer un pcm dans _~/.asoundrc_ et dire à l'application de travailler avec ce pcm. Mais le loopback est une solution facile pour cela (Voir *alsa*(7e) pour le loopback _aloop_) :
	- on compose son pcm dans _~/.asoundrc_ (dans l'exemple qui suit, le nom du pcm est _scarlett_4i4_dmix_plug_),
	- on connecte le pcm au loopback,
	- on connecte l'application au loopback (qui est vu comme un hardward même quand on utilise _aloop_).

```
lafrier$ gst-launch-1.0 -v \
  alsasrc device="hw:Loopback,1" \
  ! alsasink device="scarlett_4i4_dmix_plug" \
  alsasrc device="scarlett_4i4_dmix_plug" \
  ! alsasink device="hw:Loopback,1"
```

# VOIR AUSSI
*jack*(7e)

