ffmpeg(1e)

# NAME

ffmpeg - Gestion des fichiers et flux video (et audio)

# DESCRIPTION

Cette documentation décrit l'utilisation du gestionnaire video *ffmpeg*(1).

# UTILISATION

A écrire ...

# POUR MEMOIRE : UNE VIEILLE DOC DE AIDEUNIX

A coller ...

# POUR MEMOIRE : UNE DOC DE CLEMENT POUR LA CAPTURE DES VISIOS

---
header: Capture vidéo d'une fenêtre X
section: 1
...

# Capture vidéo d'une fenêtre X

Ces quelques scripts montrent un exemple d'utilisation des outils `xdotool` et
`ffmpeg` pour faire la capture vidéo d'une fenêtre X et la capture audio du
microphone par défaut de la machine.

## Installation des paquets nécessaires

### Sur Ubuntu

```
sudo apt install ffmpeg xdotool
```

### Sur Gentoo

```
emerge ffmpeg xdotool
```

## Capture de la vidéo d'une fenêtre X

Nous décrivons ici les principales commandes, ainsi que leurs options, utilisées
pour faire la capture d'une fenêtre X en ligne de commande à l'aide des outils
`ffmpeg` et `xdotool`.

### Options importantes de `ffmpeg`

#### Capture de l'affichage X, i.e. du bureau

Pour la capture du bureau, nous utilisons les options suivantes :

```
-f x11grab -s 1024x1024 -i :0.0+10,20
```

Voici une explication rapide de ces options :

`-f x11grab`

:   Cette option dit à `ffmpeg` que les options qui suivent seront à appliquer
    sur le format d'entrée `x11grab` qui récupère l'affichage X11, donc le
    bureau.

`-s 1024x1024`

:   Cette option donne la taille de la vidéo, et ce sera la taille de la zone du
    bureau qui sera capturée. Ici, la vidéo fera 1024 pixels sur 1024 pixels.

`-i :0.0+10,20`

:   Cette option dit sur quel écran récupérer la vidéo, et le décalage en pixels
    sur les axes X et Y à appliquer. Ici, nous prenons l'écran 0 (le premier),
    et nous nous décalons de 10 pixels en X et 20 pixels en Y par rapport au
    coin haut gauche de l'écran. Ce point sera le coin haut gauche de la zone de
    la vidéo.

#### Enregistrement sans compression

Afin de nous assurer que la vidéo pourra être enregistrée en temps réel sur le
disque sans perte d'image, nous évitons de faire la compression à la volée.
Autre avantage de ce façon de procéder, c'est que nous pourrons par la suite
compresser une même vidéo avec différents encodeurs, en fonction de ce que nous
voulons faire. Voici les options utilisées :

```
-preset ultrafast -lossless 1 -threads 0
```

Voici une explication rapide de ces options :

`-preset ultrafast`

:   Permet d'utiliser une configuration d'encodage de la vidéo dite ultra
    rapide. Cette configuration étant prédéfinie par `ffmpeg` et est sensée
    garantir que l'encodage sera le plus rapide possible.

`-lossless 1`

:   Permet de faire un encodage sans perte de qualité. Cela garantie que nous
    n'avons pas de compression appliquée lors de l'encodage, et cela permet
    aussi de nous garantir que les compressions futures que nous appliquerons à
    la vidéo partiront d'une qualité identique à la source initiale, donc sans
    perte.

`-threads 0`

:   Cette option n'est pas spécifique à la vidéo, elle permet juste de dire à
    `ffmpeg` d'utiliser de manière optimale le nombre de cœurs du processeur. Le
    chiffre `0` peut être remplacé par le nombre de cœurs du processeurs pour
    nous assurer que `ffmpeg` les utilisera tous.

#### Enregistrement du son en utilisant le microphone par défaut

Nous décrivons ici toutes les options utilisées pour la capture du son provenant
du microphone par défaut.

```
-f alsa -ac 1 -ar 44100 -i default
```

`-f alsa`

:   Cette option dit à `ffmpeg` que les options qui suivent seront à appliquer
    sur le format d'entrée `alsa` qui récupère le son en utilisant le pilote
    `alsa`.

`-ac 1`

:   `ac` pour _audio channels_. Donne le nombre de canaux audio à utiliser. Ici,
    nous sommes en mono. Pour être en stéréo, il faut mettre le chiffre `2`.

`-ar 44100`

:   `ar` pour _audio rate_. Donne la fréquence à utiliser pour l'enregistrement.
    Ici, 44100 Hz.

`-i default`

:   Donne le nom du matériel logique `alsa` à utiliser pour l'enregistrement. Si
    vous en avez défini un par défaut, ce sera celui-ci qui sera utilisé. Vous
    pouvez donner le nom précis du matériel que vous voulez utiliser. Les noms
    peuvent être récupérés avec la commande `arecord -L`.

### Utilisation de `xdotool`

Cet outil permet de simuler les évènements clavier et souris. Nous l'utilisons
pour préparer les fenêtres à capturer, ainsi que pour récupérer leurs position
et taille. Nous utilisons ensuite ces informations pour construire les options
passées à `ffmpeg`.

`xdotool sleep 3 getwindowfocus getwindowgeometry`

`sleep 3`

:   Permet d'attendre 3 secondes avant d'exécuter les commandes suivantes. Cela
    nous permet de laisser le temps à l'utilisateur d'aller donner le focus à la
    fenêtre qu'il va vouloir capturer.

`getwindowfocus`

:   Permet de récupérer l'identifiant de la fenêtre qui a actuellement le focus.
    Cette commande a pour effet d'empiler l'identifiant dans une pile interne à
    `xdotool` afin de pouvoir l'utiliser **implicitement** par les commandes qui
    suivent.

`getwindowgeometry --shell`

:   Permet de récupérer les informations géométriques de la fenêtre passée en
    paramètre. Ici, nous ne donnons pas de d'identifiant de fenêtre, c'est alors
    l'identifiant en haut de la pile qui sera utilisée, d'où la commande
    précédente. L'option `--shell` permet de retourner les informations sous la
    forme `PROPRIETE=VALEUR`, ce qui nous sera utile pour les exploiter au
    travers d'un script Bash.

### Commande complète

```
IFS='' read FFMPEG_CMD <<"EOF"
ffmpeg \
  -f alsa \
  -ac 1 \
  -ar 44100 \
  -i default \
  -f x11grab \
  -s ${WIDTH}x${HEIGHT} \
  -i :${SCREEN}.0+${X},${Y} \
  -vcodec libx264 \
  -preset ultrafast \
  -lossless 1 \
  -threads 0 \
  output.mp4
EOF

env `xdotool sleep 3 getwindowfocus getwindowgeometry --shell | xargs` \
  bash -c "${FFMPEG_CMD}"
```

Les commandes `xdotool` et `ffmpeg` ont été décrites, nous nous attacherons donc
ici à expliquer leur utilisation conjointe.

La commande `read` est utilisée ici pour construire une chaîne de caractères
contenant l'appel à `ffmpeg` qui utilise les variables d'environnement
retournées par `xdotool`. Il y a peut être d'autres moyens avec Bash d'arriver
au même résultat. L'idée est de protéger cette chaîne de caractères pour que
l'évaluation des variables ne soit faite qu'au moment où la commande `bash -c
"${FFMPEG_CMD}"` sera exécutée.

La commande `env` permet d'ajouter des variables d'environnement données en
ligne de commande, avant d'appeler la commande, ici `bash -c "${FFMPEG_CMD}"`.

L'utilisation de `xargs` permet de concaténer sur une seule ligne, toutes les
variables affichées ligne par ligne sur la sortie standard par la commande
`xdotool [...]`.

## Compression de la vidéo

Une fois que notre vidéo a été enregistrée, il y a fort à parier que sa taille
sur disque soit élevée. Une compression sera probablement nécessaire en fonction
des cas d'utilisation.

Pour pouvoir mettre une vidéo sur le Web en utilisant un format standard et
répdanu, nous proposons de compresser les vidéos avec l'encodeur VP9.
[Subtilitée décrite dans la documentation de
`ffmpeg`](https://trac.ffmpeg.org/wiki/Encode/VP9), il faut utiliser le
sous-echantillonage chromatique YUV 4:2:0 pour avoir de meilleurs compatibilité.

Voici la commande que nous utilisons :

```
ffmpeg \
  -i output.mp4 -vcodec libvpx-vp9 -pix_fmt yuv420p -crf 30 -b:v 0 \
  -threads 0 output.webm
```

`-i output.mp4`

:   Donne la vidéo d'entrée à compresser.

`-vcodec libvpx-vp9`

:   Donne l'encodeur à utiliser, ici nous utilisons la bibliothèque `libvpx-vp9`
    qui fournit l'encodage vidéo VP9.
 
`-pix_fmt yuv420p`

:   Donne le format de chaque pixel, ici l'encodage utilisera du YUV 4:2:0.

`-crf 30`

:   `crf` pour _Constant Rate Factor_. Détermine la qualité de l'encodage.
    [Cette documentation](https://developers.google.com/media/vp9/settings/vod/)
    donne plus d'information sur la valeur à utiliser pour ce paramètre.

`-b:v 0`

:   Défini le nombre de bit par seconde à utiliser pour compresser la vidéo.
    Cette option est utile quand nous voulons faire de la vidéo en streaming.
    Dans notre cas, pour nous assurer que l'option `-crf` sera bien respectée,
    nous ne donnons pas de contrainte pour cette valeur.

## Configuration pour une utilisation spécifique

Faire une capture vidéo se fait rarement en une seule prise. Voici le script
que j'ai utilisé pour faire la capture de la vidéo parlant du rayonnement :

```
echo "You have 3 seconds to set the focus of the window that must be captured..."
xdotool sleep 3 getwindowfocus windowsize 1480 960
ffmpeg -f alsa -ac 1 -ar 44100 -i default -f x11grab -r 15 -s 1440x780 \
  -i :0.0+20,137 -vcodec libx264 -preset ultrafast -lossless 1 \
  -threads 0 output.mp4
```

Ce script est très spécifique à ma configuration, et je l'ai ajusté pour ne
prendre qu'une sous-partie de la fenêtre Firefox que j'utilisais.

Ici, j'utilise `xdotool` pour positionner et redimensionner ma fenêtre
correctement selon mes besoins : position en 0x0 et taille de 1480x960 avant de
lancer la capture de la vidéo.

Notons que j'utilise aussi l'option `-r 15` pour dire que je ne veux que 15
images par secondes, ce qui est largement suffisant pour une vidéo où très peu
de choses bougent, et quand elles le font, elles le font lentement. Je limite
ainsi la taille de la vidéo produite.# VOIR AUSSI

*ffmpeg*(1)

