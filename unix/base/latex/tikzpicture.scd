tikzpicture(7e)

# NAME
tikzpicture - Utilisation de tikzpicture

# DESCRIPTION
*tikzpicture*(7e) est un environnement de latex qui peut être utilisé pour
faire des dessins, tracer des courbes, annoter des figures...

# UTILISATION
On peut créer et compiler un fichier *latex*(7e) qui ne contiendra que le
dessin que l'on aura créé :
```
cat << EOD > dessin.tex
\documentclass{standalone}
  
\usepackage{tikz}
\usepackage{graphicx}

\begin{document}
\begin{tikzpicture}
  % COMMANDES TIKZ PRODUISANT UN DESSIN
\end{tikzpicture}
\end{document}
EOD
```
Pour le compiler, comme un document latex classique :
```
lafrier$  pdflatex dessin.tex
```
Le fichier produit portera le nom _dessin.pdf_.

# LE CONTENU
Attention toujours mettre un point virgule après une commande

## Les noeuds 
Un objet de base en tikz est un ``noeud".
```
\node (premierNoeud) {J'écris dans le noeud} ;
```
Ici _premierNoeud_ est une étiquette à laquelle on pourra faire référence dans
la suite. Et le texte entre accolade est le contenu du noeud. On peut mettre
autre chose que du texte dans un noeud, par exemple insérer une image 
```
\node[below of=premierNoeud] (deuxiemeNoeud) {
  \includegraphics[width=3cm]{mon_image.png} 
};
```
Pour ce deuxième noeud, la commande node a une option : _below of=premierNoeud_
signifie que ce deuxième noeud sera sous le premier. Il porte l'étiquette
_deuxiemeNoeud_ et il contient la figure mon_image.png. Pour le mettre à droite
ou à gauche ou au dessus, _right of=_, _left of=_, _above of=_ et on peut aussi
combiner below/above avec right/left par exemple _below right of=premierNoeud_
place le deuxiemeNoeud en bas à droite de premierNoeud.
Au lieu d'utiliser _below of=_ on peut utiliser une combinaison de "anchor" et
de "at" :
```
\node[anchor=north] (deuxiemeNoeud) at (premierNoeud.south) {
  \includegraphics[width=3cm]{mon_image.png} 
};
```
anchor détermine le point d'ancrage du deuxiemeNoeud, et at détermine où il
doit être attaché. Les points d'ancrages des noeuds sont exprimés en points
cardinaux : north, south, east, west, north west, north east... Ici
deuxiemeNoeud sera sous premierNoeud.

## Les coordonnées
On peut aussi simplement déclarer une coordonnée quelque part, ce serait comme
un noeud sans contenu.
```
\coordinate (unAncrage) at ([xhisft=1cm]deuxiemeNoeud.east);
```
L'étiquette unAncrage réfère à un point qui se site à 1cm à droite (xshift=1cm)
de la droite (east) de deuxiemeNoeud.

## Les formes

On peut créer des formes par exemple un rectangle 
```
\draw[draw=black] (unAncrage) rectangle (0,0);
```
Dessine un rectangle qui a un coin à la position unAncrage et le coin opposé à
la position (0,0). On peut alternativement spécifier la taille du rectangle 
```
\draw[draw=black] (unAncrage) rectangle ++(2,1);
```
Le coin opposé à unAncrage sera alors à 2cm à droite et 1cm en haut (mettre des
longueurs négatives pour aller à gauche ou en bas).

Au lieu de \draw rectangle  on peut passer par la commande \node :
```
\node[rectangle, shading=axis, left color=white, right color=white, middle color=black, minimum height=2cm, minimum width=4cm, anchor=center] (nuage) at (0,0) {};
```
en spécifiant la forme (rectangle). shading=axis va créer un dégradé de couleur
à l'intérieur du rectangle. Les couleurs à gauche à droite et au milieu sont
spécifiées par _left color_, _right color_ et _middle color_ respectivement
(ici un dégradé blanc aux bords et noir au milieu). La taille du rectangle est
donnée par _minimum height_ (hauteur) et _minimum width_ (largeur). Il est
centré sur le point de coordonnées (0,0). Il faut toujours mettre les accolades
même si on ne met rien dans le noeud.

## Les lignes
Trace une ligne jaune se finissant par une fleche, depuis startRay, dans la
direction -70 degrés (0 = droite, 90 = haut, -70 vers le bas droite) et de
longueur -4cm (vers haut gauche à cause du signe moins).
```
  \coordinate (startRay) at (nuage.north);
  \draw[line width=.1cm, yellow, ->]  (ray0) --++ (-70:-4cm);
```

# VOIR AUSSI
latex(7e)
