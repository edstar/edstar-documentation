gnuplot(7e)

# NAME

gnuplot - Utilisation de gnuplot

# DESCRIPTION

*gnuplot*(1) est un logiciel pour créer des figures à partir de fichiers ASCII au
format colonnes, soit en mode interactif dans le terminal, soit via un script

# UTILISATION

Par exemple le fichier _f.dat_ contient 4 colonnes :

```
i_run mean_i std_i ref_i
```

## En mode interactif

Lancer *gnuplot* depuis un terminal pour ouvrir un invite de commande :

```
lafrier $  gnuplot
```

Tracer colonne 1 en fonction de colonne 0 _using_ :

```
gnuplot> plot "f.dat" using 0:1
```

Avec des barres d'erreurs données par colonne 2 _with errorbars_ :

```
gnuplot> plot "f.dat" using 0:1:2 with errorbars
```

En ajoutant la courbe colonne 3 en fonction de colonne 0 :

```
gnuplot> plot "f.dat" using 0:1:2 with errorbars, "f.dat" using 0:3
```

Avec des titres pour la légende _title_ :

```
gnuplot> plot "f.dat" using 0:1:2 title "col1" with errorbars, "f.dat" using 0:3 title "col3"
```

## En mode script

Ecrire des commandes dans un fichier :

```
lafrier $ echo "set terminal png size 1280,920;\\
  set output 'fig.png';   \\
  set xlabel 'axe des x'; \\
  set ylabel 'axe des y'; \\
  plot 'f.dat' using 0:1 title 'col 1';"\\
  > gnuplot_script
```

Interpréter le script avec gnuplot :

```
lafrier $ gnuplot < gnuplot_script
```

## Pour explorer des fonctions analytiques

On peut définir une fonction de x, ou une fonction de x et de y, par exemple

```
gnuplot> f(x) = sin(x)
gnuplot> g(x,y) = sin(x)*exp(y)
```

On peut ensuite faire des tracés en fonction de x, par exemple entre x=-10 et
x=5 (en fixant y pour la fonction de deux variables) :

```
gnuplot> y=1
gnuplot> plot[x=-10:5] f(x)
gnuplot> plot[x=-10:5] g(x,y)
```

On peut aussi faire des tracés en fonction de y :

```
gnuplot> x=1
gnuplot> plot[y=-10:5] g(x,y)
```

# VOIR AUSSI

*gnuplot*(1)
