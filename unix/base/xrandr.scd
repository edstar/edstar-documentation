xrandr(1e)

# NAME
xrandr - Utilisation de xrandr

# DESCRIPTION
*xrandr* permet de configurer l'écran.

# UTILISATION
La commande _xrandr_ seule affiche la liste des écrans disponibles et des résolutions qu'ils sont capables de reconnaitre :
```
lafrier$  xrandr
```

Exemple de configuration :
```
lafrier$  xrandr --output eDP-1 --mode 1024x768 --output DP-3 --pos 0x0
```

# VOIR AUSSI
*xrandr*(1)
