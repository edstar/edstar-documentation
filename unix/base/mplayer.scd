mplayer(1e)

# NAME
mplayer - Utilisation de mplayer

# DESCRIPTION
*mplayer* permet de lire des fichiers.

# UTILISATION
Pour lire un fichier stéréo (par exemple _toto.mp3_) et renvoyer le même son sur les deux enceintes (mélanger les déux canneaux à égalité) :
```
lafrier$  mplayer toto.mp3 -af extrastereo=0
```
