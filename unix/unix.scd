unix(7e)

# NAME
unix - Description de unix

# DESCRIPTION

*unix* est une partie de *edstar-documentation*(7e). Elle est dédiée à la partie
des pratiques récurentes au sein de la plateforme _EDStar_ qui sont inspirées de
l'histoire du système d'exploitation _UNIX_.

# VOIR AUSSI
*edstar-documentation*(7e)
