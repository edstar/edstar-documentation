ssh-serveur-gentoo(7e)

# NAME
ssh-serveur-gentoo - Installer et configurer un serveur ssh sur gentoo

# INSTALLATION
La plupart des installations de base de *gentoo* passent par l'installation d'un serveur *ssh* via le paquet _net-misc/openssh_. Il n'y a donc rien à faire pour installer un serveur *ssh*.

# CONFIGURATION

```
root#  vi /etc/ssh/sshd_config
vi:
  Port 2222
  PermitRootLogin no
  PasswordAuthentication no
  KbdInteractiveAuthentication no
  UsePAM no
  PrintMotd no
  PrintLastLog no
  Subsystem       sftp    /usr/lib64/misc/sftp-server
  AcceptEnv LANG LC_ALL LC_COLLATE LC_CTYPE LC_MESSAGES LC_MONETARY LC_NUMERIC LC_TIME LANGUAGE LC_ADDRESS LC_IDENTIFICATION LC_MEASUREMENT LC_NAME LC_PAPER LC_TELEPHONE
  AcceptEnv COLORTERM
  AllowUsers lafrier@nom-machine lafrier@192.168.1.16
root#  rc-service sshd start
root#  rc-update add sshd default
```

RQ: Changer 2222 par ce que vous voulez.

Si on a effectivement changé le port (2222 au lieu de 22), la commande *ssh*
devient (pour se connecter à _machine-lafrier_ depuis une machine distante) :
```
lafrier$  ssh lafrier@machine-lafrier -p2222
```

# VOIR AUSSI
*ssh*(1e),*ssh-gentoo*(7e)
