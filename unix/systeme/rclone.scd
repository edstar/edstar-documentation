rclone(1e)

# NAME

rclone - Utilisation de _rclone_ pour accéder à des service de cloud 

# INTRODUCTION

Certains débutants sur la plateforme ont besoin, depuis le système _edix_,
d'accéder à des fichiers qu'ils ont déposé sur un service de cloud, ou au
contraire de déposer des fichiers sur un cloud de façon à y accéder depuis leur
système habituel. La commande _rclone_ permet ce geste de façon simple, en ligne
de commande, pour plusieurs des services de cloud les plus usuels.

# UTILISATION

Si _rclone_ a déjà été utilisé auparavant, on peut commencer par afficher les
configurations existantes :
```
rclone config show
```
Les noms des configurations existantes apparaîssent alors entre corchets, e.g
```
[drivelafrier]
```

Pour créer une configuration (e.g. drivelafrier pour google drive) :
```
rclone config
e/n/d/r/c/s/q> n
name> drivelafrier
Storage> 13
client_id>
client_secret>
scope> 1
root_folder_id>
service_account_file>
y/n>
y/n>
y/n>
y/e/d>
e/n/d/r/c/s/q>q
```

## Lister les fichiers et répertoires sur un cloud

```
rclone ls drivelafrier:
```
ou
```
rclone lsd drivelafrier:
```

## Effacer un répertoire sur un cloud

```
rclone purge drivelafrier:repertoire
```

## Monter et démonter un volume
Pour monter le volume correspondant à un cloud configuré (e.g. drivelafrier),
s'assurer que le répertoire /mnt/drivelafrier existe et qu'il est bien propriété
de l'utilisateur (ici lafrier). Ensuite :
```
rclone mount drivelafrier: /mnt/drivelafrier
```

Pour démonter le volume :
```
fusermount -u /mnt/drivelafrier
```

## Recopier le contenu d'un cloud sur un disque local (sans effacement)
En admettant qu'il existe localement un répertoire nommé
/home/lafrier/drivelafrier.copy sur le disque local, pour recopier le contenu de
drivelafrier (voir la configuration ci-dessus) dans ce répertoire~:
```
rclone copy drivelafrier: /home/lafrier/drivelafrier.copy
```
Le fait que l'on ait utilisé la sous-commande copy signifie que l'on ne
supprimera pas les fichier existant sur /home/lafrier/drivelafrier.copy qui
n'existent plus sur drivelafrier. Par contre, ne seront recopié que les fichiers
qui ont été mondifiés : en ce sens c'est comme une synchronisation dans laquelle
on choisit de préserver les fichiers ayant disparus.

## Recopier le contenu d'un disque local sur un cloud (sans effacement)
Il s'agit de l'opération inverse de la précédente. On suppose qu'existe
localement un répertoire nommé /home/lafrier/drivelafrier et on souhaite
recopier son contenu sur le cloud drivelafrier
(voir la configuration ci-dessus) :
```
rclone copy /home/lafrier/drivelafrier drivelafrier: 
```
Les fichiers existant sur le cloud qui n'existent plus sur le volume local ne
seront pas effacés.

## Recopier le contenu d'un cloud sur un disque local (avec effacement)
En admettant qu'il existe localement un répertoire nommé
/home/lafrier/drivelafrier.sync sur le disque local, pour synchroniser ce
répertoire avec le contenu de drivelafrier (voir la configuration ci-dessus)~:
```
rclone sync drivelafrier: /home/lafrier/drivelafrier.sync
```
Le fait que l'on ait utilisé la sous-commande sync signifie que l'on
supprimera les fichier existant sur /home/lafrier/drivelafrier.sync si ils
n'existent plus sur drivelafrier.

## Recopier le contenu d'un disque local sur un cloud (avec effacement)
Il s'agit de l'opération inverse de la précédente. On suppose qu'existe
localement un répertoire nommé /home/lafrier/drivelafrier et on souhaite
recopier son contenu sur le cloud drivelafrier
(voir la configuration ci-dessus) :
```
rclone sync /home/lafrier/drivelafrier drivelafrier: 
```
Les fichiers existant sur le cloud qui n'existent plus sur le volume local
seront effacés.

# VOIR AUSSI

*rclone*(1),
