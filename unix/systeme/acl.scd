acl(7e)

# NAME

acl - Introduction à l'utilisation des listes de contrôle d'accès

# INTRODUCTION

Les listes de contrôles d'accès (ou *acl*(5) pour Access Control Lists)
permettent un contrôle plus fin des droits d'accès aux fichiers et répertoires
que ne le permettent les droits d'accés standards d'un système de fichier UNIX.
Elles autorisent notamment de définir des droits accès différenciés par
utilisateur et par groupe.

# UTILISATION

Créer un dépôt git nommé _repo_ et y donner accés en lecture et écriture à tous
les membres du groupe _users_. Donner l'accès en exécution si le fichier est un
répertoire. S'assurer que les nouveaux fichiers et répertoires créées dans le
dépôt auront par défaut les mêmes droits d'accès :

```
mkdir repo
git init --bare repo
setfacl -R -m g:users:rwX repo
setfacl -R -m -d g:users:rwX repo
```

Interroger les listes de contrôle d'accès appliquées au répertoire _repo_ :

```
getfacl repo
```

# VOIR AUSSI

*acl*(5),
*getfacl*(1),
*setfacl*(1)
