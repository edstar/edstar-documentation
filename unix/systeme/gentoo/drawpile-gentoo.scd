drawpile-gentoo(7e)

# NAME
drawpile-gentoo - Installer drawpile sur gentoo

# INSTALLATION

## Avec appimage

```
root#  cd /opt
root#  wget https://github.com/drawpile/Drawpile/releases/download/2.2.0/Drawpile-2.2.0-x86_64.AppImage
root#  chmod +x Drawpile-2.2.0-x86_64.AppImage
root#  emerge -av sys-fs/fuse:0
```

et pour les utilisateurs,
```
lafrier$  echo "alias drawpile='/opt/Drawpile-2.2.0-x86_64.AppImage'" >> /home/lafrier/.bashrc
```

## A partir des sources (ne fonctionne plus)

```
root#  emerge -av kde-frameworks/extra-cmake-modules
root#  emerge -av kde-frameworks/karchive
root#  emerge -av net-libs/miniupnpc
root#  emerge -av dev-libs/libsodium
root#  emerge -av kde-frameworks/kdnssd
root#  emerge -av dev-qt/qtwidgets
root#  emerge -av dev-libs/qtkeychain
root#  emerge -av net-libs/libmicrohttpd
root#  emerge -av dev-qt/qtmultimedia:5
root#  cd /opt
root#  git clone https://github.com/drawpile/Drawpile.git
root#  cd Drawpile
root#  mkdir build
root#  cd build
root#  cmake ..
root#  make
root#  make install
```

RQ: Au niveau du *cmake*, il y a différents warnings, notamment concernant _qtcolorwidget_. Mais ce n'est
pas gênant pour la suite.

# VOIR AUSSI
*drawpile-ubuntu*(7e), *drawpile*(1e)
