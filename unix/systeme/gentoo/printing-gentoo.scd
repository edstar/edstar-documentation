printing-gentoo(7e)

# NAME

printing-gentoo - Installation d'une imprimante sur Gentoo

# INSTALLATION

Sur UNIX, l'outil incontournable pour imprimer un document est le système
d'impression *cups*(1). On commence donc par l'installer via le gestionnaire de
paquets de notre distribution. Pour cela on active le support de _cups_ pour le
paquet _ghostscript-gpl_, une dépendance requise par le système d'impression. On
active également les options _exif_, _jpeg_, _pdf_ et _png_ pour le paquet
_cups-filters_ afin de permettre l'impression des formats correspondants.
On invoque enfin l'installation de *cups* lui même. Ne reste plus qu'à démarrer
le daemon *cupsd*(8) pour la session courrante et de l'ajouter à la liste des
services à démarrer par défaut au lancement du système :

```
root ~ #  "echo app-text/ghostscript-gpl cups" \\
  >> /etc/portage/package.use/ghostscript-gpl
root ~ #  "echo net-print/cups-filters exif jpeg pdf png" \\
  >> /etc/portage/package.use/cups-filters
root ~ #  emerge -av net-print/cups
root ~ #  rc-service cupsd start
root ~ #  rc-update add cupsd default
```

Nous installons alors les pilotes de l'imprimante que l'on souhaite utiliser.
Pour cela on pourra au préalable vérifier que celle-ci est bien supportée sur
notre système en se rendant sur le site d'_OpenPrinting_ [2]. Et s'il reste
possible de télécharger les pilotes directement sur ce site on préfèrera
installer le paquet _gutenprint_ (en activant le support de *cups* et des
pilotes *ppd*) qui disposera vraisemblablement du pilote adéquat dès lors que
l'imprimante est supportée sur un système UNIX. Ce n'est qu'à défaut qu'on lui
préférera les pilotes proposés par _OpenPrinting_ :

```
root ~ #  echo "net-print/gutenprint cups ppds" \\
  >> /etc/portage/package.use/gutenprint
root ~ #  emerge -av net-print/gutenprint
```

Reste à configurer notre système d'impression pour pouvoir imprimer nos
documents via notre imprimante. Pour cela, nous renvoyons le lecteur vers la
page de documentation *printing*(7e).

# VOIR AUSSI

. <https://wiki.gentoo.org/wiki/Printing>
. <https://www.openprinting.org/printers>

*cups*(1), *cupsd*(8), *printing*(7e)
