intel-microcode-gentoo(7e)

# NAME

intel-microcode-gentoo - Mise à jour du microprogramme d'un processeur Intel sur Gentoo

# CONFIGURATION DU NOYAU

```
root ~ #  cd /usr/src/linux/
root /usr/src/linux #  make menuconfig
  CONFIG_BLK_DEV_INITRD=y
  CONFIG_MICROCODE=y
  CONFIG_MICROCODE_INTEL=y
root /usr/src/linux #  make && make install && modules_install
root /usr/src/linux #  grub-mkconfig -o /boot/grub/grub.cfg
```

# MISE À JOUR DU MICROCODE

```
root ~ #  echo "sys-firmware/intel-microcode intel-ucode" \
  >> /etc/portage/package.license/intel-microcode
root ~ #  emerge -av sys-firmware/intel-microcode
```

```
root ~ #  sed -n '/^microcode/{p;q}' /proc/cpuinfo > ucodeid_$(date -Ihours).txt
root ~ #  rm -f /boot/early_ucode.cpio
root ~ #  iucode_tool -S \
  --write-earlyfw=/boot/early_ucode.cpio /lib/firmware/intel-ucode/*
root ~ #  grub-mkconfig -o /boot/grub/grub.cfg
```
