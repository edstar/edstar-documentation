sxmo-gentoo(7e)

# NAME
sxmo-gentoo - Installer l'environnement de bureau Sxmo sur Gentoo

# INTRODUCTION

Cette documentation décrit l'installation de l'environnement de bureau
Sxmo [1] sur un système GNU/Linux Gentoo, un environnement à destination d'un
téléphone portable de type "smartphone". Dans cette documentation, nous
assumons que la machine sur laquelle Sxmo est installée est bien de ce type,
en l'occurence un Pinephone. Nous renvoyons le lecteur vers la documentation
*pinephone-gentoo*(7e) pour la procédure d'installation de la distribution
Gentoo sur ce type de matériel.

L'environnement de bureau Sxmo s'inscrit dans le mouvement _suckless_ [2] dont
l'objectif est le minimalisme, la simplicité et la stabilité des programmes
développés. Plus qu'une simple inspiration, Sxmo s'appuie sur de nombreux
programmes directement issus de l'écosystème _suckless_ tels que le système de
fenêtrage *dwm*, l'émulateur de terminal *st* ou le menu dynamique *dmenu*.
Au delà d'une simple collection d'outils minimalistes, le fonctionnement de
Sxmo est dans son entier pensé dans les mêmes termes de sobriété et de
robustesse, avec un objectif non moins essentiel : que l'utilisateur puisse
étudier et modifier lui même son environnement de travail. Dit autrement,
qu'il en soit aussi le programmeur. Fort de ces objectifs hérités de la pensée
Unix, l'architecture de Sxmo s'articule autour d'un ensemble de scripts shell
très facilement modifiables et qui s'interfacent naturellement dans son
environnement graphique ou dans un simple terminal.

Bien que activement développé, et malgré la force de sa proposition, Sxmo
reste un projet récent où 4 distributions Linux sont officiellement supportées
(Alpine, Arch, Debian et postmarketOS). L'installer sur une autre distribution
telle que Gentoo demande à ce qu'on y mette du notre eu égard les spécificités
de la distribution. Par ailleurs, Sxmo se fixe un cadre a priori quant aux
programmes sur lesquels il s'appuie. Des choix nécessairement discutables,
remis en cause dans cette documentation dans un but toujours plus affirmé de
sobriété et de simplicité de l'environnement de bureau ainsi installé. Loin
d'être un reproche adressé à l'encontre de Sxmo quant à ses choix de
programmes, c'est au contraire la réussite d'un de ses principaux objectifs
qui est ici saluée, à savoir la possibilité laissé à l'utilisateur d'étudier
et de modifier par lui même le comportement de son environnement de travail.
On soulignera que cette administration sur mesure, si elle reste un plus
indéniable, requiert néanmoins un travail spécifique. Un aspect d'autant moins
à négliger compte tenu le manque de maturité de Sxmo dont une simple mise à
jour mineure peut entraîner de nombreux changements qui peuvent impacter les
modifications que l'utilisateur aurait apportées.

# IDENTIFIER LES PROGRAMMES À INSTALLER

Pour automatiser son installation sur une machine, le projet Sxmo propose un
script shell qui s'appuie sur le gestionnaire de paquets de la distribution
Linux sur lequel il est invoqué. Ce script liste également d'autres programmes
à télécharger et à compiler directement sur la machine cible car non
nécessairement disponibles en tant que paquet. Comme annoncé en introduction,
Gentoo n'est malheureusement pas officiellement supporté par Sxmo, ce qui ici
se traduit par un script shell non prévu pour cette distribution. Nous
pourrions éventuellement venir enrichir ce script pour lui ajouter son
support. Cependant, notre installation se veut bien trop exploratoire pour
que nous ayons déjà l'ambition de l'automatiser. Nous venons à la place
étudier ce script pour nous aider à identifier les paquets Gentoo que nous
devons installer ainsi que les programmes à compiler localement.

Nous commençons donc par cloner le dépôt *git* contenant le script shell
sus-mentionné. Nous recherchons alors dans son fichier source la liste des
paquets à installer et ce pour la distribution Alpine et postmarketOS. Nous
nous intéressons à ces deux distributions car celles-ci partagent avec Gentoo
son système d'initialisation, à savoir OpenRC, et sont donc celles se
rapprochant le plus de notre distribution :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-build
root ~ #  more sxmo-build/sxmo_build_all.sh
```

Lors de la lecture du fichier source, on remarquera que Sxmo supporte deux
serveurs d'affichage : l'historique X11 et son remplaçant en devenir Wayland.
Par défaut, l'installation de Sxmo utilise Wayland. Dans notre cas, nous
continuerons néanmois d'utiliser X11 qui malgré ses défauts reste plus mature
en plus de supporter les programmes issus du collectif _suckless_ sans avoir à
recourir à ses alternatives. Nous ignorerons donc les dépendances liées à
Wayland.

On notera que si le script d'installation utilisé par Sxmo liste bien
certaines de ses dépendances, il est cependant loin d'être exhaustif. D'aucun
pourra noter ci-après que nombres de paquets que nous installerons ne sont pas
référencés par ce script et ont été ajoutées au fil des essais erreurs de notre
installation.

# INSTALLER LES BASES DE L'ENVIRONNEMENT DE BUREAU

## Serveur d'affichage X11

Nous commençons notre installation en installant via le gestionnaire de
paquets le serveur d'affichage X11 ainsi que plusieurs de ses bibliothèques
dont nous aurons besoin pour compiler les autres programmes de notre
environnement, tel que le système de fenêtrage *dwm*. Nous installons aussi
plusieurs outils notamment utilisés par Sxmo pour interragir avec le système
d'affichage, tel que *xrandr* pour gérer les sorties vidéos, *xcalib*
pour calibrer les écrans, *xclip* pour copier/coller un texte sélectionné ou
encore *xdotool* pour simuler des entrées clavier :

```
root ~ #  emerge -av x11-base/xorg-server
root ~ #  emerge -av x11-libs/libXft
root ~ #  emerge -av x11-libs/libXinerama
root ~ #  emerge -av x11-libs/libxkbcommon
root ~ #  emerge -av x11-libs/libXscrnSaver
root ~ #  emerge -av x11-apps/xinput
root ~ #  emerge -av x11-apps/xmodmap
root ~ #  emerge -av x11-apps/xrandr
root ~ #  emerge -av x11-apps/xsetroot
root ~ #  emerge -av x11-misc/setxkbmap
root ~ #  emerge -av x11-drivers/xf86-input-synaptics
root ~ #  emerge -av x11-misc/xclip
root ~ #  emerge -av x11-misc/xdotool
root ~ #  emerge -av x11-misc/xdg-utils
root ~ #  echo "x11-misc/xcalib ~arm64" \
  >> /etc/portage/package.accept_keywords/sxmo
root ~ #  emerge -av x11-misc/xcalib
root ~ #  echo "media-libs/libsdl2 xinerama" \
  >> /etc/portage/package.use/sxmo
root ~ #  emerge -av media-libs/libsdl2
```

## Police de caractères

Dans cette section, nous utilisons portage, le gestionnaire de paquet de
Gentoo, pour installer les polices de caractères que nous souhaitons
utiliser :

```
root ~ #  emerge -av media-fonts/dejavu
root ~ #  emerge -av media-fonts/font-misc-misc
```

Pour son interface graphique, Sxmo utilise une police de caractères à
espacement fixe. Si les paquets installés précédemment ont biens ce type de
police il leur manque néanmoins de nombreux glyphes non alphabétiques que Sxmo
utilise comme icône.  Nous installons donc ci-dessous la police à espacement
fixe de la famille _Nerd_ [3] qui contient ce type de glyphes. Absente du
gestionnaire de paquet, nous commençons par télécharger son archive avant d'en
déployer les fichiers et de mettre à jour le cache du système quant aux
polices de caractères disponibles :

```
root ~ #  mkdir -p /usr/share/fonts/nerd/
root ~ #  cd /usr/share/fonts/nerd
root /usr/share/fonts/nerd #  wget
  https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip
root /usr/share/fonts/nerd #  unzip FiraMono.zip
root /usr/share/fonts/nerd #  fc-cache
```

## Système de fenêtrage

Nous allons ici installer la version du système de fenêtrage *dwm* prevue pour
une interface de téléphone portable. Comme tout projet _suckless_, ce
programme s'installe directement à partir de son code source que l'on compile
localement, le fichier d'en-tête _config.h_ servant de fichier de
configuration utilisateur. Ci-après nous utilisons la configuration par défaut
définit dans le fichier _config.def.h_. À noter qu'on prend le soin de
désactiver l'utilisation du programm _bonsai_ pour la gestion des touches du
PinePhone, un programme écrit à l'intention de Wayland ici parfaitement
dispensable :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-dwm
root ~ #  cd sxmo-dwm
root ~/sxmo-dwm #  cp -f config.def.h config.h
root ~/sxmo-dwm #  vi config.mk
root ~/sxmo-dwm #  make PREFIX=/usr NOBONSAIFLAGS=-DNOBONSAI
root ~/sxmo-dwm #  make PREFIX=/usr install
```

## Émulateur de terminal

Après *dwm*, c'est un autre outil du projet _suckless_ [2] que nous installons
ici, à savoir l'émulateur de terminal *st*, lui aussi modifié pour
l'environnement de bureau Sxmo. Tout comme *dwm* nous le compilons directement
à partir de ses sources en utilisant sa configuration par défaut :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-st
root ~ #  cd sxmo-st
root ~/sxmo-st #  cp -f config.def.h config.h
root ~/sxmo-st #  make PREFIX=/usr
root ~/sxmo-st #  make PREFIX=/usr install
```

## Menus dynamiques

Dans cette sous section nous installons deux menus dynamiques. Le premier, est
encore une fois une version modifiée pour téléphone portable d'un projet
_suckless_, à savoir *dmenu*. Nous l'installons là aussi directement à partir
de ses sources :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-dmenu
root ~ #  cd sxmo-dmenu
root ~/sxmo-dmenu #  cp -f config.def.h config.h
root ~/sxmo-dmenu #  make PREFIX=/usr
root ~/sxmo-dmenu #  make PREFIX=/usr install
```

Si *dmenu* se veut être un menu dynamique graphique, c'est à dire s'affichant
via le serveur d'affichage X11 dont il dépend, le menu dynamique *bemenu*,
quant à lui, peut s'afficher dans un terminal. On rappelle à cette occasion que
l'environnement de bureau Sxmo est une collection de scripts shell qui peuvent
s'exécuter directement via un interpréteur de commandes POSIX. En fonction de
l'environnement dans lequel ils s'exécutent, les menus invoqués par ces
scripts seront affichés soit dans une fenêtre X11 via *dmenu* soit directement
dans le terminal qui l'exécute via *bemenu*. Cette fonctionnalité permet par
exemple d'utiliser l'interface de Sxmo pour consulter et écrire des SMS via
une simple connection SSH à partir du terminal de son ordinateur de bureau ;
ce qui permet de profiter du clavier et de l'écran de ce dernier.

Comme second menu dynamique nous installons donc *bemenu* lui aussi
directement à partir de ses sources. On soulignera qu'en plus du terminal,
*bemenu* supporte les serveurs d'affichage Wayland et X11. Pour ce dernier,
Sxmo utilise déjà *dmenu* et nous ne souhaitons pas utiliser Wayland. Par
conséquent nous ne compilons que la version de *bemenu* prévue pour un
terminal (cible _curses_) et les commandes clientes qui permettent de
l'invoquer (cible _clients_) :

```
root ~ #  emerge -av app-text/scdoc
```

```
root ~ #  git clone https://git.sr.ht/~stacyharper/bemenu
root ~ #  cd bemenu
root ~/bemenu #  make PREFIX=/usr clients curses
root ~/bemenu #  make PREFIX=/usr install
```

## Clavier virtuel

Ci après, nous installons le clavier virtuel *svkbd* originellement développé
pour l'environnement de bureau Sxmo avant de rejoindre le projet _suckless_ ;
démontrant une fois encore leur lien réciproque. Comme tout projet _suckless_,
*svkbd* s'installe en compilant directement son code source et se configure
via un fichier d'en-tête C (_config.h_). Nous utilisons ci-après la
configuration par défaut définit dans le fichier _config.def.h_ :

```
root ~ #  git clone https://git.suckless.org/svkbd
root ~ #  cd svkbd
root ~/svkbd #  cp -f config.def.h config.h
root ~/svkbd #  make PREFIX=/usr
root ~/svkbd #  make PREFIX=/usr install
```

## Vibrations et sons du clavier virtuel

Après l'installation du clavier virtuel, nous installons *clickclack*, un
programme dont le seul objet est d'émettre un son et générer des vibrations
quant une touche du clavier virtuel est pressée. Le but est d'aider
l'utilisateur à percevoir la pression d'une touche virtuelle en émulant les
retours haptiques et sonores d'un clavier physique. Une fois encore, nous
installons ce petit programme en compilant directement son code source :

```
root ~ #  git clone https://git.sr.ht/~proycon/clickclack
root ~ #  cd clickclack
root ~/clickclack #  make PREFIX=/usr
root ~/clickclack #  make PREFIX=/usr install
```

## Invoquer un programme par un geste

Nous installons ici *ligsd*, un programme développé pour le Pinephone qui
permet d'associer une commande à un mouvement tracer sur un écran tactile. Ce
programme est conçu pour fonctionner sous X11 et Wayland. Ci-après nous ne le
compilons que pour le seul serveur d'affichage X11, évitant ainsi d'installer
les dépendances propres à Wayland :

```
root ~ #  git clone https://git.sr.ht/~mil/lisgd
root ~ #  cd lisgd
root ~/lisgd #  make PREFIX=/usr WITHOUT_WAYLAND=1
root ~/lisgd #  make PREFIX=/usr install
```

## Traiter les numéros de téléphone

Dans cette sous section, nous installons l'utilitaire *pnc*, un programme en
ligne de commande qui interface la bibliothèque _libphonenumber_ dont l'objet
est d'analyser, formatter et valider des numéros de téléphones. On commence donc
par installer cette bibliothèque via le gestionnaire de paquets du système et le
dépôt _bingch_. Nous renvoyons le lecteur vers *pinephone-gentoo*(7e) pour la
procédure permettant d'ajouter ce dépôt :

```
root ~ #  echo "dev-libs/libphonenumber::bingch ~arm64"
  >> /etc/portage/package.use/sxmo
root ~ #  emerge -av dev-libs/libphonenumber
```

Nous pouvons désormais installer le programme *pnc* à partir de son code
source :

```
root ~ #  git clone https://git.sr.ht/~anjan/pnc
root ~ #  cd pnc
root ~/pnc #  cmake -DCMAKE_INSTALL_PREFIX=/usr .
root ~/pnc #  make && make install
```

## Planifier et exécuter automatiquement une tâche

Ci après, nous installons un daemon Cron dont le but est d'exécuter
automatiquement des tâches planifiées par l'utilisateur. Gentoo propose
plusieurs mis mise en oeuvre d'un daemon Cron. Nous choisissons ici
d'installer le daemon *dcron* que nous ajoutons à la liste des services à
démarrer par défaut :

```
root ~ #  emerge -av sys-process/dcron
root ~ #  rc-service dcron start
root ~ #  rc-update add dcron default
```

L'objet de cette sous section n'est pas de documenter comment utiliser et
configurer Cron pour planifier l'exécution d'une tâche. Pour cela nous
renvoyons le lecteur vers la documentation Gentoo qui traite cette
question [4]. Nous nous attardons ici sur la seule partie propre à
l'utilisation d'un daemon Cron sur téléphone portable qui, par économie
d'énergie, se met en sommeil dès qu'il n'est pas utilisé. Or, une tâche
planifiée via le daemon Cron ne peut s'exécuter que si ce dernier est actif.
Pour palier cette contradiction, Sxmo utilise le programme *mnc*, installé
ci-après, qui affiche sur la sortie standard quand le prochaine tâche Cron
devra s'exécuter ; une donnée utilisée par Sxmo en combinaison de la commande
*rtcwake*(8) pour réveiller le système à temps.

```
root ~ #  git clone https://git.sr.ht/~anjan/mnc
root ~ #  cd mnc
root ~/mnc #  go build mnc.go
root ~/mnc #  cp -f mnc /usr/bin
```

## Gérer les copie de texte entre programmes

Nous allons désormais installer le programme *autocutsel* dont l'objectif est
d'assurer la cohérence des zones mémoires dédiées à la copie d'un texte. Dès
lors, un texte copié dans un programme peut être collé dans un autre
indépendamment du procédé de copie que chacun de ces programmes utilise :

```
root ~ #  git clone https://github.com/sigmike/autocutsel
root ~ #  cd autocutsel
root ~/autocutsel #  ./bootstrap
root ~/autocutsel #  ./configure --prefix=/usr
root ~/autocutsel #  make
root ~/autocutsel #  make install
```

## Système de notifications

Dans cette section nous installons le système de notifications *dunst* dont
l'objet est d'afficher temporairement un textes à l'écran. Sur Sxmo *dunst*
est notamment utilisé pour notifier à l'utilisateur la réception d'un message
ou d'un appel, la mise à jour du volume sonore ou du rétroéclairage, ou
encore une erreur système :

```
root ~ #  echo "x11-misc/dunst ~arm64" \
  >> /etc/portage/package.accept_keywords/sxmo
root ~ #  emerge -av x11-misc/dunst
```

## Masquer le pointeur

Nous installons ci-dessous le programme *unclutter-xfixes* utilisé pour
masquer l'icône du pointeur. En plus de ses dépendances à des bibliothèques
X11 déjà installées, ce programme s'appuie sur _libev_ que nous installons
ci-après via le gestionnaire de paquet du système :

```
root ~ #  emerge -av dev-libs/libev
```

Nous pouvons alors télécharger puis compiler les fichiers sources de
*unclutter-xfixes* avant de l'installer sur le système :

```
root ~ #  git clone https://github.com/Airblade/unclutter-xfixes
root ~ #  cd unclutter-xfixes
root ~/unclutter-xfixes #  make PREFX=/usr/ -j4
root ~/unclutter-xfixes #  make PREFX=/usr/ install
```

À noter que le programme installé se nomme *unclutter*, à savoir le nom du
programme original dont la variante *unclutter-xfixes* propose une mise en
oeuvre améliorée. Or, Sxmo s'attend à pouvoir invoquer un programme nommé
*unclutter-xfixes*. Pour répondre à cet attendu, nous créons simplement le
lien symbolique *unclutter-xfixes* qui référence l'exécutable effectivement
installé :

```
root ~/unclutter-xfixes #  ln -s /usr/bin/unclutter \
  /usr/bin/unclutter-xfixes
```

## Rétroéclairage

Dans cette sous sections nous installons *light*, un programme en ligne de
commande utilisé par Sxmo pour contrôler le rétroéclairage de l'écran :

```
root ~ #  git clone https://github.com/haikarainen/light
root ~ #  cd light
root ~/light #  ./autogen.sh
root ~/light #  ./configure --prefix=/usr
root ~/light #  make
root ~/light #  make install
```

## Capteur de proximité

Nous compilons et installons plusieurs outils qui accompagnent le noyau Linux et
permettent d'interroger et gérer différents convertisseurs et capteurs. Parmi
ceux-ci on notera les convertisseurs d'un signal digital vers analogique (et
vice versa) les accéléromètres, les gyroscopes ou encore les capteurs de
proximité. C'est ce dernier type de capteur que Sxmo vient notamment interroger
pour, par exemple, éteindre l'écran du Pinephone (et ainsi économiser ses
batteries) lors d'un appel pendant lequel le téléphone est porté à proximité de
l'oreille.

Ci après, nous allons dans les sources du noyau utilisé sur le système. À noter
que l'on peut utiliser la commande *uname -r* pour identifier la version du
noyau courant. Nous pouvons alors compiler puis installer les utilitaires du
sous système d'entrées/sorties industrielles (Industrial I/O) proposés par le
noyau Linux :

```
root ~ #  cd /usr/src/linux-<version>/
root /usr/src/linux-<version> #  make -C tools/iio
root /usr/src/linux-<version> #  make -C tools/iio install
```

Après installation, nous disposons sur le système du programme
*iio_event_monitor* utilisé par Sxmo pour interroger les évènements du capteurs
de proximité du Pinephone.

## Divers

linux-tools-iio

```
# Inutile ??
root ~ #  emerge -av net-dns/avahi
root ~ #  emerge -av dev-libs/libaio
root ~ #  git clone https://github.com/analogdevicesinc/libiio
root ~ #  cd libiio
root ~/libiio #  mkdir build && cd build
root ~/libiio/build #  cmake -DCMAKE_INSTALL_PREFIX=/usr ../
root ~/libiio/build #  make && make install

# Utile !!! la commande iio_event_monitor est explicitement utilisée
root ~ #  cd /usr/src/linux-<version>/
root /usr/src/linux-<version> #  make -C tools/iio
root /usr/src/linux-<version> #  make -C tools/iio install
```

superd

```
root ~ #  git clone https://git.sr.ht/~craftyguy/superd
root ~ #  cd superd
root ~/superd #  make
root ~/superd #  make PREFIX=/usr install

```

calendar

```
root ~ #  echo "app-misc/calendar ~arm64" >> \\
	/etc/portage/package.accept_keywords/sxmo
root ~ #  emerge -av app-misc/calendar
```

```
root ~ #  mkdir -p /etc/portage/package.accept_keywords
root ~ #  nano /etc/portage/package.accept_keywords/sxmo
nano:
  app-admin/conky ~arm64
  media-video/mpv ~arm64
  x11-apps/xcalc ~arm64
#  x11-misc/xcalib ~arm64
#  dev-libs/libphonenumber::bingch ~arm64
root ~ #  mkdir -p /etc/portage/package.use
root ~ #  nano /etc/portage/package.use/sxmo
nano:
  app-admin/conky truetype
  app-text/xmlto text
  media-libs/freetype harfbuzz
  media-libs/harfbuzz icu
#  media-libs/libsdl2 xinerama
#root ~ #  emerge -av x11-base/xorg-server
#root ~ #  emerge -av x11-libs/libXft
#root ~ #  emerge -av x11-libs/libXinerama
#root ~ #  emerge -av x11-libs/libxkbcommon
#root ~ #  emerge -av x11-libs/libXscrnSaver
#root ~ #  emerge -av x11-apps/xinput
#root ~ #  emerge -av x11-apps/xmodmap
#root ~ #  emerge -av x11-apps/xrandr
#root ~ #  emerge -av x11-apps/xsetroot
#root ~ #  emerge -av x11-misc/setxkbmap
#root ~ #  emerge -av x11-drivers/xf86-input-synaptics
#root ~ #  emerge -av x11-misc/xdotool
#root ~ #  emerge -av x11-misc/xdg-utils
#root ~ #  emerge -av x11-misc/xcalib
#root ~ #  emerge -av media-libs/libsdl2
#root ~ #  emerge -av dev-libs/libphonenumber
#root ~ #  emerge -av dev-libs/libev
root ~ #  emerge -av dev-lang/go
#root ~ #  emerge -av dev-libs/wayland
#root ~ #  emerge -av dev-libs/wayland-protocols
root ~ #  emerge -av app-admin/conky
#root ~ #  emerge -av media-fonts/dejavu
#root ~ #  emerge -av media-fonts/font-misc-misc
root ~ #  emerge -av media-sound/alsa-utils
root ~ #  emerge -av app-text/asciidoc
root ~ #  emerge -av sys-fs/inotify-tools
root ~ #  emerge -av media-gfx/sxiv
```


# INSTALLER SXMO

## Système de fenêtrage

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-dwm
root ~ #  cd sxmo-dwm
root ~/sxmo-dwm #  cp -f config.def.h config.h
root ~/sxmo-dwm #  make PREFIX=/usr
root ~/sxmo-dwm #  make PREFIX=/usr install
```

## Émulateur de terminal

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-st
root ~ #  cd sxmo-st
root ~/sxmo-st #  cp -f config.def.h config.h
root ~/sxmo-st #  make PREFIX=/usr
root ~/sxmo-st #  make PREFIX=/usr install
```

## Menu dynamique

Pour X11 :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-dmenu
root ~ #  cd sxmo-dmenu
root ~/sxmo-dmenu #  cp -f config.def.h config.h
root ~/sxmo-dmenu #  make PREFIX=/usr
root ~/sxmo-dmenu #  make PREFIX=/usr install
```

Pour le terminal :

```
root ~ #  git clone https://git.sr.ht/~stacyharper/bemenu
root ~ #  cd bemenu
root ~/bemenu #  make PREFIX=/usr
root ~/bemenu #  make PREFIX=/usr install
```

## Exécuter un programme via un geste

```
root ~ #  git clone https://git.sr.ht/~mil/lisgd
root ~ #  cd lisgd
root ~/lisgd #  cp -f config.def.h config.h
root ~/lisgd #  make PREFIX=/usr
root ~/lisgd #  make PREFIX=/usr install
```

## Clavier virtuel

```
root ~ #  git clone https://git.sr.ht/~proycon/svkbd
root ~ #  cd svkbd
root ~/svkbd #  cp -f config.def.h config.h
root ~/svkbd #  make PREFIX=/usr
root ~/svkbd #  make PREFIX=/usr install
```

## Vibrations et sons du clavier virtuel

```
root ~ #  git clone https://git.sr.ht/~proycon/clickclack
root ~ #  cd clickclack
root ~/clickclack #  make PREFIX=/usr
root ~/clickclack #  make PREFIX=/usr install
```

## Traiter les numéros de téléphone

```
root ~ #  echo "dev-libs/libpinephonenumber::bingch ~arm64"
  >> /etc/portage/package.use/sxmo
root ~ #  emerge -av dev-libs/libphonenumber
```

```
root ~ #  git clone https://github.com/Orange-OpenSource/pn
root ~ #  cd pn
root ~/pn #  cmake -DCMAKE_INSTALL_PREFIX=/usr .
root ~/pn #  make && make install
```

## Exécution automatique de scripts

```
root ~ #  git clone https://git.sr.ht/~anjan/mnc
root ~ #  cd mnc
root ~/mnc #  go build mnc.go
root ~/mnc #  cp -f mnc /usr/bin
```

## Gestion du copier/coller

```
root ~ #  git clone https://github.com/sigmike/autocutsel
root ~ #  cd autocutsel
root ~/autocutsel #  ./bootstrap
root ~/autocutsel #  ./configure --prefix=/usr
root ~/autocutsel #  make
root ~/autocutsel #  make install
```

## Notifications

```
root ~ #  git clone https://github.com/dunst-project/dunst
root ~ #  cd dunst
root ~/dunst #  make PREFIX=/usr -j4
root ~/dunst #  make PREFIX=/usr install
```

## Masquer le curseur

```
root ~ #  emerge -av dev-libs/libev
root ~ #  git clone https://github.com/Airblade/unclutter-xfixes
root ~ #  cd unclutter-xfixes
root ~/unclutter-xfixes #  make PREFX=/usr/ -j4
root ~/unclutter-xfixes #  make PREFX=/usr/ install
root ~/unclutter-xfixes #  ln -s /usr/bin/unclutter \
  /usr/bin/unclutter-xfixes
```

## Rétroéclairage

```
root ~ #  git clone https://github.cil/haikarainen/light
root ~ #  cd light
root ~/light #  ./autogen.sh
root ~/light #  ./configure --prefix=/usr
root ~/light #  make
root ~/light #  make install
```

# INSTALLER LES APPLICATIONS

```
```

## Calculatrice

```
root ~ #  echo "x11-apps/xcalc ~arm64" >>
  /etc/portage/package.accept_keywords/sxmo
root ~ #  emerge -av x11-apps/xcalc
root ~ #  emerge -av media-fonts/terminus-font
```

Scripts Bash utilitaires :

```
root ~ #  git clone https://git.sr.ht/~mil/sxmo-utils
root ~ #  cd sxmo-utils
root ~ #  git clone -b lafrier
root ~ #  nano scripts/core/sxmo_common.sh
nano:
  if [ OS="Gentoo" ]; then
    export OS="postmarketos"
  fi
root ~ #  nano scripts/mode/sxmo_modem.sh
nano
  checkmodem() {
    # ...

    # Enable the if necessary
    state=$(mmcli -m "$(modem_n)")
    if echo "$state" | grep -q -E "^.*state:.*disabled.*$"; then
      mmcli -m $(modem_n) --enable
    fi
  }

root ~ #  nano scripts/core/sxmo_wifitoggle.sh
nano:
  #!/usr/bin/env sh
  [ -n $"WLAN_MODULE" ] || WLAN_MODULE="8723cs"
  on() {
    if lsmod | grep -qE "$WLAN_MODULE"; then
      rc-service net.wlan0 start
    else
      modprobe "$WLAN_MODULE" && rc-service net.wlan0 start
    fi
  }
  off() {
    if lsmod | grep -qE "$WLAN_MODULE"; then
      rc-service net.wlan0 stop && modprobe -r "$WLAN_MODULE"
    else
      rc-service net.wlan0 stop
    fi
  }
  case "$1" in
    on)
      on
      ;;
    off)
      off
      ;;
    *) #toggle
      if rc-status | grep wlan | grep -qE "started"; then
        off
      else
        on
     fi
  esac
  sxmo_statusbarupdate.sh
root ~ #  nano scripts/core/sxmo_upgrade.sh
nano:
  . "$(dirname "$0")/sxmo_common.sh
  echo "Updating all packages from repositories"
  sudo emerge --sync
  echo "Upgrading al packages"
  sudo emerge -uvaDUN @world
root ~/sxmo-utils #  make PREFIX=/usr
root ~/sxmo-utils #  make PREFIX=/usr install
root ~/sxmo-utils #  mv /etc/doas.conf /tmp
root ~/sxmo-utils #  ln -s  /etc/doas.d/sxmo.conf /etc/doas.conf
root ~/sxmo-utils #  rc-update add sxmo-setpermissions default
```

gojq :

```
root ~ #  git clone https://github.com/itchyny/gojq
root ~ #  cd gojq
root ~/gojq #  make
root ~/gojq #  cp gojq /usr/bin
```

MMS :

```
root ~ #  emerge -av net-libs/libsoup
root ~ #  emerge -av net-dns/c-ares
root ~ #  emerge -av dev-libs/json-c
root ~ #  emerge -av app-misc/jq
root ~ #  emerge -av net-misc/mobile-broadband-provider-info
root ~ #  emerge -av net-misc/networkmanager
root ~ #  nano /etc/init.d/networkmanager
nano:
  #!/sbin/openrc-run
  supervisor=supervise-daemon
  command=/usr/sbin/NetworkManager
  command_args_foreground="-n" # Do not daemonize. Laisse gérer le superviseur

  description="Network Manager Daemon"

  depend() {
    need dbus
    provide net
  }
root ~ #  rc-update add networkmanager default
root ~ #  rc-update del net.wlan0 default
root ~ #  nmcli connection add con-name mmsfree type gsm apn mmsfree
root ~ #  nmcli connection show
root ~ #  nmcli connection up mmsfree
root ~ # git clone https://gitlab.com/kop316/mmsd/
root ~ # cd mmsd
root ~/mssd # nano meson_options.txt
nano:
  option('build-mmsctl', type : 'boolean', value : true)
root ~/mmsd # meson --prefix=/usr build
root ~/mmsd # ninja -C build
root ~/mmsd # ninja -C build install
```

# GPS

## gpsd

```
root ~ #  echo "app-mobilephone/pinephone-modem-scripts::bignch ~arm64"
  >> /etc/portage/package.accept_keywords/sxmo
root ~ #  echo "sci-geosciences/gpsd ~arm64"
  >> /etc/portage/package.accept_keywords/sxmo
root ~ #  echo "net-dialup/atinout::bingch ~arm64"
  >> /etc/portage/package.accept_keywords/sxmo
root ~ #  emerge -av pinephone-modem-scripts
root ~ #  #emerge -av sci-geosciences/gpsd # INSTALLER PAR LES SCRIPTS
root ~ #  #emerge -av net-dialup/atinout # INSTALLER PAR LES SCRIPTS
root ~ #  rc-update add default gpsd
root ~ #  mv /usr/lib/udev/rules.d/80-modem-eg25.rules ~/ # EVITE LE CRASH DE EG25 quand on vient lire /dev/EG25.AT
root ~ #  /etc/gpsd/device-hook "" ACTIVATE # Démarrer le modem
root ~ #  mmcli -m any --location-status
root ~ #  mmcli -m any --location-enable-agps-msb
root ~ #  mmcli -m any --location-enable-gps-nmea
root ~ #  mmcli -m any --location-enable-gps-raw
root ~ #  cgps # ou gpsmon
```

## mepo

```
lafrier ~/gps $  doas echo "dev-lang/zig-bin ~arm64" \
  >> /etc/portage/package.accept_keywords/sxmo
lafrier ~/gps $  doas echo "media-libs/sdl2-image png" \
  >> /etc/portage/package.use/sxmo
lafrier ~/gps $  doas emerge -av geoclue
lafrier ~/gps $  doas emerge -av dev-lang/zig-bin
lafrier ~/gps $  doas emerge -av media-libs/sdl2-ttf
lafrier ~/gps $  doas emerge -av media-libs/sdl2-gfx # Impossibe sur Pinephone
lafrier ~/gps $  wget http://www.ferzkopp.net/Software/SDL2_gfx/SDL2_gfx-1.0.4.tar.gz
lafrier ~/gps $  tar xzvf SDL2_gfx/SDL2_gfx-1.0.4.tar.gz
lafrier ~/gps $  cd SDL2_gfx-1.0.4
lafrier ~/gps/SDL2_gfx-1.0.4 $  ./configure \
  --build=aarch64-unknown-linux-gnu --enable-mmx=no --prefix=/usr
lafrier ~/gps/SDL2_gfx-1.0.4 $  make && doas make install
lafrier ~/gps $  doas emerge -av app-misc/jq
lafrier ~/gps $  doas emerge -av x11-misc/xdotool
lafrier ~/gps $  doas emerge -av gnome-extra/zenity
lafrier ~/gps $  git clone https://git.sr.ht/~mil/mepo
lafrier ~/gps $  cd mepo
lafrier ~/gps/mepo $  zig build
lafrier ~/gps/mepo $  export PATH+=:~/gps/mepo/zig-out/bin/
lafrier ~/gps/mepo $  mepo
```

RQ : Très prometteur

```
root / #  rc-update add sxmo-setpermissions default
```

Firefox :

```
root / #  mkdir -p /usr/aarch64-unknown-linux-gnu/usr/lib64/
root / #  ln -s /usr/lib64/libtinfo.so \
  /usr/aarch64-unknown-linux-gnu/usr/lib64/libtinfo.so
root / #  emerge -av www-client/firefox
root / #  echo "app-mobilephone/mobile-config-firefox::bingch ~arm64" >>
  /etc/portage/package.use/firefox
root / #  echo "app-mobilephone/mobile-config-firefox::bingch ~arm64" >>
  /etc/portage/package.accept_keywords/firefox
root / #  emerge -av app-mobilephone/mobile-config-firefox
```

Connaitre depuis combien de temps le serveur X est inactif (utile à l'autolock) :

```
root ~ #  emerge -av sys-devel/automake:1.15 # besoin de la version 1.15
root ~ #  git clone https://github.com/lucianposton/xprintidle
root ~ #  cd xprintidle
root ~/xprintidle #  ./configure --prefix=/usr
root ~/xprintidle #  make
root ~/xprintidle #  make install
```

# FONTS

```
pine64-pinephone:~$  cp -r /usr/share/fonts/OTF/ _
  /mnt/gentoo/usr/share/fonts/fira-mono-nerd
root / #  fc-cache -r
```

# RÉSEAU

```
root ~ #  USE=-modemmanager emerge -av net-misc/networkmanager
```

# DIVERS

Éviter de compiler rust à partir des sources et utiliser le binaire à la
place :

```
root / #  mkdir -p /etc/portage/package.mask/
root / #  echo "dev-lang/rust" >> /etc/portage/package.mask/rust
```

# VOIR AUSSI

. https://sxmo.org/
. https://suckless.org/
. https://www.nerdfonts.com/
. https://wiki.gentoo.org/wiki/Cron/en

*pinephone-gentoo*(7e)
