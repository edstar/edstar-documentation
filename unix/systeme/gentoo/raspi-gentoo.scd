raspi-gentoo(7e)

# NAME
raspi-gentoo - Installer gentoo sur un ordinateur de type _raspberry pi_ (à partir de la version 3).

# INTRODUCTION
Cette documentation décrit l'installation de la distribution Linux Gentoo sur un ordinateur de type _raspberry pi_. Il ne s'agit que de l'installation. Pour la gestion courante du système voir *gentoo*(7e). Pour l'installation d'un logiciel nommé _toto_, voir si la page de manuel *toto-gentoo*(7e) existe. Pour l'installation d'un service nommé _toto_, voir si la page *toto-serveur-gentoo*(7e) existe.

# PREPARATION DE LA CARTE SD

## PARTITIONNEMENT ET FORMATTAGE
On utilise un ordinateur avec un système d'exploitation _linux_ sous la distribtion _gentoo_. On insère la carte SD du raspberry pi dans un lecteur et on cherche son nom de peripherique sous _/dev_ (il apparaît puis disparaît quand on connecte puis deconnecte la carte). Typiquement, le nom de périphérique peut être
```
/dev/mmcblk0
```
On partitionne cette carte avec _parted_. On commence par enlever les partitions existantes (si besoin), par exemple la partition 1 :
```
root#  parted -a optimal /dev/mmcblk0
parted:
  rm 1
  quit
```
On prépare trois partitions : une pour _/boot_ (typiquement 500 MB), une pour le swap (typiquement deux fois la taille de la RAM du raspberry pi, par exemple 2×1GB pour un raspberry pi 3B+), et une pour le système et les données (le reste de la carte) :
```
root#  parted -a optimal /dev/mmcblk0
parted:
  mklabel msdos
  unit mib
  mkpart primary 1 500
  set 1 boot on
  set 1 lba on
  mkpart primary 500 2500
  set 2 lba off
  mkpart primary 2500 -1
  set 3 lba off
  print
  quit
```
Lors du print, on obtient typiquement la sortie suivante :
```
Modèle : SD SC64G (sd/mmc)
Disque /dev/mmcblk0 : 60906MiB
Taille des secteurs (logiques/physiques) : 512B/512B
Table de partitions : msdos
Drapeaux de disque :

Numéro  Début    Fin       Taille    Type     Système de fichiers  Drapeaux
 1      1,00MiB  500MiB    499MiB    primary                       démarrage, lba
 2      500MiB   2500MiB   2000MiB   primary
 3      2500MiB  60905MiB  58405MiB  primary
```
Ensuite on formate les partitions :
```
root#  mkfs.fat -F 32 /dev/mmcblk0p1
root#  mkswap /dev/mmcblk0p2
root#  mkfs.ext4 /dev/mmcblk0p3
```
Après formattage, si on relance parted pour faire un print, on obtient :
```
Modèle : SD SC64G (sd/mmc)
Disque /dev/mmcblk0 : 63,9GB
Taille des secteurs (logiques/physiques) : 512B/512B
Table de partitions : msdos
Drapeaux de disque :

Numéro  Début   Fin     Taille  Type     Système de fichiers  Drapeaux
 1      1049kB  524MB   523MB   primary  fat32                démarrage, lba
 2      524MB   2621MB  2097MB  primary  linux-swap(v1)
 3      2621MB  63,9GB  61,2GB  primary  ext4
```

RQ: Parfois on observe que _lba_ a disparu. On relance alors _parted_ pour le remettre :
```
root#  parted -a optimal /dev/mmcblk0
parted:
  set 1 lba on
```

## COPIER LES FIRMWARES
On démonte la partition _/boot_ courante et on monte à la place celle de la carte SD :
```
root#  umount /boot
root#  mount /dev/mmcblk0p1 /boot
```
On accepte la license de raspberry pour le paquet de firmware _sys-boot/raspberrypi-firmware_ :
```
root#  echo "sys-boot/raspberrypi-firmware raspberrypi-videocore-bin" >> /etc/portage/package.license/raspi
```
On demasque le paquet _sys-boot/raspberrypi-firmware_ en acceptant l'architecture _arm_ :
```
root#  echo "sys-boot/raspberrypi-firmware ~arm" > /etc/portage/package.accept_keywords/raspi-tmp
```
On émerge le paquet de firmware :
```
root#  emerge -av sys-boot/raspberrypi-firmware
```
On remasque le paquet de firmware :
```
root#  rm /etc/portage/package.accept_keywords/raspi-tmp
```
Enfin, on démonte la partition _/boot_ de la carte SD et on remonte la partition _/boot_ usuelle :
```
root#  sync /boot
root#  umount /boot
root#  mount /boot
```

## COPIER LE NOYAU
On démonte la partition _/boot_ courante et on monte à la place celle de la carte SD :
```
root#  umount /boot
root#  mount /dev/mmcblk0p1 /boot
```
On accepte la license de raspberry pour le paquet de firmware _sys-boot/raspberrypi-firmware_ :
```
root#  echo "sys-kernel/raspberrypi-image raspberrypi-videocore-bin" >> /etc/portage/package.license/raspi
```
On demasque le paquet _sys-kernel/raspberrypi-image_ en acceptant l'architecture _arm_ :
```
root#  echo "sys-kernel/raspberrypi-image ~arm" > /etc/portage/package.accept_keywords/raspi-tmp
```
On émerge le paquet contenant le noyau :
```
root#  emerge -av sys-kernel/raspberrypi-image
```
On remasque le paquet de firmware :
```
root#  rm /etc/portage/package.accept_keywords/raspi-tmp
```
On crée le fichier _cmdline.txt_ en adaptant en fonction du partitionnement :
```
root#  echo "root=/dev/mmcblk0p3 rootdelay=2" > /boot/cmdline.txt
```
Enfin, on démonte la partition _/boot_ de la carte SD et on remonte la partition _/boot_ usuelle :
```
root#  sync /boot
root#  umount /boot
root#  mount /boot
```

ATTENTION ! Lors du emerge de raspberrypi-image, on note les noms des répertoires dans lesquels les modules des différents noyaux ont été installés (par exemple /lib/modules/5.10.17+). Nous recopierons ces répertoires à la même place sur le raspberry (sur la partition racine) pendant l'installation du système.


## INSTALLATION DU SYSTEME
On monte la partition système de la carte SD :
```
root#  mount /dev/mmcblk0p3 /mnt/mmcblk0p3
```
Pour la suite il faut identifier le modèle de _raspberry pi_ dont on dispose~: soit un modèle de la série initiale, soit un _raspberry pi 2_ ou un _raspberry pi 3_.

*stage3 : série initiale*++
On commence par consulter la liste des systèmes _stage3_ disponibles à l'adress web suivante (_stage3_, _arm_, _hardfp_, _openrc_, version 6) :
```
http://distfiles.gentoo.org/releases/arm/autobuilds/current-stage3-armv6j_hardfp-openrc
```
On identifie la version correspondant à ces cinq options, au format _tar.xz_, on la télécharge sur la partition système de la carte SD et on l'extrait :
```
root#  cd /mnt/mmcblk0p3
root#  wget http://distfiles.gentoo.org/releases/arm/autobuilds/current-stage3-armv6j_hardfp-openrc/stage3-armv6j_hardfp-openrc-20211126T230641Z.tar.xz
root#  tar xpvf stage3-armv6j_hardfp-openrc-20211126T230641Z.tar.xz --xattrs-include='*.*' --numeric-owner
```
On copie les modules que l'on a installés (sur le gentoo existant) dans la section précédente. On peut aussi attendre de booter sur le raspberry et faire _uname -a_ pour trouver le nom du noyau utilisé.
```
root#  mkdir -p /mnt/mmcblk0p3/lib/modules
root#  cp -r /lib/modules/5.10.17+ /mnt/mmcblk0p3/lib/modules
```
Ensuite on édite le fihier _/mnt/mmcblk0p3/etc/portage/make.conf_. On commente les deux lignes définissant _CFLAGS_ et _CXXFLAGS, et on les remplace par les suivantes :
```
root#  nano /mnt/mmcblk0p3/etc/portage/make.conf
nano:
  CFLAGS="-O2 -march=armv6j -mfpu=vfp -mfloat-abi=hard"
  CXXFLAGS="${CFLAGS}"
```
On édite le fichier _/mnt/mmcblk0p3/etc/fstab_ :
```
root#  nano /mnt/mmcblk0p3/etc/fstab
nano:
  /dev/mmcblk0p1              /boot           vfat            noauto,noatime  1 2
  /dev/mmcblk0p3              /               ext4            noatime         0 1
  /dev/mmcblk0p2              none            swap            sw              0 0
```
On choisi un clavier français :
```
root#  nano /mnt/mmcblk0p3/etc/conf.d/keymaps
nano:
  keymap="fr-pc"
```
On définit le mot de passe de root. Pour celà, on saisit la commande suivante (et on suit les instructions) :
```
root#  openssl passwd -1
```
Cela renvoie un ligne de _hash_, typiquement _$1$RBHM/tzX$vUbZpu8.0ZQ0e4yud7G4k1_. On recopie ce _hash_ dans la ligne de _root_ du fichier _/mnt/mmcblk0p3/etc/shadow_, soit typiquement (on conserve les chiffres de la fin, ici _10770:0_, qui peuvent donc être différents) :
```
root#  nano /mnt/mmcblk0p3/etc/shadow
nano:
  root:$1$RBHM/tzX$vUbZpu8.0ZQ0e4yud7G4k1:10770:0:::::
```
On démonte enfin la partition :
```
root#  sync /mnt/mmcblk0p3
root#  umount /mnt/mmcblk0p3
```
et on insère la carte SD dans le _raspberry pi_ pour faire un premier test : le système doit booter et on doit pouvoir se logger sous root avec le mot de passe que l'on a défini plus haut.

*stage3 : raspberry pi 2 ou raspberry pi 3*++
On commence par consulter la liste des systèmes _stage3_ disponibles à l'adress web suivante (_stage3_, _arm_, _hardfp_, _openrc_, version 7) :
```
http://distfiles.gentoo.org/releases/arm/autobuilds/current-stage3-armv7a_hardfp-openrc
```
On identifie la version correspondant à ces cinq options, au format _tar.xz_, on la télécharge sur la partition système de la carte SD et on l'extrait :
```
root#  cd /mnt/mmcblk0p3
root#  wget http://distfiles.gentoo.org/releases/arm/autobuilds/current-stage3-armv7a_hardfp-openrc/stage3-armv7a_hardfp-openrc-20220208T230729Z.tar.xz
root#  tar xpvf stage3-armv7a_hardfp-openrc-20220208T230729Z.tar.xz  --xattrs-include='*.*' --numeric-owner
```
On copie les modules que l'on a installés (sur le gentoo existant) dans la section précédente. On peut aussi attendre de booter sur le raspberry et faire _uname -a_ pour trouver le nom du noyau utilisé.
```
root#  mkdir -p /mnt/mmcblk0p3/lib/modules
root#  cp -r /lib/modules/5.10.63-v7+ /mnt/mmcblk0p3/lib/modules
```
Ensuite on édite le fihier _/mnt/mmcblk0p3/etc/portage/make.conf_. On commente les deux lignes définissant _CFLAGS_ et _CXXFLAGS, et on les remplace par les suivantes :
```
root#  nano /mnt/mmcblk0p3/etc/portage/make.conf
nano:
  COMMON_FLAGS="-O2 -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard"
```
On édite le fichier _/mnt/mmcblk0p3/etc/fstab_ :
```
root#  nano /mnt/mmcblk0p3/etc/fstab
nano:
  /dev/mmcblk0p1              /boot           vfat            noauto,noatime  1 2
  /dev/mmcblk0p3              /               ext4            noatime         0 1
  /dev/mmcblk0p2              none            swap            sw              0 0
```
On choisi un clavier français :
```
root#  nano /mnt/mmcblk0p3/etc/conf.d/keymaps
nano:
  keymap="fr-pc"
```
On définit le mot de passe de root. Pour celà, on saisit la commande suivante (et on suit les instructions) :
```
root#  openssl passwd -1
```
Cela renvoie un ligne de _hash_, typiquement _$1$RBHM/tzX$vUbZpu8.0ZQ0e4yud7G4k1_. On recopie ce _hash_ dans la ligne de _root_ du fichier _/mnt/mmcblk0p3/etc/shadow_, soit typiquement (on conserve les chiffres de la fin, ici _10770:0_, qui peuvent donc être différents) :
```
root#  nano /mnt/mmcblk0p3/etc/shadow
nano:
  root:$1$RBHM/tzX$vUbZpu8.0ZQ0e4yud7G4k1:10770:0:::::
```
On démonte enfin la partition :
```
root#  sync /mnt/mmcblk0p3
root#  umount /mnt/mmcblk0p3
```
et on insère la carte SD dans le _raspberry pi_ pour faire un premier test : le système doit booter et on doit pouvoir se logger sous root avec le mot de passe que l'on a défini plus haut.

*Problème de terminal*++
En l'état le terminal du raspberry se coince régulièrement avec une allerte du type "S0 ... spawning too fast". Pour l'enlever commenter une ligne dans _/etc/inittab_ :
```
root#  nano /etc/inittab
nano:
  # SERIAL CONSOLES
  #S0:12345:respawn:/sbin/agepty -L 9600 ttyS0 vt100
```

TEMOIGNAGE : Pour un écran acheté pour le raspberry pi, l'affichage était inadapté (lignes qui masquent les caractères, suelement une partie de l'écran utilisée, le reste en miroir). La solution a consisté à créer un fichier _/boot/config.txt_ avec le contenu suivant :
```
# Ecran:
if 7inch HDMI LCD (C)-(1024 600)
max_usb_current=1
hdmi_group=2
hdmi_mode=1
hdmi_mode=87
hdmi_cvt 1024 600 60 6 0 0 0
hdmi_drive=1
```

*L'arbre de portage*++
On va chercher l'arbre de portage sur le réseau, à partir de la machine gentoo pré-existante :
```
lafrier$  cd /tmp
lafrier$  wget https://distfiles.gentoo.org/snapshots/portage-latest.tar.xz
```
On monte la partition _ext4_ de la carte SD sur la machine gentoo pré-existante, par exemple sous _/mnt/mmcblk0p3_. On vérifie qu'il y a au moins 118K Inodes disponibles :
```
lafrier$  df -ih /mnt/mmcblk0p3
df:
  /dev/mmcblk0p3     856K    58K   799K    7% /mnt/mmcblk0p3
```
On extrait le fichier _portage-latest.tar.bz2_ sur la carte SD :
```
root#  cd /tmp
root#  tar xJvf portage-latest.tar.xz -C /mnt/mmcblk0p3/var/db/repos/.
root#  mv /mnt/mmcblk0p3/var/db/repos/portage /mnt/mmcblk0p3/var/db/repos/gentoo
root#  sync /mnt/mmcblk0p3
root#  umount /mnt/mmcblk0p3
```

On place la carte SD dans le raspberry et on boote. Ensuite on liste les _profile_ et les _binutils_ existants :
```
root#  eselect profile list
eselect:
  [25] default/linux/arm/17.0/armv6j (stable) *
root#  eselect binutils list
eselect:
  [1] armv6j-unknown-linux-gnueabihf-2.37_p1 *
```
Pour les versions initiales, cela donne
```
[25] default/linux/arm/17.0/armv6j (stable) *
[1] armv6j-unknown-linux-gnueabihf-2.37_p1 *
```
Pour les versions 2 et 3, cela donne
```
[34] default/linux/arm/17.0/armv7a (stable) *
[1] armv7a-unknown-linux-gnueabihf-2.37_p1 *
```

*Compilation croisée*++
On prépare le linux existant pour qu'il puisse servir à la compilation des paquets du raspberry. Pour cela on installe _crossdev_ (voir *crossdev-gentoo*(7e)) :
```
root#  emerge -av sys-devel/crossdev
```
Ensuite on compile une version de _crossdev_ correspondant à l'architecture identifiée plus haut, sur le rasperry, avec la commande _eselect binutils list_. Par exemple, avec _armv6j-unknown-linux-gnueabihf_ :
```
root#  mkdir -p /var/db/repos/cross-armv6j-unknown-linux-gnueabihf/{profiles,metadata}
root#  echo "cross-armv6j-unknown-linux-gnueabihf" >> /var/db/repos/cross-armv6j-unknown-linux-gnueabihf/profiles/repo_name
root#  nano /var/db/repos/cross-armv6j-unknown-linux-gnueabihf/metadata/layout.conf
nano:
  masters = gentoo
  thin-manifests = true
root#  nano /etc/portage/repos.conf/crossdev.conf
nano:
  [cross-armv6j-unknown-linux-gnueabihf]
  location = /var/db/repos/cross-armv6j-unknown-linux-gnueabihf
  priority = 10
  masters = gentoo
  auto-sync = no
root#  crossdev --stable --target armv6j-unknown-linux-gnueabihf
```
Ensuite on augmente la priorité de cet overlay de façon à ce que la prochaine fois, pour une nouvelle architecture, la priorité 10 soit celle qui soit retenue :
```
root#  nano /etc/portage/repos.conf/crossdev.conf
nano:
  [cross-armv6j-unknown-linux-gnueabihf]
  location = /var/db/repos/cross-armv6j-unknown-linux-gnueabihf
  priority = 1000
  masters = gentoo
  auto-sync = no
```

On va ensuite enrichir le _make.conf_ de _/usr/armv6j-unknown-linux-gnueabihf/etc/portage/make.conf_ (par exemple) pour y ajouter les informations que l'on avait mises dans le _make.conf_ du raspberry (en fonction de la version). On ajoute aussi la _FEATURE_ _buildpkg_ pour imposer que les commandes emerge produisent des paquets binaires qui pourraont ensuite être installés sur le raspberry :

Pour les versions initiales :
```
root#  nano /usr/armv6j-unknown-linux-gnueabihf/etc/portage/make.conf
nano:
  #ACCEPT_KEYWORDS="${ARCH} ~${ARCH}"
  ACCEPT_KEYWORDS="${ARCH}"

  #USE="${ARCH} -pam"
  USE="${ARCH}"

  #CFLAGS="-O2 -pipe -fomit-frame-pointer"
  CFLAGS="-O2 -march=armv6j -mfpu=vfp -mfloat-abi=hard"
  CXXFLAGS="${CFLAGS}"

  FEATURES="-collision-protect sandbox buildpkg noman noinfo nodoc"
root#  mv /usr/armv6j-unknown-linux-gnueabihf/etc/portage/make.profile /tmp
root#  ln -s /var/db/repos/gentoo/profiles/default/linux/arm/17.0/armv6j \
    /usr/armv6j-unknown-linux-gnueabihf/etc/portage/make.profile
```

Pour les versions 2 et 3 :
```
root#  nano /usr/armv7a-unknown-linux-gnueabihf/etc/portage/make.conf
nano:
  #ACCEPT_KEYWORDS="${ARCH} ~${ARCH}"
  ACCEPT_KEYWORDS="${ARCH}"

  #USE="${ARCH} -pam"
  USE="${ARCH}"

  #CFLAGS="-O2 -pipe -fomit-frame-pointer"
  CFLAGS="-O2 -march=armv7-a -mfpu=neon-vfpv4 -mfloat-abi=hard"
  CXXFLAGS="${CFLAGS}"

  FEATURES="-collision-protect sandbox buildpkg noman noinfo nodoc"
```

On doit aussi redéfinir le _make.profile_ pour le faire pointer vers les profiles que l'on a choisi sur le raspberry (voir plus haut).

Pour les versions initiales:
```
root#  cd /usr/armv6j-unknown-linux-gnueabihf/etc/portage
root#  rm make.profile
root#  ln -s /var/db/repos/gentoo/profiles/default/linux/arm/17.0/armv6j make.profile
```

Pour les versions 2 et 3 :
```
root#  cd /usr/armv7a-unknown-linux-gnueabihf/etc/portage
root#  rm make.profile
root#  ln -s /var/db/repos/gentoo/profiles/default/linux/arm/17.0/armv7a make.profile
```

A ce stade, toute une série de commandes commençant par _armv6j-unknown-linux-gnueabihf-_ ou _armv7a-unknown-linux-gnueabihf-_ sont disponibles pour la compilation, dont _armv6j-unknown-linux-gnueabihf-emerge_ et _armv7a-unknown-linux-gnueabihf-emerge_ qui permettront de préparer des paquets binaires pour le raspberry.

*La carte wifi*++
On met à jour l'arbre de portage de la machine existante (de façon à avoir le même arbre sur les deux machines). Puis on met à jour le paquet _linux-firmware_ :
```
root#  emerge --sync
root#  emerge -av sys-kernel/linux-firmware
```

On monte la carte SD et on recopie les fichiers du paquet _linux-firmware_ sur la carte à l'adresse _/var/cache/distfiles_ :
```
root#  cd /mnt/mmcblk0p3/var/cache/distfiles
root#  cp /var/cache/distfiles/linux-firmware* .
root#  cd
root#  sync /mnt/mmcblk0p3
root#  umount /mnt/mmcblk0p3
```

On repasse sur le raspberry et on prépare _portage_ pour la question de licence nécessaire à _linux-firmware_. Puis on installe _linux-firmware_ :
```
root#  cd /etc/portage
root#  mkdir package.license
root#  cd package.license
root#  nano -w linux-firmware
nano:
  sys-kernel/linux-firmware linux-fw-redistributable no-source-code
root#  emerge -av sys-kernel/linux-firmware
```
RQ: Bien regarder quelle version de _linux-firmware_ a été installée et effacer les autres fichiers _linux-firmware\*_ dans _/var/cache/distfiles_ (pour faire de la place).

On installe _openrc_ en utilisant la compilation croisée. Donc on passe sur la machine gentoo existante et on prépare un paquet binaire correspondant à _sys-apps/openrc_ :
```
root#  armv6j-unknown-linux-gnueabihf-emerge -av sys-apps/openrc
```
RQ: Ca ne marche pas. On rencontre un problème de dépendendance circulaire lié _libcap_ et _pam_. On casse temporairement cette dépendance cicrulaire en créant un fichier _libcap_ dans _package.use_, puis on l'enlève et on remet à jour :
```
root#  cd /usr/armv6j-unknown-linux-gnueabihf/etc/portage
root#  mkdir package.use
root#  cd package.use
root#  echo "sys-libs/libcap -pam" >> libcap
root#  armv6j-unknown-linux-gnueabihf-emerge -av sys-apps/openrc
root#  rm libcap
root#  armv6j-unknown-linux-gnueabihf-emerge -av sys-libs/libcap
root#  armv6j-unknown-linux-gnueabihf-emerge -av sys-apps/openrc
```

On installe _dhcpcd_ et _wpa_supplicant_ en utilisant la compilation croisée. Donc on passe sur la machine gentoo existante et on prépare des paquets binaires correspondant à _net-misc/dhcpcd_ et _net-wireless/wpa_supplicant_ :
```
root#  armv6j-unknown-linux-gnueabihf-emerge -av net-misc/dhcpcd
root#  armv6j-unknown-linux-gnueabihf-emerge -av net-wireless/wpa_supplicant
```

On monte le disque SD sur la machine gentoo existante et on recopie dessus les paquets qui viennent d'être créés :
```
root#  mkdir /mnt/mmcblk0p3/var/db/binpkgs
root#  cd /usr/armv6j-unknown-linux-gnueabihf/packages
root#  rsync -avr * /mnt/mmcblk0p3/var/db/binpkgs/
root#  nano /mnt/mmcblk0p3/etc/portage/make.conf
nano:
  PKGDIR=/var/db/binpkgs
root#  sync /mnt/mmcblk0p3
```
On démonte le disque SD et on reboot le raspberry, puis sur le raspberry on installe _net-wireless/wpa_supplicant_ :
```
root#  emerge -av --usepkgonly sys-apps/openrc
root#  emerge -av --usepkgonly net-misc/dhcpcd
root#  emerge -av --usepkgonly net-wireless/wpa_supplicant
```

On voit que le service _wpa_supplicant_ existe en l'activant et en le desactivant :
```
root#  rc-service wpa_supplicant start
root#  rc-service wpa_supplicant restart
root#  rc-service wpa_supplicant stop
```

On prépare l'environnement réseau :
```
root#  ln -s /etc/init.d/net.lo /etc/init.d/net.wlan0
root#  rc-update add net.wlan0 default
root#  touch /etc/wpa_supplicant/wpa_supplicant.conf
root#  chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf
```
Puis on édite _/etc/wpa_supplicant/wpa_supplicant.conf_ pour déclarer une connexion (voir *wpa_supplicant*(8e)).

On teste vérifie et on teste la connexion :
```
root#  ifconfig # il apparait dans le block wlan0 inet et netmask pour ipv4
root#  ping gnu.org
```


Sur le raspberry :

```
root#  echo "Europe/Paris" > /etc/timezone
root#  emerge --config sys-libs/timezone-data
```
La réponse est que localtime existe déjà et qu'il faut le supprimer puis relancer la commande emerge.
```
root#  mv /etc/localtime /etc/localtime_old
root#  emerge --config sys-libs/timezone-data
root#  ls -l /etc/   # Ça répond pour localtime :  ...... localtime -> ../usr/share/zoneinfo/Europe/Paris
```


Aller chercher les mirroirs gentoo et le copier dans le _make.conf_ :
```
root#  nano /etc/portage/make.conf
nano:
  GENTOO_MIRRORS="http://gentoo.mirrors.ovh.net/gentoo-distfiles/ ftp://ftp.free.fr/mirrors/ftp.gentoo.org/ http://ftp.free.fr/mirrors/ftp.gentoo.org/ http://gentoo.modulix.net/gentoo/ https://mirrors.soeasyto.com/distfiles.gentoo.org/ http://mirrors.soeasyto.com/distfiles.gentoo.org/ ftp://mirrors.soeasyto.com/distfiles.gentoo.org/"
```

Avec le Py0, arrêter gentoo et monter le disque sur la machine lafrier. Une fois sur la machine :
```
root#  nano /media/lafrier/<support-sdcard>/etc/portage/make.conf
nano:
  GENTOO_MIRRORS="http://gentoo.mirrors.ovh.net/gentoo-distfiles/ ftp://ftp.free.fr/mirrors/ftp.gentoo.org/ http://ftp.free.fr/mirrors/ftp.gentoo.org/ http://gentoo.modulix.net/gentoo/ https://mirrors.soeasyto.com/distfiles.gentoo.org/ http://mirrors.soeasyto.com/distfiles.gentoo.org/ ftp://mirrors.soeasyto.com/distfiles.gentoo.org/"
```

Régler la date à la main et l'afficher :
```
root#  date -s '2022-02-26 12:30:00'
root#  date
```

Mettre à jour la base de donnée des paquets :
```
root#  emerge --sync # testé pour voir si ça corrige l'erreur de la commande plus bas "emerge -av net-misc/ntp" ... n'a rien corrigé... peut être la mise à jour qui suit.
root#  emerge --oneshot sys-apps/portage # commande d'update recommandée à la fin de la dernière commande.
root#  emerge-webrsync # à faire ou pas? Avait été fait avant les deux précédentes commandes il y a quelques jours... il y avait eu une erreur comme quoi il faut faire un --revert pour une histoire de timestamp... En n'en tenant pas compte, soit relancer la commande directement, fonctionne peut être, soit la relancer le lendemain et ça a fonctionné sans erreur ou avertissement.
```


Choisir les USE de base :
```
root#  nano /etc/portage/make.conf
nano:
  USE="-systemd -pulseaudio alsa"
```

On prépare portage :
```
root#  mkdir --parents /etc/portage/repos.conf
root#  cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
```

# SUITE EN COURS

On va chercher les sources du noyau sur la machine gentoo existante, avec le emerge de crossdev (avec nodeps car on ne basculera jamais sur le sous-système armv6j-unknown-linux-gnueabihf, puisqu'on ne fera que des armv6j-unknown-linux-gnueabihf-gcc, etc) :
```
root#  mkdir /usr/armv6j-unknown-linux-gnueabihf/etc/portage/package.accept_keywords
root#  echo "sys-kernel/raspberrypi-sources ~arm" \
             >> /usr/armv6j-unknown-linux-gnueabihf/etc/portage/package.accept_keywords/raspberrypi-sources
root#  armv6j-unknown-linux-gnueabihf-emerge -av --nodeps sys-kernel/raspberrypi-sources
```

On va chercher les sources du noau sur  le raspberry :
```
root#
```

# SUITE EN COURS

On fait ce qu'il faut pour la date :
```
root#  emerge -av net-misc/ntp
root#  rc-service ntp-client start
root#  rc-update add ntp-client default
root#  rc-service ntpd start
root#  rc-update add ntpd default
```

Les locales :
```
root#  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
root#  echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
root#  locale-gen
root#  eselect locale list
eselect:
  [4]   en_US.utf8
root#  eselect locale set 4
root#  source /etc/profile
```

ssh :
```
root#  emerge -av net-misc/openssh
```

Suite au changement de locales, emerge annonce la nécessité de mettre à jour le fichier de configuration /etc/conf.d/keymaps. On le fait avec etc-update :
```
root#  etc-update
etc-update:
  choisir /etc/conf.d/keymaps
  sortir de l'éditeur
  Choisir "Delete update, keeping original as is" pour conserver le clavier français
```

```
root#  emerge -av app-portage/gentoolkit
```

Mise à jour :
```
root#  emerge --sync
root#  emerge -uvaDUN @world
```



# VOIR AUSSI
*systeme*(7e), *serveur*(7e), *unix*(7e), *gentoo*(7e)
