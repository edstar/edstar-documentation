latex-gentoo(7e)

# NAME
latex-gentoo - Installer latex sur gentoo

# INSTALLATION
```
root#  echo "USE="${USE} latex" >> /etc/portage/make.conf
root#  emerge -av --update --changed-use --deep @world
root#  emerge -av app-text/texlive
root#  emerge -av dev-texlive/texlive-fontsextra

Voir ce qui est nécessaire ci-dessous ...

root#  emerge -av dev-texlive/texlive-latex
root#  emerge -av dev-texlive/texlive-latexextra
root#  emerge -av dev-texlive/texlive-langfrench
root#  emerge -av dev-texlive/texlive-latexrecommended
root#  emerge -av dev-texlive/texlive-mathscience
root#  emerge -av dev-texlive/texlive-xetex
root#  emerge -av dev-tex/latex-beamer
root#  emerge -av dev-texlive/texlive-bibtexextra
root#  emerge -av dev-tex/biblatex
root#  emerge -av dev-texlive/texlive-publishers
root#  emerge -av app-text/aspell
root#  echo "dev-tex/latex2html gif png" >> /etc/portage/package.use/latex2html
root#  emerge -av dev-tex/latex2html

Finalement, une solution est peut-être d'installer tout dev-texlive et aussi
tout ce qui contient une référence à latex ou à texlive dans app-tex et dev-tex.
```

# REMARQUES
## Chercher un paquet latex
Installer app-portage/pfl. Puis lancer la commande pfl qui crée une base de
donnée. On peut ensuite chercher un paquet avec la commande e-file suivie du nom
du paquet (avec l'exention .sty).

## Mises à jour
Après une mise à jour, il est peut-être utile de lancer la commande suivante :
```
root#  /usr/sbin/texmf-update
```

Voir si cela fait sens d'autoriser les users à génerer des fonts. Si oui :
```
root#  texconfig font rw
```
Si non :
```
root#  texconfig font ro
```



# VOIR AUSSI
*latex*(7e)
