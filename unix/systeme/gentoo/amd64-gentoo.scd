amd64-gentoo(7e)

# NAME
amd64-gentoo - Installer gentoo sur un ordinateur ayant un processeur acceptant le jeu d'instructions _amd64_

# INTRODUCTION

Cette documentation décrit l'installation de la distribution Linux Gentoo sur un ordinateur ayant un processeur acceptant le jeu d'instructions _amd64_. Il ne s'agit que de l'installation. Pour la gestion courante du système voir *gentoo*(7e). Pour l'installation d'un logiciel nommé _toto_, voir si la page de manuel *toto-gentoo*(7e) existe. Pour l'installation d'un service nommé _toto_, voir si la page *toto-serveur-gentoo*(7e) existe.

# CREATION DU LIVECD

## Choisir le bon support d'installation

Consulter la page de téléchargement https://www.gentoo.org/downloads/ et noter
le nom et l'adresse https de l'image iso du CD minimal.

Télécharger l'image iso dans le répertoire /root/gentoo/ :
```
root ~ #  wget https://<lien et nom de fichier>.iso -P ~/gentoo
```

## Créer une clé usb bootable

Insérer une clé usb vierge (ou dont on peut effacer le contenu) et noter le nom
du device, par exemple en regardant le nom du fichier qui est apparu sous /dev/
lors de l'insertion (essayer en retirant/insérant plusiseurs fois si besoin).
Nous faisons ensuite l'hypothèse que le nom du device est sdc.

Copier l'image du CD minimal Gentoo sur la clé USB (si elle est sur /dev/sdc) :
```
root ~#  dd if=/chemin/vers/l/image.iso of=/dev/sdc bs=8192k
root ~#  sync /dev/sdc
```

## Booter sur la clé et tester le réseau

Insertion de la clé usb sur le PC, appuier sur la touche (Suppr, F1, F10, ou Échap) pour accéder au menu boot puis choisir l'option qui permet le démarrage sur la clé.

Appuyer sur une touche quelconque (sauf la touche entréedans les 15 secondes pour rester sur le menu de boot de gentoo.
Deux choix proposés : noyau F1 et options F2 ou taper directement le noyau choisi avec ou sans options.

```
boot : gentoo
```

Choisir le clavier pendant le boot en appuyant immédiatement sur Alt+F1 :
Pour le clavier français, c'est 18 (sans utiliser shift) puis enter.
Rq: On peut aussi saisir directement fr plutôt que faire Alt+F1.
Faire le choix du clavier assez vite car le boot reprend son cours rapidement.
```
<< Load keymap (Enter for default): 18
```

Connecter le PC via la carte réseau éthernet ou wifi ou usb via téléphone et vérifier s'il y a une connexion dans la liste :
```
livecd ~# ifconfig
```

Tester le réseau en envoyant 3 paquets :
```
livecd ~# ping -c 3 www.gentoo.org
```

# PREPARER LES DISQUES

Préparer les disques' on utilise fdisk pour enlever les partitions préexistantes et en créer d'autres :
```
livecd ~# fdisk /dev/sda
Command (m for help) : 'p'# afficher les partitions préexistantes.
Command (m for help) : 'o'# effacer toutes les partitions.
```

Creating the boot partition :

```
Command (m for help):n

Partition type
p   primary (0 primary, 0 extended, 4 free)
e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-60549119, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-60549119, default 60549119): +256M

Created a new partition 1 of type 'Linux' and of size 256 MiB.
```

Creating the swap partition

```
Command (m for help):n

Partition type
p   primary (1 primary, 0 extended, 3 free)
e   extended (container for logical partitions)
Select (default p): p
Partition number (2-4, default 2): 2
First sector (526336-60549119, default 526336):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (526336-60549119, default 60549119): +4G

Created a new partition 2 of type 'Linux' and of size 4 GiB.
```

à réécrire :
After all this is done, type t to set the partition type, 2 to select the partition just created and then type in 82 to set the partition type to "Linux Swap".



```
Command (m for help):t

Partition number (1,2, default 2): 2
Hex code (type L to list all codes): 82

Changed type of partition 'Linux' to 'Linux swap / Solaris'.
```


Creating the root partition :

Finally, to create the root partition, type n to create a new partition. Then type p and 3 to create the third primary partition, /dev/sda3. When prompted for the first sector, hit Enter. When prompted for the last sector, hit Enter to create a partition that takes up the rest of the remaining space on the disk. After completing these steps, typing p should display a partition table that looks similar to this:

```
Command (m for help):p

Disk /dev/sda: 28.89 GiB, 31001149440 bytes, 60549120 sectors
Disk model: DataTraveler 2.0
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe04e67c4

Device     Boot   Start      End  Sectors  Size Id Type
/dev/sda1          2048   526335   524288  256M 83 Linux
/dev/sda2        526336  8914943  8388608    4G 82 Linux swap / Solaris
/dev/sda3       8914944 60549119 51634176 24.6G 83 Linux
```

Saving the partition layout :
```
Command (m for help):w
```
Now it is time to put filesystems on the partitions.


Formater les partitions :
```
livecd ~# mkfs.vfat /dev/sda1
livecd ~# mkfs.ext4 /dev/sda3
livecd ~# mkswap /dev/sda2
livecd ~# swapon /dev/sda2 # démarre la partition swap.
```

Monter la partition racine
```
livecd ~# mount /dev/sda3 /mnt/gentoo
```

Installation d'une archive tar
Réglage de la date et de l'heure
Vérifiez la date et l'heure actuelle avec la commande date :

```
livecd ~# date
```

Mettre à l'heure :
```
livecd ~# ntpd -q -g# si ça ne fonctionne pas, voir la suite.
```

pour régler la date au 3 Octobre 2016 à 13:16 :
```
livecd ~# date 100313162016 # mois jour heure minutes année
```

Téléchargement de l'archive tar
Accédez au point de montage de Gentoo où se trouve le système de fichier racine (probablement /mnt/gentoo) :
livecd ~# /mnt/gentoo

Ceux utilisant un environnement avec des navigateurs Internet graphiques n'auront aucun problème à copier l'adresse d'une archive tar depuis la section téléchargements ( https://www.gentoo.org/downloads/#other-arches ) du site principal. Sélectionnez simplement l'onglet approprié, clique-droit sur le lien vers l'archive tar, ensuite Copier l'adresse du lien pour copier le lien vers le presse-papiers, puis collez le lien à l'utilitaire wget en ligne de commande pour télécharger l'archive tar :
```
livecd ~# wget <URL_DE_L_ARCHIVE_COLLEE>
```

Vérifier et valider l'archive :
Utilisez openssl et comparez les résultats avec les sommes de contrôle fournies par les fichiers .DIGESTS ou .DIGESTS.asc.

À écrire ou à mettre dans un autre man.


Extraction de l'archive tar :

Maintenant, extraire l'archive téléchargée sur le système. Pour ce faire, utilisez la commande tar :

```
root#tar xpvf stage3-*.tar.bz2 --xattrs-include='*.*' --numeric-owner
```

Assurez-vous que les mêmes options (xpf et --xattrs-include='*.*') sont utilisées. Le x signifie extraire, le p pour préserver les permissions et le f pour signifier que l'on veut extraire un fichier (et non l'entrée standard). --xattrs-include='*.*' permet de conserver les attributs étendus contenus dans tous les espaces de noms de l'archive. Finalement, --numeric-owner est utilisé afin d'assurer que les identifiants de groupe et d'utilisateur des fichiers extraits de l'archive restent les mêmes que ceux voulus par l'équipe de Gentoo (même si certains utilisateurs aventureux n'utilisent pas le support d'installation officiel de Gentoo).


Configuration des options de compilation

Lister tout les périphérique connectés et on vérifie que dev/sda3 est bien dans  /mnt/gento
dans la liste affiché par la commande :

```
livecd df - h
```

On vérifie les options de compilateur de gcc pour optimisé la compilation en C et C++ (voir le man de gcc) et pour le calcul en parallèle avec la varialbe MAKEOPTS qui définit le nombre de calcul en parallèle
qui peuvent avoir lieu (choisir le nombre de coeurs de la machine par défaut).

```
livecd nano -w /mnt/gentoo/etc/portage/make.conf
```

Facultatif: Sélection des miroirs
Pas traité ici

Copier les informations DNS

Il reste une chose à faire avant d'entrer dans le nouvel environnement et c'est de copier les informations DNS dans /etc/resolv.conf. C'est nécessaire afin de s'assurer que le réseau fonctionne toujours même après être entré dans le nouvel environnement. /etc/resolv.conf contient les serveurs de nom pour le réseau.

Pour copier ces informations, il est recommandé de passer l'option --dereference à la commande cp. Cela permet de s'assurer que, si /etc/resolv.conf est un lien symbolique, la cible du lien est copiée à la place du lien lui-même. Le lien symbolique dans le nouvel environnement ponterait autrement vers un fichier non existant (vu que la cible du lien n'existe probablement pas dans le nouvel environnement).

```
livecd#cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
```

Monter les systèmes de fichiers nécessaires

```
livecd  ~# mount --types proc /proc /mnt/gentoo/proc
livecd  ~# mount --rbind /sys /mnt/gentoo/sys
*livecd  ~# mount --make-rslave /mnt/gentoo/sys
livecd  ~# mount --rbind /dev /mnt/gentoo/dev
*livecd  ~# mount --make-rslave /mnt/gentoo/dev
```

\* -make-rslave utile que pour le systemd

Entrer dans le nouvel environnement

Maintenant que toutes les partitions sont initialisées et que l'environnement de base est installé, il est temps d'entrer dans le nouvel environnement d'installation en utilisant chroot. Cela signifie que la session changera de racine (emplacement de plus haut niveau pouvant être atteint) depuis l'environnement d'installation courant (cédérom d'installation ou autre support) vers le système d'installation (à savoir les partitions précédemment initialisées). D'où le nom change root ou chroot.

Ce processus de chroot se déroule en trois étapes:

. L'emplacement de la racine est changé de / (sur le support d'installation) à /mnt/gentoo/ (sur les partitions) en utilisant chroot.
. Certains paramètres (situés dans /etc/profile) sont rechargés en mémoire en utilisant la commande source.
. L'invite de commande principal est modifié afin de se rappeler plus facilement que cette session se situe dans un environnement chroot.

```
livecd ~#chroot /mnt/gentoo /bin/bash
livecd /#source /etc/profile
livecd /#export PS1="(chroot) ${PS1}"
```

Monter la partition boot

```
(chroot) livecd /# mount /dev/sda1 /boot
```

Configurer Portage

Installer un instantané du dépôt ebuild depuis le Web

```
(chroot) livecd /# emerge-webrsync
```

Quand le dépôt ebuild de Gentoo est synchronisé sur le système, Portage peut avertir l'utilisateur avec des messages similaires à ceux-ci :
*IMPORTANT: 2 news items need reading for repository 'gentoo'.*
*Use eselect news to read news items.*

Choisir le bon profil

Un profil est un élément de construction pour tout système Gentoo. Non seulement il spécifie des valeurs par défaut pour USE, CFLAGS, et autres variables importantes, il limite aussi le système à une certaine gamme de version des paquets. Ces paramètres sont tous gérés par les développeurs Portage de Gentoo.

Il est possible de voir quel profil le système utilise actuellement grâce à eselect, en utilisant maintenant le module profile :


```
(chroot) livecd /# eselect profile list
```

Ici on choisit le profile de base : default/linux/amd64/17.1 \*

```
root #eselect profile set 1
```

Mettre à jour l'ensemble @world
```
(chroot) livecd /# emerge --ask --verbose --update --deep --newuse @world
```

Visualisation de la configuration de la variable USE
```
(chroot) livecd# emerge --info | grep ^USE
```

On modifie le fichier /etc/portage/make.conf avec la configuration personalisée de la variable USE

```
(chroot) livecd# nano -w /etc/portage/make.conf
```

Nous avons choisi de ne pas utiliser bindist ni pulseaudio et d'ajouter alsa, elogind et X :
USE="-bindist -pulseaudio alsa elogind X"

De plus nous ajoutons MAKEOPTS="-j(insert number of core)" avec le nombre de processeur sur la machine à afficher avec:

```
(chroot) livecd#nproc
```

Fuseau horaire
Choisir le bon fuseau horaire dans la liste :

```
(chroot) livecd#ls /usr/share/zoneinfo
```

et le mettre dans le fichier /etc/timezone (pour OpenRC):

```
(chroot) livecd#echo "Europe/Paris" > /etc/timezone
```

Mettre à jour le fichier etc/localtime en faisant :

```
(chroot) livecd# emerge --config sys-libs/timezone-data
```

## Configurer les paramètres régionaux

Pour choisir la langue de la police et l'encodage

```
(chroot) livecd# nano -w /etc/locale.gen
```

Par exemple pour avoir l'encodage en français en ISO et UTF8, on écrit dans le
fichier : fr_FR ISO-8859-1 fr_FR.UTF-8 UTF-8.

Pour générer les paramétre contenu dans le fichier /etc/locale.gen, on exécute la commande :
```
(chroot) livecd# locale-gen
```

Une fois les parametre définis, il convient d'en choisir un. Pour afficher la liste :
```
(chroot) livecd# eselect locale list
```

Pour choisir un paramètre dans la liste :
```
(chroot) livecd# eselect locale set (insert number)
```

Il faut maintenant mettre l'environnement à jour :
```
(chroot) livecd# env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
```

# Configurer le noyau Linux

## Installer les sources

Choisir les sources
```
(chroot) livecd# emerge --ask sys-kernel/gentoo-sources
```

Lister les sources avec :
```
(chroot) livecd# eselect kernel list
```

Choisir une source avec (exemple pour 1):
```
(chroot) livecd# eselect kernel set 1
```

Puis créer un lien symbolique avec :
```
(chroot) livecd# ls -l /usr/src/linux
```

## Configuration automatique ou manuelle (ici automatique)
On utilise genkernel, en l'installant :
```
(chroot) livecd# emerge --ask sys-kernel/genkernel
```

On configure le point de montage :
```
(chroot) livecd# nano -w /etc/fstab
```
On boot /dev/sda1 depuis ext2 et il faut avoir dans le fichier :
```
/dev/sda1	/boot	ext2	defaults	0 2
```

On ajoute ACCEPT_LICENSE="\*" dans le /etc/portage/make.conf pour supporter n'importe quelle licence.
Compilation du noyau automatique avec :
```
(chroot) livecd#  genkernel all
```

Pour finir la configuration :
```
(chroot) livecd# ls /boot/kernel* /boot/initramfs*
```


## Configurer les modules du noyau
Premierement listons les modules disponibles (en ajoutant la version du noyau par simple completion):
```
(chroot) livecd# find /lib/modules/<version noyau>/ -type f -iname '*.o' -or -iname '*.ko' | less
```

Une fois listé, créons le répertoire modules-load.d avec l'option -p pour créer le répertoire /etc/ si non existant :
```
(chroot) livecd# mkdir -p /etc/modules-load.d
```

Nous pouvons ensuite éditer le fichier /etc/modules-load.d/network.conf avec les modules que nous voulons lister.
```
(chroot) livecd# nano -w /etc/modules-load.d/network.conf
```

Normalement "udev" se chargera de lister automatiquement tout les modules. L'étape précèdente est utile que si nous
voulons utiliser des modules spécifiques.

# Configurer le systeme
## Creer le fichier fstab
Il faut lister les partition dans le fichier fstab avec la commmande :
```
(chroot) livecd# nano -w /etc/fstab
```

Exemple de fichier fstab :

/dev/sda1   /boot        ext2    defaults,noatime     0 2
/dev/sda2   none         swap    sw                   0 0
/dev/sda3   /            ext4    noatime              0 1

/dev/cdrom  /mnt/cdrom   auto    noauto,user          0 0


## Informations de mise en réseau
Nommer l'ordinateur avec la commande
```
(chroot) livecd# nano -w /etc/conf.d/hostname
```

## Réseau
Configure l'interface réseau pour l'environnement installé.
```
(chroot) livecd# emerge --ask --noreplace net-misc/netifrc
```

Pour configurer avec les adresse IP et de routage :
```
(chroot) livecd# nano -w /etc/conf.d/net
```

Exemple de fichier :
config_eth0="dhcp"

Pour activer les interfaces réseau lors du démarrage, elles doivent être ajoutées au runlevel par défaut :
```
(chroot) livecd# cd /etc/init.d
(chroot) livecd# ln -s net.lo net.eth0
(chroot) livecd# rc-update add net.eth0 default
```

Ensuite, informer Linux sur l'environnement réseau :
```
(chroot) livecd# nano -w /etc/hosts
```

Exemple de fichier avec tux nom de l'ordinateur : 
# Cela définit le système actuel et doit être mis
127.0.0.1     tux.homenetwork tux localhost

# Définitions optionnelles d'autres systèmes sur le réseau
192.168.0.5   jenny.homenetwork jenny
192.168.0.6   benny.homenetwork benny

## Informations système
## Mot de passe root
```
(chroot) livecd# passwd
```

## Configuration de l'initialisation et du démarrage
Si necessaire modifier le fichier suivant pour configurer les services.
```
(chroot) livecd# nano -w /etc/rc.conf
```

Pour configurer le clavier :
```
(chroot) livecd# nano -w /etc/conf.d/keymaps
```
Ouvrir le fichier /usr/share/keymaps pour voir les options. Nous choissison le clavier francais keymap="fr".

Configurer finalement le fichier suivant pour les options d'horloge en mettant clock="local".
```
(chroot) livecd# nano -w /etc/conf.d/hwclock
```

# Installation des outils système
## Outil de journalisation du système
Ici nous utilisons sysklogd (d'autres choix sont possibles) avec
```
(chroot) livecd# emerge --ask app-admin/sysklogd
```
puis

```
(chroot) livecd# rc-update add sysklogd default
```

## Synchroniser le temps
En utilise chrony pous synchroniser l'horloge du système.
```
(chroot) livecd# emerge --ask net-misc/chrony
```
puis,
```
(chroot) livecd# rc-update add chronyd default
```
## Outils de mise en réseau
Installer un client DHCP pour se connecter à un serveur, avec la commande
```
(chroot) livecd# emerge --ask net-misc/dhcpcd
```

# Configurer le chargeur d'amorçage

On utilise ici GRUB2

## Emerge
En UEFI on ajoute dans le fichier make.conf cette ligne :
```
(chroot) livecd# echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
```
puis
```
(chroot) livecd# emerge --ask sys-boot/grub
```
Le système GRUB est maintenant présent sur le système mais doit être installé.

## Installation
Avec EFI
```
(chroot) livecd# grub-install --target=x86_64-efi --efi-directory=/boot
```
Sur BIOS :
```
(chroot) livecd# grub-install /dev/sda
```


## Prochaine étape(s)
https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Bootloader/fr#Red.C3.A9marrer_le_syst.C3.A8me

Regler le probleme EFI et MDR

# VOIR AUSSI
- <https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation>

*systeme*(7e), *serveur*(7e)
