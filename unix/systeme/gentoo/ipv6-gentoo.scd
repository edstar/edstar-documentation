ipv6-gentoo(7e)

# NAME
ipv6-gentoo - Activer ou desactiver IPV6 sur Gentoo

# INTRODUCTION

Chaque machine connectée au réseau a potentiellement deux adresses : une
adresse IPV4 et une adresse IPV6. Pour voir vos adresses vous pouvez utiliser
la commande suivante :
```
lafrier$ curl ifconfig.co/json
```
ou bien consulter la page web suivante :
```
https://test-ipv6.com/index.html.fr_FR
```

# ACTIVER IPV6

Par défaut, sur gentoo les deux adresses sont activées. Il n'y a donc rien à
faire.

# DESACTIVER IPV6

Pour desactiver IPV6 :
```
root# vim /etc/default/grub
vim:
  GRUB_CMDLINE_LINUX="ipv6.disable=1"
root# grub-mkconfig -o /boot/grub/grub.cfg
root# reboot
```
