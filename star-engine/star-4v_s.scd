star-4v_s(7e)

# NAME
star-4v_s - Description de star-4v_s

# DESCRIPTION
*star-4v_s* est un logiciel conçu avant tout pour illustrer l'utilisation de la bibliothèque de base du *star-engine*. Ce logiciel prend en entrée un fichier décrivant une surface fermée (au format _obj_) et il estime la moyenne des longueurs de chemins de diffusion multiple depuis leur entrée dans le volume défini par la surface (entrée uniforme, direction d'entrée lambertienne) jusqu'à leur première sortie. Cette longueur moyenne est connue théoriquement~: elle vaut _4V/S_ où _S_ est la surface et _V_ le volume intérieur. Elle est donc indépendante du libre parcours moyen de diffusion et cet invariant est notamment très utile comme moyen de valider les algorithmes de diffusion multiple en géométrie complexe.

# INSTALLATION ET COMPILATION
*star-4v_s* est disponible à l'adresse suivante : _https://gitlab.com/meso-star/star-4v_s_. On l'installe par exemple à l'adresse suivante : _/home/lafrier/Edstar/star-4v_s_. La compilation s'effectue de la façon suivante (en remplaçant _/home/lafrier/Edstar/Star-Engine-0.10.0-GNU-Linux64/_ par l'adresse de la version du *star-engine* que vous souhaitez utiliser) :

```
lafrier$  cd /home/lafrier/Edstar/star-4v_s
lafrier$  mkdir build
lafrier$  cd build
lafrier$  cmake -DCMAKE_PREFIX_PATH=/home/lafrier/Edstar/Star-Engine-0.10.0-GNU-Linux64/ ../cmake/
lafrier$  make
```

# UTILISATION
On commence par aller chercher un fichier _obj_ (le stanford bunny, que l'on place par exemple sous _/home/lafrier/tmp_) :

```
lafrier$  cd /home/lafrier/tmp
lafrier$  wget http://www.prinmath.com/csci5229/OBJ/bunny.zip
lafrier$  unzip bunny.zip
lafrier$  rm bunny.zip
```

Voici un exemple d'utilisation, avec l'échantillonnage de _10000_ chemins et un libre parcours moyen de _0.1_ (donc un coefficient de diffusion de _10_) :

```
lafrier$  source /home/lafrier/Edstar/Star-Engine-0.10.0-GNU-Linux64/etc/star-engine.profile
lafrier$  cd /home/lafrier/Edstar/star-4v_s/build
lafrier$  ./s4vs ~/Gros-volumes-local/Geometries/bunny.obj 10000 10
```

# COMMENTAIRE DU CODE
à écrire

# VOIR AUSSI
*star-engine*(7e)
