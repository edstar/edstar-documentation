Bienvenue !

# Listes de diffusion

Il y en a trois qui sont générales et ensuite il y en a plusieurs autres
qui concernent des projets ou des ateliers [1]

Pour s'inscrire :
envoyer un mail vide à sympa@mailhost avec pour objet 
subscribe liste Prénom Nom

Pour se désinscrire :
envoyer un mail vide à sympa@mailhost avec pour objet 
unsubscribe liste 

En remplaçant "mailhost" et "liste" par les valeurs correspondantes à la
liste à laquelle vous souhaitez vous (dés)inscrire :

## Infos Générales : starmail@services.cnrs.fr
- liste = starmail
- mailhost = services.cnrs.fr

Il s'agit d'une liste d'information générale que vous pouvez utiliser
pour transmettre des news ou des infos. Typiquement pour partager la
sortie d'un papier, l'annonce d'une conf, le lancement d'un atelier, la
soutenance d'une thèse...

## Discussions informatique : edevstar@services.cnrs.fr
- liste = edevstar
- mailhost = services.cnrs.fr

Liste de type "dev" : "users, development discussion, bug reports and
general discussion."

## Séminaire de Roffiac : seminaire-roffiac@groupes.renater.fr
- liste = seminaire-roffiac
- mailhost = groupes.renater.fr

Tout ce qui a trait à l'organisation du séminaire annuel d'EDStar.

# Le nastar

## Kezaco

Le nastar est un disque auquel on peut accéder via ssh [2]. Tous les
membres d'EDStar qui le souhaitent peuvent avoir un compte. Il permet de
stocker des fichiers, des dépots git, visibles ou non par les autres
membres d'EDStar. Il ne permet pas de faire du calcul.

## Pour faire une demande de compte

- Générer une paire de clés ssh via la commande ssh-keygen [3]
- Envoyer la clé publique (*.pub) par courriel à
  stephane.blanco@laplace.univ-tlse.fr en mettant comme objet :
  [nastar] demande de compte Prénom Nom

## Utilisation

Une fois votre compte créé, ajoutez ces lignes à votre fichier
~/.ssh/config (crééez-le s'il n'existe pas) :

Host nastar
  Hostname nastar.laplace.enseeiht.fr
  User votre_nom_de_famille
  IdentityFile ~/.ssh/nom_de_la_cle_nastar

Vous pourrez ainsi vous connecter au nastar en exécutant la ligne de
commande suivante : ssh nastar

# gitlab EDStar

## Kezaco

Le gitlab edstar est un espace de stockage pour les projets git du
collectif. Il permet de cloner et publier les projets git via ssh (ce
qui nécessite d'être membre edstar - voir ci dessous), et également de
cloner des projets git publics via le protocole http (accessible au
monde entier, en lecture seule).

## Pour faire une demande de compte 

- Générer une paire de clé ssh (en ligne de commande avec ssh-keygen)
- Envoyer la clé publique (nom_de_la_cle_gitlab.pub) par courriel à
  najda.villefranque@lmd.ipsl.fr en mettant comme objet :
  [gitlab edstar] demande de compte Prénom Nom

## Utilisation

Une fois votre compte créé, ajoutez ces lignes à votre fichier
~/.ssh/config (crééez le s'il n'existe pas) :

Host gitlab_edstar
  Hostname gitlab.com
  User git
  IdentityFile ~/.ssh/nom_de_la_cle_gitlab

Vous pourrez ainsi cloner un projet en utilisant la commande suivante
git clone gitlab_edstar:edstar/nom_du_projet_a_cloner.git

## Un problème avec gitlab ?

Il arrive que le compte gitlab se bloque. Si vous n'arrivez pas à cloner
ou pousser vos modifications, appelez ou écrivez à Najda ou Jérémi.

# Notes

[1] il nous faudrait un sommaire quelque part qui liste les différentes
listes de diffusion existantes dans le groupe ?

[2] voir le manuel ssh de edstar-documentation

[3] syntaxe de nom de clé ssh : "service_prénom_nom_machine" par exemple
pour la clé nastar de Jean Paul Lafrier sur sa machine coltrane :
nastar_jean_paul_lafrier_coltrane
