#!/bin/sh -e

# Se référer au fichier COPYRIGHT pour les informations sur la propriété
# patrimoniale et au fichier LICENCE pour les conditions d'utilisation et de
# redistribution du présent fichier.

install_bash_completion()
{
  if [ $# -lt 1 ]; then
    echo "usage: install_bash_completion <prefix> [shell ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

  dirs="\
    /usr/share
    /usr/local/share"

  for f in "$@"; do
    dst="${prefix}/share/bash-completion/completions/${f}"
    patch="unix/atelier/bash-completion-${f}.diff" # Patch to apply

    printf "%s\n" "${dirs}" | while read -r dir; do
      src="${dir}/bash-completion/completions/${f}"
      if [ -f "${src}" ]; then
        echo "Installing ${dst}"
        cp "${src}" "${dst}"
        patch -p1 -d "${prefix}/share" < "${patch}"
        break
      fi
    done
  done
}

uninstall_bash_completion()
{
  if [ $# -lt 1 ]; then
    echo "usage: uninstall_bash_completion <prefix> [shell ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

  for f in "$@"; do
    dst="${prefix}/share/bash-completion/completions/${f}"
		echo "Removing ${dst}"
		rm -f "${dst}"
	done
}

install_man()
{
  if [ $# -lt 1 ]; then
    echo "usage: install_man <prefix> [man ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

	for src in "$@"; do
		dst="${prefix}/share/man/man${src##*.}/${src##*/}"
		echo "Installing ${dst}"
		cp "${src}" "${dst}"
	done
}

uninstall_man()
{
  if [ $# -lt 1 ]; then
    echo "usage: uninstall_man <prefix> [man ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

	for src in "$@"; do
		dst="${prefix}/share/man/man${src##*.}/${src##*/}"
		echo "Removing ${dst}"
		rm -f "${dst}"
	done
}

install_shell()
{
  if [ $# -lt 1 ]; then
    echo "usage: install_shell <prefix> [shell ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

  for src in "$@"; do
    dst="${prefix}/bin/${src##*/}"
   	echo "Installing ${dst}"
    cp "${src}" "${dst}"
  done
}

uninstall_shell()
{
  if [ $# -lt 1 ]; then
    echo "usage: uninstall_shell <prefix> [shell ...]" >&2
    exit 1
  fi

  prefix="$1"
  shift 1

	for src in "$@"; do
    dst="${prefix}/bin/${src##*/}"
		echo "Removing ${dst}"
		rm -f "${dst}"
	done
}

"$@"
