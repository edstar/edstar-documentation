# --- 
# Install High-Tune:RenDeRer software:
# --- 

set -e

# 0. Define your work directory and move to it
export WDIR=~/HighTune/renderer/
mkdir -p $WDIR && cd $WDIR

# 1. Clone the git repository
git clone -b High-Tune-0.6.1 https://gitlab.com/meso-star/star-engine.git High-Tune-0.6.1

# 2. Compile
mkdir High-Tune-0.6.1/build
cd High-Tune-0.6.1/build
cmake ../cmake
make

# 3. Test
source $WDIR/High-Tune-0.6.1/local/etc/high_tune.profile
htrdr -h

# => should display short help

# 4. Download the High-Tune:Starter-Pack
cd $WDIR
wget https://www.meso-star.com/projects/high-tune/downloads/High-Tune-Starter-Pack-0.6.0.tar.gz
tar xvf High-Tune-Starter-Pack-0.6.0.tar.gz
rm -f High-Tune-Starter-Pack-0.6.0.tar.gz

# 5. Edit a scene to decrease image definition 
export HTSPK=$WDIR/High-Tune-Starter-Pack-0.6.0/
cd $HTSPK
sed -e "s/image=1280x720/image=320x240/g" \
    -e "s/samples=256/samples=32/g" \
    scenes/DZVAR > scenes/DZVAR_LR

# 6. Compute an image
bash ht-run.sh scenes/DZVAR_LR

# 7. Visualize it
htpp DZVAR_320x240x32.txt | display
