develop-htrdr(7e)

# NAME

develop-htrdr - Développer de nouvelles fonctionnalités dans htrdr

# INTRODUCTION

L'installation de htrdr qui est décrite sur le site web de meso-star est celle
destinée à des utilisateurs extérieurs, qui utiliseront htrdr pour l'essentiel
comme un outil de production. Au sein de la plateforme edstar, htrdr et les
bibliothèques dont il dépend sont des projets de recherche vivants, avec
plusieurs branches de développement avec des niveaux de maturité divers. Ceci
documente une pratique possible de développement du projet htrdr.

# EXEMPLE

On veut rajouter la fonctionnalité suivante dans htrdr : le calcul des
propriétés optiques des nuages (coefficients d'extinction, albedo de diffusion
simple, paramètre d'asymétrie de la fonction de phase) à partir de tables de
Mie ne dépend plus uniquement du champ de concentration en eau mais aussi du
rayon effectif des gouttelettes. Cela signifie que :
1) la bibliothèque htmie qui gère les données de Mie doit être modifiée pour
que les tables incluent une dimension supplémentaire : le rayon effectif des
gouttelettes (dans la version officielle, la seule dimension est la dimension
spectrale)
2) la bibliothèque htcp qui gère les données de la LES (essentiellement le
champ de concentration en eau nuageuse) doit être modifiée pour inclure un
champ supplémentaire : le champ 3D de taille de gouttelettes (dans la version
officielle, ce champ n'existe pas car la taille des gouttelettes est supposée
uniforme dans l'espace)
3) la bibliothèque htsky qui fait le lien entre les données chargées par htmie
et htcp et l'applicatif htrdr doit être modifié pour que les propriétés
optiques soient calculées, pour toute longueur d'onde et toute position, en
fonction de la concentration en eau et de la taille des gouttelettes.
4) l'applicatif htrdr doit être modifié pour que le paramètre d'asymétrie de la
fonction de phase ne soit plus une donnée constante mais dépende de la taille
des gouttelettes via la position de l'évènement de diffusion.

On ne va pas décrire en détail les modifications apportées dans le code mais la
procédure permettant de faire ces modifications.

## Création d'un nouveau répertoire associé au projet de développement

On commence par créer un répertoire local destiné au projet de développement

```
lafrier ~ $ mkdir htrdr_gouttelettes
lafrier ~ $ cd htrdr_gouttelettes
```

## Installation du projet star-engine htrdr comme base de départ

On installe la version du projet distribuée via le star-engine par meso-star :
cloner le projet hébergé sur gitlab et le compiler. L'applicatif htrdr et ses
dépendances seront installés dans le répertoire local/ du répertoire
d'installation. Ca permet de récupérer et installer toutes les dépendances que
l'on ne va pas modifier dans le cadre de ce projet de développement.

```
lafrier ~/htrdr_gouttelettes $ git clone https://gitlab.com/meso-star/star-engine.git -b htrdr star-engine-htrdr
lafrier ~/htrdr_gouttelettes $ cd star-engine-htrdr
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ mkdir build
lafrier ~/htrdr_gouttelettes/star-engine-htrdr/build $ cd build
lafrier ~/htrdr_gouttelettes/star-engine-htrdr/build $ cmake ../cmake
lafrier ~/htrdr_gouttelettes/star-engine-htrdr/build $ make
```

Cette installation de htrdr est utilisable mais les sources sont difficilement
modifiables (l'arborescence qui permet d'y accéder est profonde) et ce type
d'installation via le star-engine n'a pas vocation à accueillir le
développement.

## Modification ciblée du code

### Récupérer les sources des sous-projets pour pouvoir développer dedans

On récupère un par un les sous-projets que l'on va développer
```
lafrier ~/htrdr_gouttelettes $ git clone  https://gitlab.com/meso-star/htmie.git
lafrier ~/htrdr_gouttelettes $ git clone  https://gitlab.com/meso-star/htcp.git
lafrier ~/htrdr_gouttelettes $ git clone  https://gitlab.com/meso-star/htsky.git
lafrier ~/htrdr_gouttelettes $ git clone  https://gitlab.com/meso-star/htrdr.git
```

Ensuite on va compiler et installer ces sous-projets (non encore modifiés) à la
place de ceux qui ont été installées par défaut au moment de l'installation via
le star-engine (pour l'instant ce sont les mêmes)
```
lafrier ~/htrdr_gouttelettes $ cd htmie
lafrier ~/htrdr_gouttelettes/htmie $ mkdir build && cd build
lafrier ~/htrdr_gouttelettes/htmie/build $ cmake ../cmake \
                             -DCMAKE_INSTALL_PREFIX=/home/lafrier/htrdr_gouttelettes/star-engine-htrdr/local/
lafrier ~/htrdr_gouttelettes/htmie/build $ make
lafrier ~/htrdr_gouttelettes/htmie/build $ make install
```

L'option -DCMAKE_INSTALL_PREFIX passée à cmake permet deux choses : au moment
de la compilation (ici de htmie), cmake va chercher les dépendances de htmie
dans le répertoire indiqué (ici
/home/lafrier/htrdr_gouttelettes/star-engine-htrdr/local/) qui auront été
installée lors de l'installation du projet "officiel", et au moment de
l'installation (make install) la bibliothèque htmie que l'on vient de compiler
va être installée au répertoire indiqué, écrasant donc l'installation
précédente. "Installer" signifie seulement que les objets compilés qui se
trouvent dans build/ (les points .so dans le cas des bibliothèques, les
exécutables dans le cas des applicatifs) et les vitrines qui se trouvent dans
src/ (les "headers" qui en C sont les fichiers .h) sont copiés dans le
répertoire d'installation (respectivement dans local/lib/, local/bin/ et
local/include/).

### Modifier les sources des sous-projets et installer les versions modifiées

Pour chaque sous-projet (ici donné pour htmie) :

Créer une nouvelles branche (git branch) et se positionner dessus (git checkout)
```
lafrier ~/htrdr_gouttelettes $ cd htmie
lafrier ~/htrdr_gouttelettes/htmie $ git branch htrdr_gouttelettes
lafrier ~/htrdr_gouttelettes/htmie $ git checkout htrdr_gouttelettes
```

Faire les modifications qu'on veut dans le répertoire src/ du sous-projet

Puis depuis le répertoire build/ du sous-projet, compiler et installer
```
lafrier ~/htrdr_gouttelettes/htmie/build $ make
lafrier ~/htrdr_gouttelettes/htmie/build $ make install
```

Comme on a déjà indiqué à cmake le répertoire d'installation (le répertoire
local/ de l'installation qui a été faite via le star-engine), les versions
modifiées des sous-projets seront installées dans le répertoire en question.

Comme on a aussi (par la même option) indiqué à cmake que les dépendances des
sous-projets étaient installées dans ce même répertoire, la version modifiée de
htsky qui dépend de htmie et htcp va être compilée avec les nouvelles versions
de htmie et htcp que l'on vient d'installer (il faut le faire dans le bon ordre
bien sûr). Et de même avec htrdr qui dépend de htsky, htmie et htcp, il sera
compilé avec les versions modifiées de ces trois sous-projets.

### Publier les sous-projets modifiés

Une fois les modifications effectuées, testées et versionnées à l'aide
de git (voir *git*(1e)), on peut publier ces nouvelles versions des
sous-projets, par exemple sur le gitlab edstar

Avec git on ajoute une cible distante "gitlab-edstar" au projet git local
```
lafrier ~/htrdr_gouttelettes/htmie $ git remote add gitlab-edstar gitlab-edstar:edstar/htmie.git
```

Le premier "gitlab-edstar" est une étiquette pour se référer à la cible
distante, qui est à l'adresse gitlab-edstar:edstar/htmie.git, qui, si on a bien
une entrée gitlab-edstar dans notre ~/.ssh/config (voir man *ssh*(1e)), est un
accès ssh à gitlab.com/edstar/htmie.git. Et de même pour les autres
sous-projets.

On peut alors publier les sources modifiées sur gitlab-edstar :
```
lafrier ~/htrdr_gouttelettes/htmie $ git push gitlab-edstar htrdr_gouttelettes
```

### Enrichir l'installateur de projets

Pour finir, on peut vouloir facilier l'installation de cette nouvelle version
du projet htrdr en enrichissant le star-engine. Ainsi, au lieu d'installer un
par un chaque sous-projet modifié (comme on a dû le faire au début), on pourra
passer par une installation automatique via le star-engine.

Pour cela il suffit de modifier le fichier cmake/CMakeLists.txt de
l'installation qu'on a faite via le star-engine, pour qu'au lieu d'aller
chercher les dépendances sur le gitlab de meso-star, il récupère les
dépendances modifiées et republiées sur le gitlab edstar.

Pour faire ces modifs on va d'abord créer une nouvelle branche au projet
star-engine
```
lafrier ~/htrdr_gouttelettes $ cd star-engine-htrdr
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ git branch htrdr_gouttelettes && git checkout htrdr_gouttelettes
```

Puis on modifie le fichier cmake/CMakeLists.txt : on ajoute une variable
REPO_EDSTAR à la liste des répertoires où sont hébergés les différents
sous-projets
```
@@ -52,6 +52,7 @@ set(REPO_STAR https://gitlab.com/meso-star/ CACHE STRING "The repository of the
 set(REPO_STAR_EX git@gitlab.com:meso-star/ CACHE STRING "Repository of the private Star projects.")
 set(REPO_VAPLV https://gitlab.com/vaplv/ CACHE STRING "The repository of several external projects.")
 set(REPO_PKG https://www.meso-star.com/packages/v0.3/ CACHE STRING "The repository of the binary packages.")
+set(REPO_EDSTAR https:www.gitlab.com/edstar/ CACHE STRING "Repository of edstar's projects")

 if(NOT CMAKE_BUILD_TYPE)
   set(CMAKE_BUILD_TYPE "RELEASE" CACHE STRING "Build type of the Star-Engine" FORCE)
```

Ensuite pour chaque sous-projet que l'on a modifié, on remplace la variable du
répertoire d'hébergement et le nom de version par REPO_EDSTAR et le nom de la
branche sur laquelle on a développé

```
@@ -175,7 +176,7 @@ set(atrtp_project ${REPO_STAR} 0.0
 ################################################################################
 # Declare High-Tune projects
 ################################################################################
-set(htcp_project ${REPO_STAR} 0.0.3
+set(htcp_project ${REPO_EDSTAR} htrdr_gouttelettes
   "-DNO_TEST=ON"
   "rcmake rsys")
```
```
@@ -183,7 +184,7 @@ set(htgop_project ${REPO_STAR} 0.1.2
   "-DNO_TEST=ON"
   "rcmake rsys")

-set(htmie_project ${REPO_STAR} 0.0.3
+set(htmie_project ${REPO_EDSTAR} htrdr_gouttelettes
   "-DNO_TEST=ON"
   "rcmake rsys")
```

On ajoute et on commet ces modifications
```
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ git add cmake/CMakeLists.txt
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ git commit -m "Version htrdr_gouttelettes"
```

On ajoute une cible distante au projet git local
```
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ git remote add gitlab-edstar gitlab-edstar:edstar/star-engine.git
```

et on publie notre star-engine modifié sur gitlab-edstar
```
lafrier ~/htrdr_gouttelettes/star-engine-htrdr $ git push gitlab-edstar htrdr_gouttelettes
```

Si on veut installer cette nouvelle version de htrdr sur une nouvelle machine,
on pourra maintenant le faire via l'installateur star-engine

```
mambo ~ $ git clone https://gitlab.com/edstar/star-engine.git -b htrdr_gouttelettes htrdr_gouttelettes
mambo ~ $ cd htrdr_gouttelettes
mambo ~/htrdr_gouttelettes $ mkdir build
mambo ~/htrdr_gouttelettes/build $ cd build
mambo ~/htrdr_gouttelettes/build $ cmake ../cmake
mambo ~/htrdr_gouttelettes/build $ make
```

# VOIR AUSSI
*ssh*(1e) *git*(1e) *star-engine.txt*
